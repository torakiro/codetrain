package jp.kelonos.codetrain;

public class BusinessLogicException extends Throwable {

	private static final long serialVersionUID = 1L;
	
	public BusinessLogicException(String message) {
		super(message);
	}
}
