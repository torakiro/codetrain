package jp.kelonos.codetrain;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;

import jp.kelonos.codetrain.dto.AuthorityGroupDto;
import jp.kelonos.codetrain.dto.StaffDto;

public class Common {

	public static final int USER = 0;
	public static final int CUSTOMER = 1;
	public static final int OPERATION = 2;

	public static void operatorAfterLogin(StaffDto staffDto, HttpServletRequest request, HttpServletResponse response) {
		// ログインに成功した場合は、権限グループに合わせてそれぞれの画面に遷移する
		try {
			switch (staffDto.getAuthority_group().getInitial_activity()) {
				case AuthorityGroupDto.INITIAL_SHOW_CORPLIST:
					response.sendRedirect("list-customer");
					//request.getRequestDispatcher("list-customer").forward(request, response);
					break;
				case AuthorityGroupDto.INITIAL_SHOW_OPERATORLIST:
					response.sendRedirect("list-operator");
					//request.getRequestDispatcher("list-operator").forward(request, response);
					break;
				case AuthorityGroupDto.INITIAL_SHOW_TEXT:
					response.sendRedirect("list-text");
					//request.getRequestDispatcher("list-text").forward(request, response);
					break;
				case AuthorityGroupDto.INITIAL_SHOW_MESSAGE_USERLIST:
					response.sendRedirect("list-message-sender");
					//request.getRequestDispatcher("list-message-sender").forward(request, response);
					break;
				default:
					Common.logout(Common.OPERATION, response);
			}
			return;
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}
		try {
			response.sendRedirect("logout");
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}

	public static boolean isUserLoggedIn (HttpSession session, HttpServletResponse response, Logger logger) {
		if (session == null || session.getAttribute("user") == null) {
			logger.warn("利用者機能：セッションタイムアウト");
			logout(USER, response);
			return false;
		}
		return true;
	}

	public static boolean isCustomerLoggedIn (HttpSession session, HttpServletResponse response, Logger logger) {
		if (session == null || session.getAttribute("customer") == null) {
			logger.warn("法人窓口機能：セッションタイムアウト");
			logout(CUSTOMER, response);
			return false;
		}
		return true;
	}

	public static boolean isOperatorLoggedIn (HttpSession session, HttpServletResponse response, Logger logger) {
		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("運用管理機能：セッションタイムアウト");
			logout(OPERATION, response);
			return false;
		}
		return true;
	}

	public static void operatorError(HttpServletResponse response) {
		try {
			response.sendRedirect("operation-system-error.jsp");
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}

	public static void customerError(HttpServletResponse response) {
		try {
			response.sendRedirect("customer-system-error.jsp");
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}

	public static void userError(HttpServletResponse response) {
		try {
			response.sendRedirect("system-error.jsp");
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}

	public static void logout(String url, HttpServletResponse response) {
		try {
			response.sendRedirect(url);
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}

	public static void logout(HttpServletResponse response) {
		logout("logout", response);
	}

	public static void logout(int function,HttpServletResponse response) {
		try {
			switch(function) {
				case USER:
					logout("logout?func="+USER, response);
					break;
				case CUSTOMER:
					logout("logout?func="+CUSTOMER, response);
					break;
				case OPERATION:
					logout("logout?func="+OPERATION, response);
					break;
				default:
					response.sendRedirect("system-error.jsp");
			}
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}

}
