package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kelonos.codetrain.dto.UserRequestDto;

/**
 * ユーザテーブルのDataAccessObject
 * @author Mr.X
 */
public class UserRequestDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public UserRequestDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * 連絡してきたユーザを取得する
	 * @return 連絡情報リスト
	 * @throws SQLException SQL例外
	 */
	public List<UserRequestDto> findUsers() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("       user_id");
		sb.append("       ,min(repliedFlg) as isreplied");
		sb.append("       ,user.name");
		sb.append("       ,max(user_request.update_time) as updated_at");
		sb.append("       ,user_request.update_user");
		sb.append("   from user_request");
		sb.append("   inner join user");
		sb.append("   on user_request.user_id = user.id");
		sb.append("  where user.deleteFlg = 0");
		sb.append("  group by user.id");
		sb.append("  order by max(user_request.update_time) desc");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			List<UserRequestDto> list = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				UserRequestDto dto = new UserRequestDto();

				dto.setRepliedFlg(rs.getBoolean("isreplied"));
				dto.setUser_id(rs.getInt("user_id"));
				dto.setUpdate_time(rs.getTimestamp("updated_at"));
				dto.setUpdate_user(rs.getInt("update_user"));
				dto.setUser_name(rs.getString("name"));

				list.add(dto);
			}
			// 該当するデータがない場合はnullを返却する
			return list;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}

	}

	/**
	 * 連絡情報リストと、それに紐づくカテゴリ名・コース名のうち、ユーザIDに紐づくものをすべて取得する
	 * @param ユーザID
	 * @return 連絡情報リスト
	 * @throws SQLException SQL例外
	 */
	public List<UserRequestDto> findAllByUser(int user_id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("       user_request.id as request_id");
		sb.append("       ,user_request.category_id");
		sb.append("       ,user_request.course_id");
		sb.append("       ,text");
		sb.append("       ,repliedFlg");
		sb.append("       ,user_id");
		sb.append("       ,user_request.update_time");
		sb.append("       ,user_request.update_user");
		sb.append("       ,user.name");
		sb.append("       ,user.deleteFlg");
		sb.append("       ,course.name as course_name");
		sb.append("       ,category.name as category_name");
		sb.append("   from user_request inner join user on user_request.user_id = user.id");
		sb.append("    inner join course on user_request.course_id = course.id");
		sb.append("    inner join category on user_request.category_id = category.id");
		sb.append("   where user_id = ?");
		sb.append("   order by user_request.update_time desc");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setInt(1, user_id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			List<UserRequestDto> list = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				UserRequestDto dto = new UserRequestDto();

				dto.setId(rs.getInt("request_id"));
				dto.setCategory_id(rs.getInt("category_id"));
				dto.setCourse_id(rs.getInt("course_id"));
				dto.setText(rs.getString("text"));
				dto.setRepliedFlg(rs.getBoolean("repliedFlg"));
				dto.setUser_id(rs.getInt("user_id"));
				dto.setUpdate_time(rs.getTimestamp("update_time"));
				dto.setUpdate_user(rs.getInt("update_user"));
				dto.setUser_name(rs.getString("name"));
				dto.setCourse_name(rs.getString("course_name"));
				dto.setCategory_name(rs.getString("category_name"));

				list.add(dto);
			}
			// 該当するデータがない場合はnullを返却する
			return list;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}

	}
	/**
	 * 返信フラグを更新する
	 * @param id 要望ID
	 * @param replyFlg あたらしい返信フラグ
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int updateReplyFlg(int id, boolean replyFlg) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update user_request set");
		sb.append("      repliedFlg = ?");
		sb.append(" where id = ?");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setBoolean(1, replyFlg);
			ps.setInt(2, id);

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * 利用者要望情報を登録する
	 * @param dto 利用者要望情報
	 * @return 登録件数
	 * @throws SQLException SQL除外
	 */
	public int insert(UserRequestDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into USER_REQUEST");
		sb.append(" (");
		sb.append(" CATEGORY_ID");
		sb.append(" ,COURSE_ID");
		sb.append(" ,TEXT");
		sb.append(" ,REPLIEDFLG");
		sb.append(" ,USER_ID");
		sb.append(" ,UPDATE_USER");
		sb.append(" )");
		sb.append(" values");
		sb.append(" (");
		sb.append(" ?");
		sb.append(" ,?");
		sb.append(" ,?");
		sb.append(" ,FALSE");
		sb.append(" ,?");
		sb.append(" ,?");
		sb.append(" )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setInt(1, dto.getCategory_id());
			ps.setInt(2, dto.getCourse_id());
			ps.setString(3, dto.getText());
			ps.setInt(4, dto.getUser_id());
			ps.setInt(5, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	public boolean exists(int id)throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append(" select *");
		sb.append("   from USER_REQUEST ");
		sb.append("  where ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果を返す
			if (rs.next()) {
				return true;
			}
			return false;
		}
	}

}
