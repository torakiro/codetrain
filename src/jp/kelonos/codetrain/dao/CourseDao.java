package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.kelonos.codetrain.dto.CourseDto;


public class CourseDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public CourseDao(Connection conn) {
		this.conn = conn;
	}

	public CourseDto findById(int id) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,NAME");
		sb.append("       ,OVERVIEW");
		sb.append("       ,REQUIREMENT");
		sb.append("       ,GOAL");
		sb.append("       ,TIME_APPROX");
		sb.append("       ,CATEGORY_ID");
		sb.append("       ,FREECOURSEFLG");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from COURSE");
		sb.append("   where ID = ?");


		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setInt(1, id);
			// SQLを実行する
			ResultSet rs = ps.executeQuery();
			// 結果をDTOに詰める
			if (rs.next()) {
				CourseDto course = new CourseDto();
				course.setId(rs.getInt("ID"));
				course.setName(rs.getString("NAME"));
				course.setOverview(rs.getString("OVERVIEW"));
				if (rs.wasNull())
					course.setOverview(null);
				course.setRequirement(rs.getString("REQUIREMENT"));
				if (rs.wasNull())
					course.setRequirement(null);
				course.setGoal(rs.getString("GOAL"));
				if (rs.wasNull())
					course.setGoal(null);
				course.setTime_approx(rs.getInt("TIME_APPROX"));
				if (rs.wasNull())
					course.setTime_approx(null);
				course.setCategory_id(rs.getInt("CATEGORY_ID"));
				course.setFreecourseFlg(rs.getBoolean("FREECOURSEFLG"));
				course.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				course.setUpdate_user(rs.getInt("UPDATE_USER"));
				return course;
			}
			return null;
		}
	}

	public List<CourseDto> findAll() throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,NAME");
		sb.append("       ,OVERVIEW");
		sb.append("       ,REQUIREMENT");
		sb.append("       ,GOAL");
		sb.append("       ,TIME_APPROX");
		sb.append("       ,CATEGORY_ID");
		sb.append("       ,FREECOURSEFLG");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from COURSE");


		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			List<CourseDto> list = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				CourseDto course = new CourseDto();
				course.setId(rs.getInt("ID"));
				course.setName(rs.getString("NAME"));
				course.setOverview(rs.getString("OVERVIEW"));
				if (rs.wasNull())
					course.setOverview(null);
				course.setRequirement(rs.getString("REQUIREMENT"));
				if (rs.wasNull())
					course.setRequirement(null);
				course.setGoal(rs.getString("GOAL"));
				if (rs.wasNull())
					course.setGoal(null);
				course.setTime_approx(rs.getInt("TIME_APPROX"));
				if (rs.wasNull())
					course.setTime_approx(null);
				course.setCategory_id(rs.getInt("CATEGORY_ID"));
				course.setFreecourseFlg(rs.getBoolean("FREECOURSEFLG"));
				course.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				course.setUpdate_user(rs.getInt("UPDATE_USER"));
				list.add(course);
			}
			return list;
		}
	}

	/**
	 * カテゴリ情報に紐づくコース情報を取得する
	 * @param categoryId カテゴリID
	 * @return コース情報リスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<CourseDto> findByCategoryId(int categoryId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,NAME");
		sb.append("       ,OVERVIEW");
		sb.append("       ,REQUIREMENT");
		sb.append("       ,GOAL");
		sb.append("       ,TIME_APPROX");
		sb.append("       ,CATEGORY_ID");
		sb.append("       ,FREECOURSEFLG");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from COURSE");
		sb.append("   where CATEGORY_ID = ?");


		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setInt(1, categoryId);
			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			ArrayList<CourseDto>courses =new ArrayList<>();
			// 結果をDTOに詰める
			while (rs.next()) {
				CourseDto course = new CourseDto();
				course.setId(rs.getInt("ID"));
				course.setName(rs.getString("NAME"));
				course.setOverview(rs.getString("OVERVIEW"));
				if (rs.wasNull())
					course.setOverview(null);
				course.setRequirement(rs.getString("REQUIREMENT"));
				if (rs.wasNull())
					course.setRequirement(null);
				course.setGoal(rs.getString("GOAL"));
				if (rs.wasNull())
					course.setGoal(null);
				course.setTime_approx(rs.getInt("TIME_APPROX"));
				if (rs.wasNull())
					course.setTime_approx(null);
				course.setCategory_id(rs.getInt("CATEGORY_ID"));
				course.setFreecourseFlg(rs.getBoolean("FREECOURSEFLG"));
				course.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				course.setUpdate_user(rs.getInt("UPDATE_USER"));
				courses.add(course);
			}
			return courses;
		}
	}

	/**
	 * auto_incrementの値を取得する
	 * @return auto_incrementの値。存在しない場合はnull
	 * @throws SQLException SQL例外
	 */
	public Integer selectAutoIncrement() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("select auto_increment from information_schema.tables where table_name='");
		sb.append("course");
		sb.append("'");

    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());

			Integer result = null;
			while (rs.next()) {
				result = rs.getInt("auto_increment");
			}
			return result;
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * コースを追加する
	 * @param dto コース情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insertWithPrimaryKey(CourseDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into course");
		sb.append("           (");
		sb.append("            id");
		sb.append("            ,name");
		sb.append("            ,overview");
		sb.append("            ,requirement");
		sb.append("            ,goal");
		sb.append("            ,time_approx");
		sb.append("            ,category_id");
		sb.append("            ,freecourseFlg");
		sb.append("            ,UPDATE_USER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getName());
			ps.setString(3, dto.getOverview());
			ps.setString(4, dto.getRequirement());
			ps.setString(5, dto.getGoal());
			if (dto.getTime_approx() != null) {
				ps.setInt(6, dto.getTime_approx());
			} else {
				ps.setNull(6, java.sql.Types.INTEGER);
			}
			ps.setInt(7, dto.getCategory_id());
			ps.setBoolean(8, dto.isFreecourseFlg());
			ps.setInt(9, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * コースを追加する
	 * @param dto コース情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insert(CourseDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into course");
		sb.append("           (");
		sb.append("            name");
		sb.append("            ,overview");
		sb.append("            ,requirement");
		sb.append("            ,goal");
		sb.append("            ,time_approx");
		sb.append("            ,category_id");
		sb.append("            ,freecourseFlg");
		sb.append("            ,UPDATE_USER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getName());
			ps.setString(2, dto.getOverview());
			ps.setString(3, dto.getRequirement());
			ps.setString(4, dto.getGoal());
			if (dto.getTime_approx() != null) {
				ps.setInt(5, dto.getTime_approx());
			} else {
				ps.setNull(5, java.sql.Types.INTEGER);
			}
			ps.setInt(6, dto.getCategory_id());
			ps.setBoolean(7, dto.isFreecourseFlg());
			ps.setInt(8, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}

	}

	public boolean exists(int id)throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append(" select *");
		sb.append("   from COURSE ");
		sb.append("  where ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果を返す
			if (rs.next()) {
				return true;
			}
			return false;
		}
	}

}
