package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kelonos.codetrain.dto.AuthorityGroupDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * ユーザテーブルのDataAccessObject
 * @author Mr.X
 */
public class StaffDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public StaffDao(Connection conn) {
		this.conn = conn;
	}

	/**
     * IDとパスワードに紐づくユーザ情報を取得する
     * @param id ID
     * @param password パスワード
     * @return ユーザ情報
     * @throws SQLException SQL例外
     */
    public StaffDto findByidAndPassword(String id, String password) throws SQLException {

    	// SQL文を作成する
    	StringBuffer sb = new StringBuffer();
    	sb.append(" select");
    	sb.append("        staff.ID");
    	sb.append("       ,LOGIN_ID");
//    	sb.append("       ,PASSWORD");
    	sb.append("       ,staff.NAME");
    	sb.append("       ,passwordsetflg");
    	sb.append("       ,authority_group");
    	sb.append("       ,staff.UPDATE_TIME");
    	sb.append("       ,staff.UPDATE_USER");
    	sb.append("       ,authority_group.name as authority_name");
    	sb.append("       ,show_operatorlist");
    	sb.append("       ,create_operator");
    	sb.append("       ,show_corplist");
    	sb.append("       ,create_corp");
    	sb.append("       ,create_text");
    	sb.append("       ,edit_text");
    	sb.append("       ,show_text");
    	sb.append("       ,reply_message");
    	sb.append("       ,show_message");
    	sb.append("       ,show_message_userlist");
    	sb.append("       ,control_billing");
    	sb.append("   from STAFF");
    	sb.append("   inner join authority_group");
    	sb.append("   on staff.authority_group = authority_group.id");
    	sb.append("  where LOGIN_ID = ?");
    	sb.append("    and PASSWORD = sha2(?, 256)");

    	// ステートメントオブジェクトを作成する
        try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホルダーに値をセットする
            ps.setString(1, id);
            ps.setString(2, password);

            // SQLを実行する
            ResultSet rs = ps.executeQuery();

            // 結果をDTOに詰める
            if (rs.next()) {
                StaffDto user = new StaffDto();
                user.setId(rs.getInt("staff.ID"));
                user.setLogin_id(rs.getString("LOGIN_ID"));
                user.setName(rs.getString("staff.NAME"));
                user.setPasswordsetFlg(rs.getBoolean("passwordsetflg"));
                user.setUpdate_time(rs.getTimestamp("staff.update_time"));
                user.setUpdate_user(rs.getInt("staff.update_user"));

                AuthorityGroupDto authority = new AuthorityGroupDto();
            	setAuthority(authority, rs);
                user.setAuthority_group(authority);
                return user;
            }
            // 該当するデータがない場合はnullを返却する
        	return null;
        }
    }

	/**
     * 運営者の情報をすべて取得する
     * @return 運営者情報リスト
     * @throws SQLException SQL例外
     */
    public List<StaffDto> findAll() throws SQLException {

    	// SQL文を作成する
    	StringBuffer sb = new StringBuffer();
    	sb.append(" select");
    	sb.append("        staff.ID");
    	sb.append("       ,LOGIN_ID");
//    	sb.append("       ,PASSWORD");
    	sb.append("       ,staff.NAME");
    	sb.append("       ,passwordsetflg");
    	sb.append("       ,authority_group");
    	sb.append("       ,staff.UPDATE_TIME");
    	sb.append("       ,staff.UPDATE_USER");
    	sb.append("       ,authority_group.name as authority_name");
    	sb.append("       ,show_operatorlist");
    	sb.append("       ,create_operator");
    	sb.append("       ,show_corplist");
    	sb.append("       ,create_corp");
    	sb.append("       ,create_text");
    	sb.append("       ,edit_text");
    	sb.append("       ,show_text");
    	sb.append("       ,reply_message");
    	sb.append("       ,show_message");
    	sb.append("       ,show_message_userlist");
    	sb.append("       ,control_billing");
    	sb.append("   from STAFF");
    	sb.append("   inner join authority_group");
    	sb.append("   on staff.authority_group = authority_group.id");

    	// ステートメントオブジェクトを作成する
        try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

            // SQLを実行する
            ResultSet rs = ps.executeQuery();

            List<StaffDto> list = new ArrayList<>();

            // 結果をDTOに詰める
            while (rs.next()) {
                StaffDto user = new StaffDto();
                user.setId(rs.getInt("staff.ID"));
                user.setLogin_id(rs.getString("LOGIN_ID"));
                user.setName(rs.getString("staff.NAME"));
                user.setPasswordsetFlg(rs.getBoolean("passwordsetflg"));
                user.setUpdate_time(rs.getTimestamp("staff.update_time"));
                user.setUpdate_user(rs.getInt("staff.update_user"));

                AuthorityGroupDto authority = new AuthorityGroupDto();
            	setAuthority(authority, rs);
                user.setAuthority_group(authority);
                list.add(user);
            }
        	return list;
        }
    }

	/**
     * AuthorityDTOに初期画面情報、権限情報を格納する。
     * @param dto 入れる対象のauthorityDTO
     * @param rs resultsetインスタンス
     * @return パラメータのauthorityDTOインスタンスに初期画面情報、権限情報を格納したもの
     * @throws SQLException SQL例外
     */
    static AuthorityGroupDto setAuthority(AuthorityGroupDto dto, ResultSet rs) throws SQLException{

    	dto.setName(rs.getString("authority_name"));
    	dto.setShow_operatorlist(rs.getBoolean("show_operatorlist"));
    	dto.setCreate_operator(rs.getBoolean("create_operator"));
    	dto.setShow_corplist(rs.getBoolean("show_corplist"));
    	dto.setCreate_corp(rs.getBoolean("create_corp"));
    	dto.setCreate_text(rs.getBoolean("create_text"));
    	dto.setEdit_text(rs.getBoolean("edit_text"));
    	dto.setShow_text(rs.getBoolean("show_text"));
    	dto.setReply_message(rs.getBoolean("reply_message"));
    	dto.setShow_message(rs.getBoolean("show_message"));
    	dto.setShow_message_userlist(rs.getBoolean("show_message_userlist"));
    	dto.setControl_billing(rs.getBoolean("control_billing"));

    	dto.setInitial_activity(AuthorityGroupDto.NO_INITIAL_ACTIVITY);
    	if (rs.getInt("show_operatorlist") >= 2) dto.setInitial_activity(AuthorityGroupDto.INITIAL_SHOW_OPERATORLIST);
    	if (rs.getInt("show_text") >= 2) dto.setInitial_activity(AuthorityGroupDto.INITIAL_SHOW_TEXT);
    	if (rs.getInt("show_corplist") >= 2) dto.setInitial_activity(AuthorityGroupDto.INITIAL_SHOW_CORPLIST);
    	if (rs.getInt("show_message_userlist") >= 2) dto.setInitial_activity(AuthorityGroupDto.INITIAL_SHOW_MESSAGE_USERLIST);

    	return dto;
    }

	/**
	 * パスワードを変更する
	 * @param dto ユーザ情報
	 * @param old_password 現在のパスワード
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int updatePassword(StaffDto dto, String old_password) throws SQLException {

    	// SQL文を作成する
		String sql = "update staff set password=sha2(?, 256), update_user=?, passwordsetFlg=? where id=? and password=sha2(?, 256)";

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sql)) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getPassword());
			ps.setInt(2, dto.getUpdate_user());
			ps.setBoolean(3, dto.isPasswordsetFlg());
			ps.setInt(4, dto.getId());
			ps.setString(5, old_password);

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (SQLException e) {
			throw e;
		}
	}

	/**
	 * 運用管理者情報を追加する
	 * @param dto 追加するユーザ情報
	 * @param staff ログイン中のユーザ
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insert(StaffDto dto, int user_id) throws SQLException {

    	// SQL文を作成する
    	StringBuffer sb = new StringBuffer();
    	sb.append(" insert into staff(");
    	sb.append("        login_id");
    	sb.append("       ,PASSWORD");
    	sb.append("       ,name");
    	sb.append("       ,authority_group");
    	sb.append("       ,passwordsetFlg");
    	sb.append("       ,UPDATE_USER");
    	sb.append("  ) values(");
    	sb.append("       ?,sha2(?, 256),?,?,?,?");
    	sb.append("  )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getLogin_id());
			ps.setString(2, dto.getPassword());
			ps.setString(3, dto.getName());
			ps.setInt(4, dto.getAuthority_group().getId());
			ps.setBoolean(5, dto.isPasswordsetFlg());
			ps.setInt(6, user_id);

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (SQLException e) {
			throw e;
		}
	}

	/**
	 * 運営担当者の氏名と権限グループを変更する
	 * @param dto ユーザ情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int updateNameAndAuthorityGroup(StaffDto dto) throws SQLException {

    	// SQL文を作成する
		String sql = "update staff set name = ?, authority_group = ?, update_user = ? where id = ?";

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sql)) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getName());
			ps.setInt(2, dto.getAuthority_group().getId());
			ps.setInt(3, dto.getUpdate_user());
			ps.setInt(4, dto.getId());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (SQLException e) {
			throw e;
		}
	}

}
