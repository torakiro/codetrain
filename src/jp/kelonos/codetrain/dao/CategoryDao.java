package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kelonos.codetrain.dto.CategoryDto;

public class CategoryDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public CategoryDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * IDとパスワードに紐づくユーザ情報を取得する
	 * @param id ID
	 * @param password パスワード
	 * @return ユーザ情報
	 * @throws SQLException SQL例外
	 */
	public List<CategoryDto> selectAll() throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,NAME");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from CATEGORY");


		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			List<CategoryDto>categorys =new ArrayList<>();
			// 結果をDTOに詰める
			while (rs.next()) {
				CategoryDto category = new CategoryDto();
				category.setId(rs.getInt("ID"));
				category.setName(rs.getString("NAME"));
				category.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				category.setUpdate_user(rs.getInt("UPDATE_USER"));
				categorys.add(category);
			}
			return categorys;
		}
	}

	/**
	 * カテゴリID紐づくカテゴリ情報を取得する
	 * @param categoryId カテゴリID
	 * @return カテゴリ情報
	 * @throws SQLException SQL例外
	 */
	public CategoryDto findById(int id) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,NAME");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from CATEGORY");
		sb.append("   where ID = ?");


		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				CategoryDto category = new CategoryDto();
				category.setId(rs.getInt("ID"));
				category.setName(rs.getString("NAME"));
				category.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				category.setUpdate_user(rs.getInt("UPDATE_USER"));
				return category;
			}
			return null;
		}
	}

	/*
	 * カテゴリー情報を追加する
	 * @param dto カテゴリー情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insert(CategoryDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into category");
		sb.append("           (");
		sb.append("             name");
	//	sb.append("            ,text");
	//	sb.append("            ,curriculum_id");
	//	sb.append("            ,update_user");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
	//	sb.append("            ,?");
	//	sb.append("            ,?");
	//	sb.append("            ,?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getName());
	//		ps.setString(2, dto.getText());
	//		ps.setInt(3, dto.getCurriculum_id());
	//		ps.setInt(4, dto.getUpdate_user());
	//

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	public boolean exists(int id)throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append(" select *");
		sb.append("   from CATEGORY ");
		sb.append("  where ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果を返す
			if (rs.next()) {
				return true;
			}
			return false;
		}
	}

}