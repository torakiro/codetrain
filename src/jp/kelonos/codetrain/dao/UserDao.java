package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.kelonos.codetrain.dto.UserDto;

public class UserDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public UserDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * IDとパスワードに紐づくユーザ情報を取得する
	 * @param id ID
	 * @param password パスワード
	 * @return ユーザ情報
	 * @throws SQLException SQL例外
	 */
	public UserDto findByIdAndPassword(String id, String password) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        USER.ID");
		sb.append("       ,USER.LOGIN_ID");
		sb.append("       ,USER.NAME");
		sb.append("       ,USER.STATE");
		sb.append("       ,USER.PASSWORDSETFLG ");
		sb.append("       ,USER.DELETEFLG");
		sb.append("       ,USER.CORP_ID");
		sb.append("       ,USER.INITIAL_PASSWORD_ID");
		sb.append("       ,USER.EMPNO");
		sb.append("       ,USER.suspend_month");
		sb.append("       ,USER.UPDATE_TIME");
		sb.append("       ,USER.UPDATE_USER");
		sb.append("   from USER INNER JOIN INITIALPASSWORD ");
		sb.append("   ON USER.INITIAL_PASSWORD_ID = INITIALPASSWORD.ID ");
		sb.append("  where LOGIN_ID = ?");
		sb.append("    and ");
		sb.append("    USER.DELETEFLG = false");
		sb.append("    and ");
		sb.append("   ((PASSWORD = sha2(?, 256) and user.passwordsetflg) ");
		sb.append(" or (INITIALPASSWORD.INITIAL_PASSWORD = ? and not user.passwordsetflg)) ");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, id);
			ps.setString(2, password);
			ps.setString(3, password);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				UserDto user = new UserDto();
				user.setId(rs.getInt("ID"));
				user.setLoginId(rs.getString("LOGIN_ID"));
				user.setName(rs.getString("NAME"));
				user.setState(rs.getInt("STATE"));
				user.setPasswordsetFlg(rs.getBoolean("PASSWORDSETFLG"));
				user.setDeleteFlg(rs.getBoolean("DELETEFLG"));
				user.setCorp_id(rs.getInt("CORP_ID"));
				user.setInitialpassword_id(rs.getInt("INITIAL_PASSWORD_ID"));
				user.setEmpno(rs.getInt("EMPNO"));
				if (rs.wasNull())
					user.setEmpno(null);
				user.setSuspend_month(rs.getDate("suspend_month"));
				user.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				user.setUpdate_user(rs.getInt("UPDATE_USER"));
				return user;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	/**
	 * 法人IDに紐づくユーザ情報を取得する
	 * @param corp_id 法人ID
	 * @return ユーザ情報リスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<UserDto> selectByCorpId(int corp_id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" id");
		sb.append(" ,login_id");
		sb.append(" ,name");
		sb.append(" ,state");
		sb.append(" ,passwordsetflg");
		sb.append(" ,deleteflg");
		sb.append(" ,corp_id");
		sb.append(" ,initial_password_id");
		sb.append(" ,empno");
		sb.append(" ,suspend_month");
		sb.append(" ,update_time");
		sb.append(" ,update_user");
		sb.append(" from user");
		sb.append(" where corp_id = ?");
		sb.append(" and ");
		sb.append(" deleteflg = false");

		ArrayList<UserDto> list = new ArrayList<UserDto>();

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, corp_id);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDto user = new UserDto();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setName(rs.getString("name"));
				user.setState(rs.getInt("state"));
				user.setPasswordsetFlg(rs.getBoolean("passwordsetflg"));
				user.setDeleteFlg(rs.getBoolean("deleteflg"));
				user.setCorp_id(rs.getInt("corp_id"));
				user.setEmpno(rs.getInt("empno"));
				if (rs.wasNull())
					user.setEmpno(null);
				user.setSuspend_month(rs.getDate("suspend_month"));
				user.setUpdate_time(rs.getTimestamp("update_time"));
				user.setUpdate_user(rs.getInt("update_user"));
				list.add(user);
			}
			return list;

		}
	}

	/**
	 * IDに紐づくユーザ情報を取得する
	 * @param id ID
	 * @return ユーザ情報
	 * @throws SQLException SQL例外
	 */
	public UserDto selectByPrimaryKey(int id) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,LOGIN_ID");
		sb.append("       ,NAME");
		sb.append("       ,STATE");
		sb.append("       ,PASSWORDSETFLG ");
		sb.append("       ,DELETEFLG");
		sb.append("       ,CORP_ID");
		sb.append("       ,INITIAL_PASSWORD_ID");
		sb.append("       ,EMPNO");
		sb.append("       ,suspend_month");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from USER");
		sb.append("  where ID = ?");
		sb.append("    and ");
		sb.append("    DELETEFLG = false");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				UserDto user = new UserDto();
				user.setId(rs.getInt("ID"));
				user.setLoginId(rs.getString("LOGIN_ID"));
				user.setName(rs.getString("NAME"));
				user.setState(rs.getInt("STATE"));
				user.setPasswordsetFlg(rs.getBoolean("PASSWORDSETFLG"));
				user.setDeleteFlg(rs.getBoolean("DELETEFLG"));
				user.setCorp_id(rs.getInt("CORP_ID"));
				user.setInitialpassword_id(rs.getInt("INITIAL_PASSWORD_ID"));
				user.setEmpno(rs.getInt("EMPNO"));
				user.setSuspend_month(rs.getDate("suspend_month"));
				user.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				user.setUpdate_user(rs.getInt("UPDATE_USER"));
				return user;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	/**
	 * ユーザIDに紐づくユーザ情報を取得する
	 * @param id ユーザID
	 * @return ユーザ情報リスト
	 * @throws SQLException SQL例外
	 */
	public UserDto selectById(int id) throws SQLException {

		//SQLを作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" user.name as user_name");
		sb.append(" ,course.name as course_name");
		sb.append(" ,learn_history.progress");
		sb.append(" ,learn_history.correct_percent");
		sb.append(" from ((user");
		sb.append(" inner join learn_history");
		sb.append(" on user.id = learn_history.user_id)");
		sb.append(" inner join curriculum");
		sb.append(" on learn_history.curriculum_id = curriculum.id)");
		sb.append(" inner join course");
		sb.append(" on curriculum.course_id = course.id");
		sb.append(" where user.id = ?");
		sb.append(" and ");
		sb.append(" deleteflg = false");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDto user = new UserDto();
				user.setName(rs.getString("user_name"));
				user.setCourse_name(rs.getString("course_name"));
				user.setProgress(rs.getInt("progress"));
				user.setCorrect_percent(rs.getInt("correct_percent"));
				return user;
			}
			return null;
		}
	}

	/**
	 * 活性状態、法人IDに該当するユーザ数を取得する
	 * @param state 活性状態
	 * @param corp_id 法人ID
	 * @return ユーザ数
	 * @throws SQLException SQL例外
	 */
	public int selectCountByStateAndCorpId(int state, int corp_id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" count(*) as count");
		sb.append(" from user");
		sb.append(" where corp_id = ?");
		sb.append(" and ");
		sb.append(" state = ?");
		sb.append(" and ");
		sb.append(" deleteflg = false");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, corp_id);
			ps.setInt(2, state);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("count");
			}
			return 0;

		}
	}

	/**
	 * 今月休止状態へ移行したユーザ数を取得する
	 * @param corp_id 法人ID
	 * @return ユーザ数
	 * @throws SQLException SQL例外
	 */
	public int selectNewlySuspendedCountByCorpId(int corp_id, Date suspended_month) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" count(*) as count");
		sb.append(" from user");
		sb.append(" where corp_id = ?");
		sb.append(" and ");
		sb.append(" DATE_FORMAT(suspend_month, '%Y%m') = DATE_FORMAT(?, '%Y%m')");
		sb.append(" and ");
		sb.append(" deleteflg = false");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, corp_id);
			ps.setDate(2, suspended_month);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("count");
			}
			return 0;

		}
	}

	/**
	 * ユーザ情報を登録する
	 * @param dto ユーザ情報
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int insert(UserDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into user");
		sb.append(" (");
		sb.append(" id");
		sb.append(" ,login_id");
		sb.append(" ,state");
		sb.append(" ,passwordsetflg");
		sb.append(" ,deleteflg");
		sb.append(" ,corp_id");
		sb.append(" ,initial_password_id");
		sb.append(" ,empno");
		sb.append(" ,update_user");
		sb.append(" )");
		sb.append(" values");
		sb.append(" (");
		sb.append(" ?");
		sb.append(",?");
		sb.append(",0");
		sb.append(",false");
		sb.append(",false");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(" )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getLoginId());
			ps.setInt(3, dto.getCorp_id());
			ps.setInt(4, dto.getInitialpassword_id());
			if (dto.getEmpno() != null) { // empnoはjava.lang.Integerとする。
				ps.setInt(5, dto.getEmpno());
			} else {
				ps.setNull(5, java.sql.Types.INTEGER);
			}
			ps.setInt(6, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * ユーザの初期パスワードを更新する
	 * @param dto ユーザ情報
	 * @param initialPass 初期パスワード
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int updatePassword(UserDto user, String oldPass) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("update USER");
		sb.append(" set");
		sb.append(" PASSWORD=sha2(?, 256)");
		sb.append(" , UPDATE_USER=?");
		sb.append(" , PASSWORDSETFLG=?");
		sb.append(" , UPDATE_TIME = CURRENT_TIMESTAMP");
		sb.append(" where");
		sb.append(" ID=?");
		sb.append(" and");
		sb.append(" PASSWORD=sha2(?, 256)");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, user.getPassword());
			ps.setInt(2, user.getUpdate_user());
			ps.setBoolean(3, user.isPasswordsetFlg());
			ps.setInt(4, user.getId());
			ps.setString(5, oldPass);

			// SQLを実行する
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw e;
		}
	}

	/**
	 * IDに紐づくユーザを休止状態にする
	 * @param dto ユーザ情報
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int suspendStateById(UserDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();

		sb.append("update user");
		sb.append(" set");
		sb.append(" state = ?");
		sb.append(" ,suspend_month = ?");
		sb.append(" ,update_user = ?");
		sb.append(" where");
		sb.append(" id = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getState());
			ps.setDate(2, dto.getSuspend_month());
			ps.setInt(3, dto.getUpdate_user());
			ps.setInt(4, dto.getId());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * IDに紐づくユーザを休止状態から復帰させる
	 * @param dto ユーザ情報
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int resumeStateById(UserDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();

		sb.append("update user");
		sb.append(" set");
		sb.append(" state = ?");
		sb.append(" ,update_user = ?");
		sb.append(" where");
		sb.append(" id = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getState());
			ps.setInt(2, dto.getUpdate_user());
			ps.setInt(3, dto.getId());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * IDに紐づくユーザ情報を論理削除する
	 * @param dto ユーザ情報
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int remove(UserDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();

		sb.append("update user");
		sb.append(" set");
		sb.append(" deleteflg = true");
		sb.append(" where");
		sb.append(" id = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getId());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * ユーザのパスワードを変更する
	 * @param dto ユーザ情報
	 * @param initialPass 古いパスワード
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int updateUser(UserDto user, String initialPass) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("update ");
		sb.append("USER,INITIALPASSWORD");
		sb.append(" set");
		sb.append(" USER.PASSWORD=sha2(?, 256)");
		sb.append(" , USER.UPDATE_USER=?");
		sb.append(" , USER.PASSWORDSETFLG=?");
		sb.append(" , USER.UPDATE_TIME = CURRENT_TIMESTAMP");
		sb.append(" , USER.NAME = ?");
		sb.append(" where");
		sb.append(" USER.ID=?");
		sb.append(" and");
		sb.append("  INITIALPASSWORD.INITIAL_PASSWORD = ?");
		sb.append(" and");
		sb.append("  USER.PASSWORDSETFLG = false");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, user.getPassword());
			ps.setInt(2, user.getUpdate_user());
			ps.setBoolean(3, user.isPasswordsetFlg());
			ps.setString(4, user.getName());
			ps.setInt(5, user.getId());
			ps.setString(6, initialPass);

			// SQLを実行する
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw e;
		}
	}

	public int updateSuspendMonthByPrimaryKey(UserDto user) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("update ");
		sb.append(" user");
		sb.append(" set");
		sb.append("  suspend_month = ?");
		sb.append("  ,update_user = ?");
		sb.append(" where");
		sb.append("  id = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setDate(1, user.getSuspend_month());
			ps.setInt(2, user.getUpdate_user());
			ps.setInt(3, user.getId());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw e;
		}
	}

}
