package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.kelonos.codetrain.dto.LearnHistoryDto;

public class LearnHistoryDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public LearnHistoryDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 *ユーザーIDに紐づくヒストリー情報リストを取得する
	 * @param userId ユーザーID
	 * @return ヒストリー情報のリスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<LearnHistoryDto> findByUserIdAll(int userId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,CURRICULUM_ID");
		sb.append("       ,PROGRESS");
		sb.append("       ,STARTED_AT");
		sb.append("       ,CORRECT_PERCENT ");
		sb.append("       ,USER_ID");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from LEARN_HISTORY");
		sb.append("  where USER_ID = ? ");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			ArrayList<LearnHistoryDto> historyDtos = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				LearnHistoryDto history = new LearnHistoryDto();
				history.setId(rs.getInt("ID"));
				history.setCurriculum_id(rs.getInt("CURRICULUM_ID"));
				history.setProgress(rs.getInt("PROGRESS"));
				history.setStarted_at(rs.getTimestamp("STARTED_AT"));
				history.setCorrect_percent(rs.getInt("CORRECT_PERCENT"));
				history.setUser_id(rs.getInt("USER_ID"));
				history.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				history.setUpdate_user(rs.getInt("UPDATE_USER"));
				historyDtos.add(history);
			}
			// 該当するデータがない場合はnullを返却する
			return historyDtos;
		}
	}

	/**
	 * カリキュラムIDとユーザーIDに紐づくヒストリー情報を取得する
	 * @param curriculumId カリキュラムID
	 * @param userId ユーザーID
	 * @return ヒストリー情報
	 * @throws SQLException SQL例外
	 */
	public LearnHistoryDto findByCurIdAndUserId(int curriculumId, int userId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,CURRICULUM_ID");
		sb.append("       ,PROGRESS");
		sb.append("       ,STARTED_AT");
		sb.append("       ,CORRECT_PERCENT ");
		sb.append("       ,USER_ID");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from LEARN_HISTORY");
		sb.append("  where CURRICULUM_ID = ?");
		sb.append("    and USER_ID = ? ");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, curriculumId);
			ps.setInt(2, userId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				LearnHistoryDto history = new LearnHistoryDto();
				history.setId(rs.getInt("ID"));
				history.setCurriculum_id(rs.getInt("CURRICULUM_ID"));
				history.setProgress(rs.getInt("PROGRESS"));
				history.setStarted_at(rs.getTimestamp("STARTED_AT"));
				history.setCorrect_percent(rs.getInt("CORRECT_PERCENT"));
				history.setUser_id(rs.getInt("USER_ID"));
				history.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				history.setUpdate_user(rs.getInt("UPDATE_USER"));
				return history;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	/**
	 *ユーザーIDとコースIDに紐づく最後に更新したヒストリー情報を取得する
	 * @param userId ユーザーID
	 * @return 最後に更新したヒストリー情報
	 * @throws SQLException SQL例外
	 */
	public LearnHistoryDto findByUserIdLast(int userId, int courseId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        LEARN_HISTORY.ID");
		sb.append("       ,LEARN_HISTORY.CURRICULUM_ID");
		sb.append("       ,LEARN_HISTORY.PROGRESS");
		sb.append("       ,LEARN_HISTORY.STARTED_AT");
		sb.append("       ,LEARN_HISTORY.CORRECT_PERCENT ");
		sb.append("       ,LEARN_HISTORY.USER_ID");
		sb.append("       ,LEARN_HISTORY.UPDATE_TIME");
		sb.append("       ,LEARN_HISTORY.UPDATE_USER");
		sb.append("   from LEARN_HISTORY INNER JOIN CURRICULUM");
		sb.append("   ON LEARN_HISTORY.CURRICULUM_ID = CURRICULUM.ID ");
		sb.append("  where LEARN_HISTORY.USER_ID = ? ");
		sb.append("  and CURRICULUM.course_id = ? ");
		sb.append("  order by LEARN_HISTORY.UPDATE_TIME desc");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);
			ps.setInt(2, courseId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				LearnHistoryDto history = new LearnHistoryDto();
				history.setId(rs.getInt("ID"));
				history.setCurriculum_id(rs.getInt("CURRICULUM_ID"));
				history.setProgress(rs.getInt("PROGRESS"));
				history.setStarted_at(rs.getTimestamp("STARTED_AT"));
				history.setCorrect_percent(rs.getInt("CORRECT_PERCENT"));
				history.setUser_id(rs.getInt("USER_ID"));
				history.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				history.setUpdate_user(rs.getInt("UPDATE_USER"));
				return history;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	public ArrayList<LearnHistoryDto> findByUserIdAndCourseIdAll(int userId, int courseId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        LEARN_HISTORY.ID");
		sb.append("       ,LEARN_HISTORY.CURRICULUM_ID");
		sb.append("       ,LEARN_HISTORY.PROGRESS");
		sb.append("       ,LEARN_HISTORY.STARTED_AT");
		sb.append("       ,LEARN_HISTORY.CORRECT_PERCENT ");
		sb.append("       ,LEARN_HISTORY.USER_ID");
		sb.append("       ,LEARN_HISTORY.UPDATE_TIME");
		sb.append("       ,LEARN_HISTORY.UPDATE_USER");
		sb.append("   from LEARN_HISTORY INNER JOIN CURRICULUM");
		sb.append("   ON LEARN_HISTORY.CURRICULUM_ID = CURRICULUM.ID ");
		sb.append("  where LEARN_HISTORY.USER_ID = ? ");
		sb.append("  and CURRICULUM.course_id = ? ");
		sb.append("  order by LEARN_HISTORY.UPDATE_TIME desc");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);
			ps.setInt(2, courseId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			ArrayList<LearnHistoryDto> historyDtos = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				LearnHistoryDto history = new LearnHistoryDto();
				history.setId(rs.getInt("ID"));
				history.setCurriculum_id(rs.getInt("CURRICULUM_ID"));
				history.setProgress(rs.getInt("PROGRESS"));
				history.setStarted_at(rs.getTimestamp("STARTED_AT"));
				history.setCorrect_percent(rs.getInt("CORRECT_PERCENT"));
				history.setUser_id(rs.getInt("USER_ID"));
				history.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				history.setUpdate_user(rs.getInt("UPDATE_USER"));
				historyDtos.add(history);
			}
			// 該当するデータがない場合はnullを返却する
			return historyDtos;
		}
	}

	//カリキュラムID、進捗率, 正誤率、ユーザーIDを学習履歴表に追加する
	public int insert(int curriculum_id, int progress, int correct_parcent, int user_id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into LEARN_HISTORY");
		sb.append("           (");
		sb.append("             CURRICULUM_ID");
		sb.append("            ,PROGRESS");
		sb.append("            ,CORRECT_PERCENT");
		sb.append("            ,USER_ID");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, curriculum_id);
			ps.setInt(2, progress);
			ps.setInt(3, correct_parcent);
			ps.setInt(4, user_id);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	//IDに紐づく、進捗率, 正誤率を更新する
	public int update(int progress, int correct_parcent, int id) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        LEARN_HISTORY");
		sb.append("    set");
		sb.append("        PROGRESS = ?");
		sb.append("       ,CORRECT_PERCENT = ?");
		sb.append("       ,UPDATE_TIME = CURRENT_TIMESTAMP");
		sb.append("  where ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, progress);
			ps.setInt(2, correct_parcent);
			ps.setInt(3, id);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	/**
	 *ユーザーIDに紐づくコース情報リストを取得する
	 * @param userId ユーザーID
	 * @return コース情報のリスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<LearnHistoryDto> findFullHistoryByUserId(int userId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        category.name as category_name");
		sb.append("       ,course.name as course_name");
		sb.append("       ,curriculum.name as curriculum_name");
		sb.append("       ,course.ID as course_id");
		sb.append("       ,PROGRESS");
		//	sb.append("       ,STARTED_AT");
		sb.append("       ,CORRECT_PERCENT ");
		//	sb.append("       ,USER_ID");
		sb.append("       ,learn_history.UPDATE_TIME");
		sb.append("       ,learn_history.UPDATE_USER");
		sb.append(" from LEARN_HISTORY INNER JOIN CURRICULUM on learn_history.curriculum_id = curriculum.id");
		sb.append(" inner join course on curriculum.course_id = course.id");
		sb.append(" inner join category on course.category_id = category.id");
		sb.append("  where learn_history.USER_ID = ? ");
		/*
		sb.append("  group by course.id ");
		sb.append("  order by category.Id ");
		*/
		sb.append("  order by category.Id, course.id, curriculum.id ");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);
			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			ArrayList<LearnHistoryDto> historyDtos = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				LearnHistoryDto history = new LearnHistoryDto();
				history.setCourse_id(rs.getInt("course_ID"));
				history.setCurriculum_name(rs.getString("CURRICULUM_name"));
				history.setCourse_name(rs.getString("course_name"));
				history.setProgress(rs.getInt("PROGRESS"));
				history.setCategory_name(rs.getString("category_name"));
				history.setCourse_id(rs.getInt("course_id"));
				history.setCorrect_percent(rs.getInt("CORRECT_PERCENT"));
				//				history.setUser_id(rs.getInt("USER_ID"));
				history.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				history.setUpdate_user(rs.getInt("UPDATE_USER"));
				historyDtos.add(history);
			}
			// 該当するデータがない場合はnullを返却する
			return historyDtos;
		}
	}

	/**
	 *ユーザーIDに紐づくコース情報リストを取得する
	 * @param userId ユーザーID
	 * @return コース情報のリスト
	 * @throws SQLException SQL例外
	 */
	public Map<Integer, LearnHistoryDto> selectCourseProgressesByUserId(int userId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        avg(progress) as PROGRESS");
		sb.append("       ,course.id as course_id");
		sb.append("       ,learn_history.id as learn_history_id");
		sb.append(" from ((LEARN_HISTORY INNER JOIN CURRICULUM on learn_history.curriculum_id = curriculum.id)");
		sb.append(" inner join course on curriculum.course_id = course.id)");
		sb.append(" inner join category on course.category_id = category.id");
		sb.append("  where learn_history.USER_ID = ? ");
		sb.append("  group by course.id ");
		sb.append("  order by category.Id ");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);
			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			Map<Integer, LearnHistoryDto> historyDtos = new HashMap<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				LearnHistoryDto history = new LearnHistoryDto();
				history.setCourse_id(rs.getInt("course_id"));
				history.setId(rs.getInt("learn_history_id"));
				history.setProgress(rs.getInt("PROGRESS"));
				historyDtos.put(history.getCourse_id(), history);
			}
			// 該当するデータがない場合はnullを返却する
			return historyDtos;
		}
	}
}
