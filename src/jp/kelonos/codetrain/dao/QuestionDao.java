package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.kelonos.codetrain.dto.QuestionDto;

public class QuestionDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public QuestionDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * カリキュラムIDに紐づく問題の数を取得する
	 * @param curriculumId カリキュラムID
	 * @return 問題数
	 * @throws SQLException SQL例外
	 */
	public int countCurId(int curriculumId) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        COUNT(*)");
		sb.append("   from QUESTION ");
		sb.append("  where CURRICULUM_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, curriculumId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果を返す
			if (rs.next()) {
				return rs.getInt("COUNT(*)");
			}
			return 0;
		}
	}

	/**
	 * カリキュラムIDに紐づく問題情報のリストを取得する
	 * @param curriculumId カリキュラムID
	 * @return 問題情報リスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<QuestionDto> findByCurriculumId(int curriculumId) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,CURRICULUM_ID");
		sb.append("       ,NAME");
		sb.append("       ,QUESTION");
		sb.append("       ,CHOICE1");
		sb.append("       ,CHOICE2");
		sb.append("       ,CHOICE3");
		sb.append("       ,CHOICE4");
		sb.append("       ,ANSNO");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from QUESTION ");
		sb.append("  where CURRICULUM_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, curriculumId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			ArrayList<QuestionDto> questionDtos = new ArrayList<>();
			// 結果をDTOに詰める
			while (rs.next()) {
				QuestionDto question = new QuestionDto();
				question.setId(rs.getInt("ID"));
				question.setCurriculum_id(rs.getInt("CURRICULUM_ID"));
				question.setName(rs.getString("NAME"));
				question.setQuestion(rs.getString("QUESTION"));
				question.setChoice1(rs.getString("CHOICE1"));
				question.setChoice2(rs.getString("CHOICE2"));
				question.setChoice3(rs.getString("CHOICE3"));
				question.setChoice4(rs.getString("CHOICE4"));
				question.setAnsno(rs.getInt("ANSNO"));
				question.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				question.setUpdate_user(rs.getInt("UPDATE_USER"));
				questionDtos.add(question);
			}
			return questionDtos;
		}
	}
}
