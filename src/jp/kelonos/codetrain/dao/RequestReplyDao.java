package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.kelonos.codetrain.dto.RequestReplyDto;

/**
 * ユーザテーブルのDataAccessObject
 * @author Mr.X
 */
public class RequestReplyDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public RequestReplyDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * ユーザIDに該当する、それぞれの要望に紐づく返信リストのリストを取得する
	 * @param ユーザID
	 * @return 返信リストリスト
	 * @throws SQLException SQL例外
	 */
	public Map<Integer, List<RequestReplyDto>> getMapByUserId(int user_id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("       request_reply.id");
		sb.append("       ,request_reply.text");
		sb.append("       ,request_reply.created_at");
		sb.append("       ,request_reply.request_id");
		sb.append("       ,request_reply.update_time");
		sb.append("       ,request_reply.update_user");
		sb.append("       ,staff.name as staff_name");
		sb.append("   from user_request");
		sb.append("    inner join request_reply on user_request.id = request_reply.request_id");
		sb.append("    inner join staff on request_reply.update_user = staff.id");
		sb.append("   where user_id = ?");
		sb.append("   order by request_id desc, request_reply.update_time desc");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setInt(1, user_id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			Map<Integer, List<RequestReplyDto>> result = new HashMap<Integer, List<RequestReplyDto>>();

			int request_id = 0;
			List<RequestReplyDto> list = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				RequestReplyDto dto = new RequestReplyDto();

				dto.setId(rs.getInt("id"));
				dto.setText(rs.getString("text"));
				dto.setCreated_at(rs.getTimestamp("created_at"));
				dto.setRequest_id(rs.getInt("request_id"));
				dto.setUpdate_time(rs.getTimestamp("update_time"));
				dto.setUpdate_user(rs.getInt("update_user"));
				dto.setStaff_name(rs.getString("staff_name"));

				if (!list.isEmpty() && request_id != dto.getRequest_id()) {
					result.put(request_id, list);
					list = new ArrayList<>();
				}
				list.add(dto);
				request_id = dto.getRequest_id();

			}
			if (!list.isEmpty()) result.put(request_id, list);
			return result;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}

	}

	/**
	 * 返信情報を追加する
	 * @param dto 返信情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insert(RequestReplyDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into request_reply");
		sb.append("           (");
		sb.append("             text");
		sb.append("            ,request_id");
		sb.append("            ,created_at");
		sb.append("            ,update_user");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getText());
			ps.setInt(2, dto.getRequest_id());
			ps.setTimestamp(3, dto.getCreated_at());
			ps.setInt(4, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

}
