package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kelonos.codetrain.dto.BillingDto;

public class BillingDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public BillingDao(Connection conn) {
		this.conn = conn;
	}

	public List<BillingDto> selectByCorpId(int corp_id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" id");
		sb.append(" ,corp_id");
		sb.append(" ,price");
		sb.append(" ,billedflg");
		sb.append(" ,billing_month");
		sb.append(" ,cnt_user_active");
		sb.append(" ,cnt_user_suspended");
		sb.append(" ,cnt_user_newly_suspended");
		sb.append(" ,price_user_active");
		sb.append(" ,price_user_suspended");
		sb.append(" ,price_user_newly_suspended");
		sb.append(" ,update_time");
		sb.append(" ,update_user");
		sb.append(" from billing");
		sb.append(" where corp_id = ?");

		List<BillingDto> list = new ArrayList<>();

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, corp_id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				BillingDto billing = new BillingDto();
				billing.setId(rs.getInt("id"));
				billing.setCorp_id(rs.getInt("corp_id"));
				billing.setPrice(rs.getInt("price"));
				billing.setBilledFlg(rs.getBoolean("billedflg"));
				billing.setBilling_month(rs.getDate("billing_month"));
				billing.setCnt_user_active(rs.getInt("cnt_user_active"));
				billing.setCnt_user_suspended(rs.getInt("cnt_user_suspended"));
				billing.setCnt_user_newly_suspended(rs.getInt("cnt_user_newly_suspended"));
				billing.setPrice_user_active(rs.getInt("price_user_active"));
				billing.setPrice_user_suspended(rs.getInt("price_user_suspended"));
				billing.setPrice_user_newly_suspended(rs.getInt("price_user_newly_suspended"));
				billing.setUpdate_time(rs.getTimestamp("update_time"));
				billing.setUpdate_user(rs.getInt("update_user"));
				list.add(billing);
			}
			return list;
		}
	}

	/**
	 * ユーザ情報を登録する
	 * @param dto ユーザ情報
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int insert(BillingDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into billing");
		sb.append(" (");
		sb.append(" corp_id");
		sb.append(" ,price");
		sb.append(" ,billedflg");
		sb.append(" ,billing_month");
		sb.append(" ,cnt_user_active");
		sb.append(" ,cnt_user_suspended");
		sb.append(" ,cnt_user_newly_suspended");
		sb.append(" ,price_user_active");
		sb.append(" ,price_user_suspended");
		sb.append(" ,price_user_newly_suspended");
		sb.append(" ,update_user");
		sb.append(" )");
		sb.append(" values");
		sb.append(" (");
		sb.append(" ?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(",?");
		sb.append(" )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getCorp_id());
			ps.setInt(2, dto.getPrice());
			ps.setBoolean(3, dto.isBilledFlg());
			ps.setDate(4, dto.getBilling_month());
			ps.setInt(5, dto.getCnt_user_active());
			ps.setInt(6, dto.getCnt_user_suspended());
			ps.setInt(7, dto.getCnt_user_newly_suspended());
			ps.setInt(8, dto.getPrice_user_active());
			ps.setInt(9, dto.getPrice_user_suspended());
			ps.setInt(10, dto.getPrice_user_newly_suspended());
			ps.setInt(11, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * 解答表の解答、正誤を更新する
	 * @param answer 解答
	 * @param correct 正誤
	 * @return 1 完了 0 エラー
	 * @throws SQLException SQL例外
	 */
	public int updateByBillingMonthAndCorpId(BillingDto dto) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        billing");
		sb.append("    set");
		sb.append(" corp_id = ?");
		sb.append(" ,price = ?");
		sb.append(" ,billedflg = ?");
		//sb.append(" ,billing_month");
		sb.append(" ,cnt_user_active = ?");
		sb.append(" ,cnt_user_suspended = ?");
		sb.append(" ,cnt_user_newly_suspended = ?");
		sb.append(" ,price_user_active = ?");
		sb.append(" ,price_user_suspended = ?");
		sb.append(" ,price_user_newly_suspended = ?");
		sb.append(" ,update_user = ?");
		sb.append("  where DATE_FORMAT(billing_month, '%Y%m') = DATE_FORMAT(?, '%Y%m')");
		sb.append("  and corp_id = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getCorp_id());
			ps.setInt(2, dto.getPrice());
			ps.setBoolean(3, dto.isBilledFlg());
			ps.setInt(4, dto.getCnt_user_active());
			ps.setInt(5, dto.getCnt_user_suspended());
			ps.setInt(6, dto.getCnt_user_newly_suspended());
			ps.setInt(7, dto.getPrice_user_active());
			ps.setInt(8, dto.getPrice_user_suspended());
			ps.setInt(9, dto.getPrice_user_newly_suspended());
			ps.setInt(10, dto.getUpdate_user());
			ps.setDate(11, dto.getBilling_month());
			ps.setInt(12, dto.getCorp_id());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}
}
