package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jp.kelonos.codetrain.dto.BillingAddressDto;

/**
 * チャンネルテーブルのDataAccessObject
 * @author Mr.X
 */
public class BillingAddressDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public BillingAddressDao(Connection conn) {
        this.conn = conn;
    }

	/**
	 * 請求先一覧を取得する
	 * @return 請求先リスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<BillingAddressDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          id");
		sb.append("         ,address");
		sb.append("         ,corp_id");
		sb.append("         ,UPDATE_AT");
		sb.append("         ,UPDATE_NUMBER");
		sb.append("     from billingaddress");
		sb.append(" order by id");

		ArrayList<BillingAddressDto> list = new ArrayList<BillingAddressDto>();
    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				BillingAddressDto dto = new BillingAddressDto();
				dto.setId(rs.getInt("id"));
				dto.setAddress(rs.getString("address"));
				dto.setCorp_id(rs.getInt("corp_id"));
				dto.setUpdate_time(rs.getTimestamp("udpate_at"));
				dto.setUpdate_user(rs.getInt("update_user"));
				list.add(dto);
			}
			return list;
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * 請求先情報を追加する
	 * @param dto 請求先情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insert(BillingAddressDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into billingaddress");
		sb.append("           (");
		sb.append("             address");
		sb.append("            ,corp_id");
		sb.append("            ,UPDATE_USER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getAddress());
			ps.setInt(2, dto.getCorp_id());
			ps.setInt(3, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

}
