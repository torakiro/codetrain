package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jp.kelonos.codetrain.dto.CorpDto;

/**
 * チャンネルテーブルのDataAccessObject
 * @author Mr.X
 */
public class CorpDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public CorpDao(Connection conn) {
        this.conn = conn;
    }

	/**
	 * 法人一覧を取得する
	 * @return 法人リスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<CorpDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          id");
		sb.append("         ,name");
		sb.append("         ,domain");
		sb.append("         ,corp_person_id");
		sb.append("         ,UPDATE_time");
		sb.append("         ,UPDATE_user");
		sb.append("     from corp");
		sb.append(" order by update_time desc");

		ArrayList<CorpDto> list = new ArrayList<CorpDto>();
    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				CorpDto dto = new CorpDto();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setDomain(rs.getString("domain"));
				dto.setCorp_person_id(rs.getInt("corp_person_id"));
				dto.setUpdate_time(rs.getTimestamp("update_time"));
				dto.setUpdate_user(rs.getInt("update_user"));
				list.add(dto);
			}
			return list;
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * 法人情報を追加する
	 * @param dto 法人情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insert(CorpDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into corp");
		sb.append("           (");
		sb.append("             name");
		sb.append("            ,domain");
		sb.append("            ,corp_person_id");
		sb.append("            ,UPDATE_USER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getName());
			ps.setString(2, dto.getDomain());
			ps.setInt(3, dto.getCorp_person_id());
			ps.setInt(4, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * 主キーを指定して法人情報を追加する
	 * @param dto 法人情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insertWithPrimaryKey(CorpDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into corp");
		sb.append("           (");
		sb.append("             id");
		sb.append("            ,name");
		sb.append("            ,domain");
		sb.append("            ,corp_person_id");
		sb.append("            ,UPDATE_USER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getName());
			ps.setString(3, dto.getDomain());
			ps.setInt(4, dto.getCorp_person_id());
			ps.setInt(5, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * auto_incrementの値を取得する
	 * @return auto_incrementの値。存在しない場合はnull
	 * @throws SQLException SQL例外
	 */
	public Integer selectAutoIncrement() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("select auto_increment from information_schema.tables where table_name='");
		sb.append("corp");
		sb.append("'");

    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());

			Integer result = null;
			while (rs.next()) {
				result = rs.getInt("auto_increment");
			}
			return result;
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

}
