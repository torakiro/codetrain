package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jp.kelonos.codetrain.dto.CorpPersonDto;

/**
 * 法人担当者テーブルのDataAccessObject
 * @author Mr.X
 */
public class CorpPersonDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public CorpPersonDao(Connection conn) {
        this.conn = conn;
    }

	/**
	 * IDとパスワードに紐づくユーザ情報を取得する
	 * @param id ID
	 * @param password パスワード
	 * @return ユーザ情報
	 * @throws SQLException SQL例外
	 */
	public CorpPersonDto findByIdAndPassword(String id, String password) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" corp_person.id");
		sb.append(" ,corp_person.name");
		// sb.append(" ,PASSWORD");
		sb.append(" ,corp_person.email");
		sb.append(" ,corp_person.position");
		sb.append(" ,corp_person.passwordsetflg");
		sb.append(" ,corp_person.update_time");
		sb.append(" ,corp_person.update_user");
		sb.append(" ,corp.id as corp_id");
		sb.append(" from corp_person");
		sb.append(" inner join corp");
		sb.append(" on corp_person.id = corp_person_id");
		sb.append(" where corp_person.email = ?");
		sb.append(" and corp_person.password = sha2(?, 256)");
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, id);
			ps.setString(2, password);
			// SQLを実行する
			ResultSet rs = ps.executeQuery();
			// 結果をDTOに詰める
			if (rs.next()) {
				CorpPersonDto user = new CorpPersonDto();
				user.setId(rs.getInt("id"));
				user.setEmail(rs.getString("email"));
				//user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setPasswordsetFlg(rs.getBoolean("passwordsetflg"));
				user.setUpdate_time(rs.getTimestamp("update_time"));
				user.setUpdate_user(rs.getInt("update_user"));
				user.setCorp_id(rs.getInt("corp_id"));
				return user;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	/**
	 * IDとパスワードに紐づく法人窓口アカウントのパスワードを更新する
	 * @param id ID
	 * @param password 現在のパスワード
	 * @param newPassword 新しいパスワード
	 * @return
	 * @throws SQLException SQL除外
	 */
	public int updatePasswordByIdAndPassword(int id, String password, String newPassword)
			throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append(" corp_person");
		sb.append(" set");
		sb.append(" password = sha2(?, 256)");
		sb.append(" ,passwordsetflg = 1");
		sb.append(" where password = sha2(? ,256)");
		sb.append(" and id = ?");

		//ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1,newPassword);
			ps.setString(2,password);
			ps.setInt(3, id);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	/**
	 * 請求先情報を追加する
	 * @param dto 請求先情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insert(CorpPersonDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into corp_person");
		sb.append("           (");
		sb.append("             name");
		sb.append("            ,email");
		sb.append("            ,password");
		sb.append("            ,position");
		sb.append("            ,passwordsetFlg");
		sb.append("            ,UPDATE_USER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,sha2(?, 256)");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getName());
			ps.setString(2, dto.getEmail());
			ps.setString(3, dto.getPassword());
			ps.setString(4, dto.getPosition());
			ps.setBoolean(5, dto.isPasswordsetFlg());
			ps.setInt(6, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * 主キーを指定して法人担当者情報を追加する
	 * @param dto 法人担当者情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insertWithPrimaryKey(CorpPersonDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into corp_person");
		sb.append("           (");
		sb.append("             id");
		sb.append("            ,name");
		sb.append("            ,email");
		sb.append("            ,password");
		sb.append("            ,position");
		sb.append("            ,passwordsetFlg");
		sb.append("            ,UPDATE_USER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,sha2(?, 256)");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getName());
			ps.setString(3, dto.getEmail());
			ps.setString(4, dto.getPassword());
			ps.setString(5, dto.getPosition());
			ps.setBoolean(6, dto.isPasswordsetFlg());
			ps.setInt(7, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * auto_incrementの値を取得する
	 * @return auto_incrementの値。存在しない場合はnull
	 * @throws SQLException SQL例外
	 */
	public Integer selectAutoIncrement() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("select auto_increment from information_schema.tables where table_name='");
		sb.append("corp_person");
		sb.append("'");

    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());

			Integer result = null;
			while (rs.next()) {
				result = rs.getInt("auto_increment");
			}
			return result;
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

}
