package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.kelonos.codetrain.dto.PriceDto;

public class PriceDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public PriceDao(Connection conn) {
		this.conn = conn;
	}

	public PriceDto get() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" *");
		sb.append(" from price");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQLを実行する
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				PriceDto dto = new PriceDto();
				dto.setMonthlyprice_active_user(rs.getInt("monthlyprice_active_user"));
				dto.setMonthlyprice_suspended_user(rs.getInt("monthlyprice_suspended_user"));
				dto.setUpdate_time(rs.getTimestamp("update_time"));
				dto.setUpdate_user(rs.getInt("update_user"));
				return dto;
			}
			return null;
		}
	}

	public int set(PriceDto dto) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        price");
		sb.append("    set");
		sb.append(" monthlyprice_active_user = ?");
		sb.append(" ,monthlyprice_suspended_user = ?");
		sb.append(" ,update_time = ?");
		sb.append(" ,update_user = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getMonthlyprice_active_user());
			ps.setInt(2, dto.getMonthlyprice_suspended_user());
			ps.setTimestamp(3, dto.getUpdate_time());
			ps.setInt(4, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}
}
