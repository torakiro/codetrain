package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.kelonos.codetrain.dto.AnswerDto;

public class AnswerDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public AnswerDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * カリキュラムIDとユーザーIDに紐づく解答情報リストを取得する
	 * @param curriculumId カリキュラムID
	 * @param userId ユーザーID
	 * @return 解答情報リスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<AnswerDto> selectCurIdAndUserId(int curriculumId, int userId) throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        USER_ANSWER.ID");
		sb.append("       ,USER_ANSWER.QUESTION_ID");
		sb.append("       ,USER_ANSWER.ANSWER");
		sb.append("       ,USER_ANSWER.CORRECT");
		sb.append("       ,USER_ANSWER.USER_ID");
		sb.append("       ,USER_ANSWER.UPDATE_TIME");
		sb.append("       ,USER_ANSWER.UPDATE_USER");
		sb.append("   from USER_ANSWER inner join QUESTION ");
		sb.append("   on USER_ANSWER.QUESTION_ID = QUESTION.ID ");
		sb.append("  where QUESTION.CURRICULUM_ID = ?");
		sb.append("  and    USER_ANSWER.USER_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, curriculumId);
			ps.setInt(2, userId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			ArrayList<AnswerDto> answerDtos = new ArrayList<>();
			// 結果をDTOに詰める
			while (rs.next()) {
				AnswerDto answer = new AnswerDto();
				answer.setId(rs.getInt("USER_ANSWER.ID"));
				answer.setQuestion_id(rs.getInt("USER_ANSWER.QUESTION_ID"));
				answer.setAnswer(rs.getInt("USER_ANSWER.ANSWER"));
				answer.setCorrect(rs.getBoolean("USER_ANSWER.CORRECT"));
				answer.setUser_id(rs.getInt("USER_ANSWER.USER_ID"));
				answer.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				answer.setUpdate_user(rs.getInt("UPDATE_USER"));
				answerDtos.add(answer);
			}
			return answerDtos;
		}
	}

	/**
	 * クエスチョンIDとユーザーIDに紐づく解答を取得する
	 * @param questionId クエスチョンID
	 * @param userId ユーザーID
	 * @return 解答情報
	 * @throws SQLException SQL例外
	 */
	public AnswerDto selectQuestIdAndUserId(int questionId, int userId) throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,QUESTION_ID");
		sb.append("       ,ANSWER");
		sb.append("       ,CORRECT");
		sb.append("       ,USER_ID");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from USER_ANSWER");
		sb.append("  where QUESTION_ID = ?");
		sb.append("  and    USER_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, questionId);
			ps.setInt(2, userId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			while (rs.next()) {
				AnswerDto answer = new AnswerDto();
				answer.setId(rs.getInt("ID"));
				answer.setQuestion_id(rs.getInt("QUESTION_ID"));
				answer.setAnswer(rs.getInt("ANSWER"));
				answer.setCorrect(rs.getBoolean("CORRECT"));
				answer.setUser_id(rs.getInt("USER_ID"));
				answer.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				answer.setUpdate_user(rs.getInt("UPDATE_USER"));
				return answer;
			}
			return null;
		}
	}

	/**
	 * 1つの問題に何回解答しているか
	 * @param questionId クエスチョンID
	 * @param userId ユーザーID
	 * @return 解答数
	 * @throws SQLException SQL例外
	 */
	public int isAnswer(int questionId,int userId) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        COUNT(*)");
		sb.append("   from USER_ANSWER ");
		sb.append("  where QUESTION_ID = ?");
		sb.append("  and    USER_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, questionId);
			ps.setInt(2, userId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果を返す
			if (rs.next()) {
				return rs.getInt("COUNT(*)");
			}
			return 0;
		}
	}

	/**
	 * 1つのカリキュラムの中の解答数
	 * @param curriculumId カリキュラムID
	 * @param userId ユーザーID
	 * @return 解答数
	 * @throws SQLException SQL例外
	 */
	public int countAnswer(int curriculumId,int userId) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        COUNT(*)");
		sb.append("   from USER_ANSWER inner join QUESTION ");
		sb.append("   on USER_ANSWER.QUESTION_ID = QUESTION.ID ");
		sb.append("  where CURRICULUM_ID = ?");
		sb.append("  and    USER_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, curriculumId);
			ps.setInt(2, userId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果を返す
			if (rs.next()) {
				return rs.getInt("COUNT(*)");
			}
			return 0;
		}
	}

	/**
	 * 1つのカリキュラムの中の正答数
	 * @param curriculumId カリキュラムID
	 * @param userId ユーザーID
	 * @return 正答数
	 * @throws SQLException SQL例外
	 */
	public int countCorrect(int curriculumId,int userId) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        COUNT(*)");
		sb.append("   from USER_ANSWER inner join QUESTION ");
		sb.append("   on USER_ANSWER.QUESTION_ID = QUESTION.ID ");
		sb.append("  where CURRICULUM_ID = ?");
		sb.append("  and    USER_ID = ?");
		sb.append("  and    CORRECT = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, curriculumId);
			ps.setInt(2, userId);
			ps.setInt(3, 1);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果を返す
			if (rs.next()) {
				return rs.getInt("COUNT(*)");
			}
			return 0;
		}
	}

	/**
	 * クエスチョンID、解答、正誤、ユーザーIDを解答表に追加する
	 * @param questId クエスチョンID
	 * @param answer 解答
	 * @param correct 正誤
	 * @param userId ユーザーID
	 * @return 1 完了 0 エラー
	 * @throws SQLException SQL例外
	 */
	public int insert(int questId, int answer, boolean correct , int userId) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into USER_ANSWER");
		sb.append("           (");
		sb.append("             QUESTION_ID");
		sb.append("            ,ANSWER");
		sb.append("            ,CORRECT");
		sb.append("            ,USER_ID");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, questId);
			ps.setInt(2, answer);
			ps.setBoolean(3, correct);
			ps.setInt(4, userId);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	/**
	 * アンサーDTOを解答表に追加する
	 * @param AnswerDto アンサーDTO
	 * @return 1 完了 0 エラー
	 * @throws SQLException SQL例外
	 */
	public int insert(AnswerDto dto) throws SQLException {
		return insert(dto.getQuestion_id(), dto.getAnswer(), dto.getCorrect(), dto.getUser_id());
	}

	/**
	 * 解答表の解答、正誤を更新する
	 * @param answer 解答
	 * @param correct 正誤
	 * @return 1 完了 0 エラー
	 * @throws SQLException SQL例外
	 */
	public int update(int answer, boolean correct , int id) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        USER_ANSWER");
		sb.append("    set");
		sb.append("        ANSWER = ?");
		sb.append("       ,CORRECT = ?");
		sb.append("       ,UPDATE_TIME = CURRENT_TIMESTAMP");
		sb.append("  where ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, answer);
			ps.setBoolean(2, correct);
			ps.setInt(3, id);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}
}
