package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kelonos.codetrain.dto.TextDto;

public class TextbookDao {
	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public TextbookDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * 主キーに紐づいたテキストを取得する
	 * @param id
	 * @return テキスト
	 * @throws SQLException SQL例外
	 */
	public TextDto findByPrimaryKey(int id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          id");
		sb.append("         ,title");
		sb.append("         ,text");
		sb.append("         ,curriculum_id");
		sb.append("         ,update_time");
		sb.append("         ,UPDATE_user");
		sb.append("     from text");
		sb.append(" where id = ?");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement stmt = conn.prepareStatement(sb.toString())) {

			stmt.setInt(1, id);

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				TextDto dto = new TextDto();
				dto.setId(rs.getInt("id"));
				dto.setTitle(rs.getString("title"));
				dto.setText(rs.getString("text"));
				dto.setCurriculum_id(rs.getInt("curriculum_id"));
				dto.setUpdate_time(rs.getTimestamp("update_time"));
				dto.setUpdate_user(rs.getInt("update_user"));
				return dto;
			}
			return null;
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * テキストを取得する
	 * @param ? ?
	 * @param ? ?
	 * @return テキスト一覧
	 * @throws SQLException SQL例外
	 */
	public List<TextDto> findAll() throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
    	sb.append(" select");
    	sb.append("       text.id");
    	sb.append("       ,text.title");
    	sb.append("       ,text.text");
     	sb.append("       ,text.curriculum_id");
    	sb.append("       ,text.UPDATE_TIME");
    	sb.append("       ,text.UPDATE_USER");
    	sb.append("       ,category.name as category_name");
    	sb.append("       ,course.name as course_name");
    	sb.append("   from (((text inner join curriculum on text.curriculum_id = curriculum.id) inner join course on curriculum.course_id = course.id) inner join category on course.category_id = category.id)");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			List<TextDto> list = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				TextDto text = new TextDto();
				text.setId(rs.getInt("ID"));
				text.setTitle(rs.getString("title"));
				text.setText(rs.getString("TEXT"));
				text.setCurriculum_id(rs.getInt("curriculum_id"));
				text.setUpdate_time(rs.getTimestamp("text.update_time"));
				text.setUpdate_user(rs.getInt("text.update_user"));
				text.setCategory_name(rs.getString("category_name"));
				text.setCourse_name(rs.getString("course_name"));

				list.add(text);
			}
			return list;
		}
	}

	/**
	 * テキスト情報を追加する
	 * @param dto テキスト情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insert(TextDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into text");
		sb.append("           (");
		sb.append("             title");
		sb.append("            ,text");
		sb.append("            ,curriculum_id");
		sb.append("            ,update_user");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getTitle());
			ps.setString(2, dto.getText());
			ps.setInt(3, dto.getCurriculum_id());
			ps.setInt(4, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * テキスト情報を更新する
	 * @param dto テキスト情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int update(TextDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update text set");
		sb.append("             title = ?");
		sb.append("            ,text = ?");
		sb.append("            ,curriculum_id = ?");
		sb.append("            ,update_user = ?");
		sb.append(" where id  = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getTitle());
			ps.setString(2, dto.getText());
			ps.setInt(3, dto.getCurriculum_id());
			ps.setInt(4, dto.getUpdate_user());
			ps.setInt(5, dto.getId());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

}
