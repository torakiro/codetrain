package jp.kelonos.codetrain.dao.test;

import java.io.File;
import java.sql.Connection;

import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.UserDto;

public class UserDaoTest {

	private IDatabaseTester databaseTester;
	private IDatabaseConnection connection;

	private static final String TESTDATA_FOLDER_PATH = "WebContent/WEB-INF/testdata/dao/";

	/*------------------------------------------------------------------------
	 * todo: テスト対象のDAOクラスのクラス名をこの変数に入れてください
	 * ただし、小文字で、「Dao」や「.java」は入れない。
	 * 例）RequestReplyDao　→　requestreply
	 */
	private static final String UNDERTEST_CLASSNAME = "user";
	//------------------------------------------------------------------------

	private static final String SUFFIX_EXPECTED_DATA = "expected";
	private static final String TESTDATA_FILE_SUFFIX = ".xls";

	public UserDaoTest() throws Exception {
		databaseTester = new JdbcDatabaseTester(
				DataSourceManager.getContextResourceAttribute("driverClassName"),
				DataSourceManager.getContextResourceAttribute("url"),
				DataSourceManager.getContextResourceAttribute("username"),
				DataSourceManager.getContextResourceAttribute("password"));
	}

	@Before
	public void before() throws Exception {
		//テーブルに初期化用のデータを投入する
		IDataSet dataSet =
				new XlsDataSet(new File(TESTDATA_FOLDER_PATH + UNDERTEST_CLASSNAME + TESTDATA_FILE_SUFFIX));
		databaseTester.setDataSet(dataSet);
		databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);

		databaseTester.onSetup();
	}

	@After
	public void after() throws Exception {
		databaseTester.setTearDownOperation(DatabaseOperation.NONE);
		databaseTester.onTearDown();
	}

	@Test
	public void updateStateById() throws Exception {

		final String UNDERTEST_METHODNAME = "_" + Thread.currentThread().getStackTrace()[1].getMethodName().toLowerCase() + "_";

		IDatabaseConnection testerConnection = databaseTester.getConnection();
		Connection con = testerConnection.getConnection();
		IDataSet databaseDataSet = databaseTester.getConnection().createDataSet();

		/*-----------------------------------------------------------------------
		 * todo: テストするデータベース操作をここに入力してください。
		 * 例）DTO作成　→　テスト対象メソッド呼び出し、等
		 */
		UserDto dto = new UserDto();
		dto.setId(1);
		dto.setState(0);
		dto.setUpdate_user(0);
		UserDao sut = new UserDao(con);
		sut.updateStateById(dto);
		//-----------------------------------------------------------------------

		ITable actualTable = databaseDataSet.getTable("user");
		IDataSet expectedDataSet = new XlsDataSet(new File(TESTDATA_FOLDER_PATH + UNDERTEST_CLASSNAME + UNDERTEST_METHODNAME + SUFFIX_EXPECTED_DATA + TESTDATA_FILE_SUFFIX));
		ITable expectedTable = expectedDataSet.getTable("user");

		/*-----------------------------------------------------------------------
		 * todo: テスト対象に含めないテーブルカラムがあればここで書きます。
		 * 例）update_timeカラム、idカラムを除外する場合
		 * 　　→　String[] { "update_time", "id" })
		 */
		ITable filteredExpectedTable = DefaultColumnFilter.excludedColumnsTable(expectedTable,
				new String[] { "login_id", "name","password","name","passwordsetFlg","deleteFlg","corp_id","initial_password_id","empno","update_time" });
		ITable filteredActualTable = DefaultColumnFilter.excludedColumnsTable(actualTable,
				new String[] { "login_id", "name","password","name","passwordsetFlg","deleteFlg","corp_id","initial_password_id","empno","update_time" });
		//-----------------------------------------------------------------------

		/*-----------------------------------------------------------------------
		 * todo: ここも適宜、テスト内容に応じて変えてください。
		 */
		Assertion.assertEquals(filteredExpectedTable, filteredActualTable);
		//-----------------------------------------------------------------------

	}

}
