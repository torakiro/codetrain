package jp.kelonos.codetrain.dao.test;

import java.io.File;
import java.sql.Connection;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.RequestReplyDao;
import jp.kelonos.codetrain.dto.RequestReplyDto;

public class RequestReplyDaoTest {

	private IDatabaseTester databaseTester;
	private IDatabaseConnection connection;

	private static final String TESTDATA_FOLDER_PATH = "WebContent/WEB-INF/testdata/dao/";

	/*------------------------------------------------------------------------
	 * todo: テスト対象のDAOクラスのクラス名をこの変数に入れてください
	 * ただし、小文字で、「Dao」や「.java」は入れない。
	 * 例）RequestReplyDao　→　requestreply
	 */
	private static final String UNDERTEST_CLASSNAME = "requestreply";
	//------------------------------------------------------------------------

	private static final String SUFFIX_EXPECTED_DATA = "expected";
	private static final String TESTDATA_FILE_SUFFIX = ".xls";

	public RequestReplyDaoTest() throws Exception {
		//テストクラスをインスタンス化するときに、DBに接続するためのtesterを作成する
		databaseTester = new JdbcDatabaseTester(
				DataSourceManager.getContextResourceAttribute("driverClassName"),
				DataSourceManager.getContextResourceAttribute("url"),
				DataSourceManager.getContextResourceAttribute("username"),
				DataSourceManager.getContextResourceAttribute("password"));
	}

	@Before
	public void before() throws Exception {
		//テーブルに初期化用のデータを投入する
		IDataSet dataSet =
				new XlsDataSet(new File(TESTDATA_FOLDER_PATH + UNDERTEST_CLASSNAME + TESTDATA_FILE_SUFFIX));
		databaseTester.setDataSet(dataSet);
		databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);

		databaseTester.onSetup();
	}

	@After
	public void after() throws Exception {
		databaseTester.setTearDownOperation(DatabaseOperation.NONE);
		databaseTester.onTearDown();
	}

	@Test
	public void insert() throws Exception {

		final String UNDERTEST_METHODNAME = "_" + Thread.currentThread().getStackTrace()[1].getMethodName().toLowerCase() + "_";

		IDatabaseConnection testerConnection = databaseTester.getConnection();
		Connection con = testerConnection.getConnection();
		IDataSet databaseDataSet = databaseTester.getConnection().createDataSet();

		/*-----------------------------------------------------------------------
		 * todo: テストするデータベース操作をここに入力してください。
		 * 例）DTO作成　→　テスト対象メソッド呼び出し、等
		 */
		RequestReplyDto dto = new RequestReplyDto();
		dto.setId(4);
		dto.setText("あいう");
		dto.setCreated_at(Timestamp.valueOf("2018-06-01 10:00:00"));
		dto.setRequest_id(1);
		dto.setUpdate_time(Timestamp.valueOf(LocalDateTime.now()));
		dto.setUpdate_user(1);
		RequestReplyDao sut = new RequestReplyDao(con);
		sut.insert(dto);
		//-----------------------------------------------------------------------

		ITable actualTable = databaseDataSet.getTable("request_reply");
		IDataSet expectedDataSet = new XlsDataSet(new File(TESTDATA_FOLDER_PATH + UNDERTEST_CLASSNAME + UNDERTEST_METHODNAME + SUFFIX_EXPECTED_DATA + TESTDATA_FILE_SUFFIX));
		ITable expectedTable = expectedDataSet.getTable("request_reply");

		/*-----------------------------------------------------------------------
		 * todo: テスト対象に含めないテーブルカラムがあればここで書きます。
		 * 例）update_timeカラム、idカラムを除外する場合
		 * 　　→　String[] { "update_time", "id" })
		 */
		ITable filteredExpectedTable = DefaultColumnFilter.excludedColumnsTable(expectedTable,
				new String[] { "update_time", "id" });
		ITable filteredActualTable = DefaultColumnFilter.excludedColumnsTable(actualTable,
				new String[] { "update_time", "id" });
		//-----------------------------------------------------------------------

		/*-----------------------------------------------------------------------
		 * todo: ここも適宜、テスト内容に応じて変えてください。
		 */
		Assertion.assertEquals(filteredExpectedTable, filteredActualTable);
		//-----------------------------------------------------------------------

	}

}
