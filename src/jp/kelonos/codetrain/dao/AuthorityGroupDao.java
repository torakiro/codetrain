package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kelonos.codetrain.dto.AuthorityGroupDto;

public class AuthorityGroupDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public AuthorityGroupDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * 権限グループをすべて取得する
	 * @return 権限グループ情報リスト
	 * @throws SQLException SQL例外
	 */
	public List<AuthorityGroupDto> findAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        id");
		sb.append("        ,name as authority_name");
		sb.append("        ,show_operatorlist");
		sb.append("        ,create_operator");
		sb.append("        ,show_corplist");
		sb.append("        ,create_corp");
		sb.append("        ,create_text");
		sb.append("        ,edit_text");
		sb.append("        ,show_text");
		sb.append("        ,reply_message");
		sb.append("        ,show_message");
		sb.append("        ,show_message_userlist");
		sb.append("        ,control_billing");
		sb.append("        ,update_time");
		sb.append("        ,update_user");
		sb.append("   from authority_group");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			List<AuthorityGroupDto> list = new ArrayList<>();

			// 結果をDTOに詰める
			while (rs.next()) {
				AuthorityGroupDto dto = new AuthorityGroupDto();

				dto = StaffDao.setAuthority(dto, rs);
				dto.setId(rs.getInt("id"));
				dto.setUpdate_time(rs.getTimestamp("update_time"));
				dto.setUpdate_user(rs.getInt("update_user"));

				list.add(dto);
			}
			return list;
		}
	}

	/**
	 * IDに紐づいた権限グループを取得する
	 * @param 主キー
	 * @return 権限グループ情報リスト
	 * @throws SQLException SQL例外
	 */
	public AuthorityGroupDto selectByPrimaryKey(int id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        id");
		sb.append("        ,name as authority_name");
		sb.append("        ,show_operatorlist");
		sb.append("        ,create_operator");
		sb.append("        ,show_corplist");
		sb.append("        ,create_corp");
		sb.append("        ,create_text");
		sb.append("        ,edit_text");
		sb.append("        ,show_text");
		sb.append("        ,reply_message");
		sb.append("        ,show_message");
		sb.append("        ,show_message_userlist");
		sb.append("        ,control_billing");
		sb.append("        ,update_time");
		sb.append("        ,update_user");
		sb.append("   from authority_group");
		sb.append(" where id = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			while (rs.next()) {
				AuthorityGroupDto dto = new AuthorityGroupDto();

				dto = StaffDao.setAuthority(dto, rs);
				dto.setId(rs.getInt("id"));
		    	dto.setName(rs.getString("authority_name"));
		    	dto.setShow_operatorlist(rs.getBoolean("show_operatorlist"));
		    	dto.setCreate_operator(rs.getBoolean("create_operator"));
		    	dto.setShow_corplist(rs.getBoolean("show_corplist"));
		    	dto.setCreate_corp(rs.getBoolean("create_corp"));
		    	dto.setCreate_text(rs.getBoolean("create_text"));
		    	dto.setEdit_text(rs.getBoolean("edit_text"));
		    	dto.setShow_text(rs.getBoolean("show_text"));
		    	dto.setReply_message(rs.getBoolean("reply_message"));
		    	dto.setShow_message(rs.getBoolean("show_message"));
		    	dto.setShow_message_userlist(rs.getBoolean("show_message_userlist"));
		    	dto.setControl_billing(rs.getBoolean("conrol_billing"));
				dto.setUpdate_time(rs.getTimestamp("update_time"));
				dto.setUpdate_user(rs.getInt("update_user"));

				return dto;
			}
			return null;
		}
	}

}
