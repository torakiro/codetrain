package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kelonos.codetrain.dto.ReasonSuspendDto;

public class ReasonSuspendDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public ReasonSuspendDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * カリキュラムIDとユーザーIDに紐づく解答情報リストを取得する
	 * @param curriculumId カリキュラムID
	 * @param userId ユーザーID
	 * @return 解答情報リスト
	 * @throws SQLException SQL例外
	 */
	public List<ReasonSuspendDto> selectByCorpId(int corp_id) throws SQLException{
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        user.id as user_id");
		sb.append("        ,usersuspend.id as id");
		sb.append("        ,user.state");
		sb.append("        ,user.corp_id");
		sb.append("        ,user.name");
		sb.append("        ,user.empno");
		sb.append("        ,user.suspend_month");
		sb.append("        ,usersuspend.reason");
		sb.append("        ,usersuspend.update_time");
		sb.append("        ,usersuspend.update_user");
		sb.append("   from user");
		sb.append("    inner join usersuspend on user.id = usersuspend.user_id");
		sb.append("   where user.deleteFlg = false and user.corp_id = ?");
		sb.append("   group by usersuspend.user_id");
		sb.append("   order by usersuspend.update_time, suspend_month desc");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, corp_id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			List<ReasonSuspendDto> list = new ArrayList<>();
			// 結果をDTOに詰める
			while (rs.next()) {
				ReasonSuspendDto dto = new ReasonSuspendDto();
				dto.setId(rs.getInt("id"));
				dto.setUser_id(rs.getInt("user_id"));
				dto.setState(rs.getInt("state"));
				dto.setCorp_id(rs.getInt("corp_id"));
				dto.setUser_name(rs.getString("name"));
				dto.setSuspend_month(rs.getDate("suspend_month"));
				dto.setReason(rs.getString("reason"));
				dto.setUpdate_time(rs.getTimestamp("update_time"));
				dto.setEmpno(rs.getInt("empno"));
				if (rs.wasNull())
					dto.setEmpno(null);
				dto.setUpdate_user(rs.getInt("update_user"));
				list.add(dto);
			}
			return list;
		}
	}

}
