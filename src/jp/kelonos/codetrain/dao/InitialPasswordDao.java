package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jp.kelonos.codetrain.dto.InitialPasswordDto;

/**
 * 初期パスワードテーブルのDataAccessObject
 * @author Mr.X
 */
public class InitialPasswordDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public InitialPasswordDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * 主キーを指定して初期パスワード情報を追加する
	 * @param dto 初期パスワード情報
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int insertWithPrimaryKey(InitialPasswordDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into initialpassword");
		sb.append(" (");
		sb.append(" id");
		sb.append(" ,initial_password");
		sb.append(" ,update_user");
		sb.append(" )");
		sb.append(" values");
		sb.append(" (");
		sb.append(" ?");
		sb.append(",?");
		sb.append(",?");
		sb.append(" )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getInitial_password());
			ps.setInt(3, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * auto_incrementの値を取得する
	 * @return auto_incrementの値。存在しない場合はnull
	 * @throws SQLException SQL例外
	 */
	public Integer selectAutoIncrement() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("select auto_increment from information_schema.tables where table_name='");
		sb.append("initialpassword");
		sb.append("'");

		// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());

			Integer result = null;
			while (rs.next()) {
				result = rs.getInt("auto_increment");
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}
}
