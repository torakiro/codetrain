package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jp.kelonos.codetrain.dto.CurriculumDto;

public class CurriculumDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public CurriculumDao(Connection conn) {
		this.conn = conn;
	}

	public CurriculumDto findById(int id) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,NAME");
		sb.append("       ,COURSE_ID");
		sb.append("       ,EXAMFLG");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from CURRICULUM ");
		sb.append("  where ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				CurriculumDto curriculum = new CurriculumDto();
				curriculum.setId(rs.getInt("ID"));
				curriculum.setName(rs.getString("NAME"));
				curriculum.setCourse_id(rs.getInt("COURSE_ID"));
				curriculum.setExamFlg(rs.getBoolean("EXAMFLG"));
				curriculum.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				curriculum.setUpdate_user(rs.getInt("UPDATE_USER"));
				return curriculum;
			}
			return null;
		}
	}

	/**
	 * コースIDに紐づくカリキュラム情報を取得する
	 * @param courceId コースID
	 * @return カリキュラム情報リスト
	 * @throws SQLException SQL例外
	 */
	public ArrayList<CurriculumDto> findByCourseId(int courceId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,NAME");
		sb.append("       ,COURSE_ID");
		sb.append("       ,EXAMFLG");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from CURRICULUM ");
		sb.append("  where COURSE_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, courceId);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			ArrayList<CurriculumDto> curriculumDtos = new ArrayList<>();
			// 結果をDTOに詰める
			while (rs.next()) {
				CurriculumDto curriculum = new CurriculumDto();
				curriculum.setId(rs.getInt("ID"));
				curriculum.setName(rs.getString("NAME"));
				curriculum.setCourse_id(rs.getInt("COURSE_ID"));
				curriculum.setExamFlg(rs.getBoolean("EXAMFLG"));
				curriculum.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				curriculum.setUpdate_user(rs.getInt("UPDATE_USER"));
				curriculumDtos.add(curriculum);
			}
			// 該当するデータがない場合はnullを返却する
			return curriculumDtos;
		}
	}

	/**
	 * カリキュラムを追加する
	 * @param dto カリキュラム情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int insertWithPrimaryKey(CurriculumDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into curriculum");
		sb.append("           (");
		sb.append("            id");
		sb.append("            ,name");
		sb.append("            ,course_id");
		sb.append("            ,examFlg");
		sb.append("            ,UPDATE_USER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getName());
			ps.setInt(3, dto.getCourse_id());
			ps.setBoolean(4, dto.isExamFlg());
			ps.setInt(5, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * コースIDに紐づくカリキュラム情報を取得する
	 * @param courceId コースID
	 * @return カリキュラム情報
	 * @throws SQLException SQL例外
	 */
	public CurriculumDto findByPrimaryKey(int id) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,NAME");
		sb.append("       ,COURSE_ID");
		sb.append("       ,examFlg");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from CURRICULUM ");
		sb.append("  where ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			while (rs.next()) {
				CurriculumDto curriculum = new CurriculumDto();
				curriculum.setId(rs.getInt("ID"));
				curriculum.setName(rs.getString("NAME"));
				curriculum.setCourse_id(rs.getInt("COURSE_ID"));
				curriculum.setExamFlg(rs.getBoolean("examFlg"));
				curriculum.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				curriculum.setUpdate_user(rs.getInt("UPDATE_USER"));
				return curriculum;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	/**
	 * カリキュラムを更新する
	 * @param dto カリキュラム情報
	 * @return 更新件数
	 * @throws SQLException SQL例外
	 */
	public int update(CurriculumDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update curriculum set");
		sb.append("            name = ?");
		sb.append("            ,course_id = ?");
		sb.append("            ,examFlg = ?");
		sb.append("            ,UPDATE_USER = ?");
		sb.append(" where id = ?");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getName());
			ps.setInt(2, dto.getCourse_id());
			ps.setBoolean(3, dto.isExamFlg());
			ps.setInt(4, dto.getUpdate_user());
			ps.setInt(5, dto.getId());

			// SQLを実行する
			return ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	/**
	 * auto_incrementの値を取得する
	 * @return auto_incrementの値。存在しない場合はnull
	 * @throws SQLException SQL例外
	 */
	public Integer selectAutoIncrement() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("select auto_increment from information_schema.tables where table_name='");
		sb.append("curriculum");
		sb.append("'");

    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());

			Integer result = null;
			while (rs.next()) {
				result = rs.getInt("auto_increment");
			}
			return result;
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}
}
