package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.kelonos.codetrain.dto.TextDto;


public class TextDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public TextDao(Connection conn) {
		this.conn = conn;
	}

	public TextDto findByCurriculumId(int curriculumId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        ID");
		sb.append("       ,TITLE");
		sb.append("       ,TEXT");
		sb.append("       ,CURRICULUM_ID");
		sb.append("       ,UPDATE_TIME");
		sb.append("       ,UPDATE_USER");
		sb.append("   from TEXT");
		sb.append("   where CURRICULUM_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setInt(1, curriculumId);
			// SQLを実行する
			ResultSet rs = ps.executeQuery();
			// 結果をDTOに詰める
			if (rs.next()) {
				TextDto text = new TextDto();
				text.setId(rs.getInt("ID"));
				text.setTitle(rs.getString("TITLE"));
				text.setText(rs.getString("TEXT"));
				text.setCurriculum_id(rs.getInt("CURRICULUM_ID"));
				text.setUpdate_time(rs.getTimestamp("UPDATE_TIME"));
				text.setUpdate_user(rs.getInt("UPDATE_USER"));
				return text;
			}
			return null;
		}
	}
}
