package jp.kelonos.codetrain.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.kelonos.codetrain.dto.UserSuspendDto;

public class UserSuspendDao {
	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public UserSuspendDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * 休止理由を追加する
	 * @param dto 休止理由情報
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int insert(UserSuspendDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("insert into usersuspend");
		sb.append(" (");
		sb.append(" reason");
		sb.append(" ,user_id");
		sb.append(" ,deleteflg");
		sb.append(" ,update_user");
		sb.append(" )");
		sb.append(" values");
		sb.append(" (");
		sb.append(" ?");
		sb.append(",?");
		sb.append(",false");
		sb.append(",?");
		sb.append(" )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getReason());
			ps.setInt(2, dto.getUser_id());
			ps.setInt(3, dto.getUpdate_user());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}
	/**
	 * 休止理由を論理削除する
	 * @param dto 休止理由情報
	 * @return 更新件数
	 * @throws SQLException SQL除外
	 */
	public int logicalDelete(UserSuspendDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("update usersuspend");
		sb.append(" set");
		sb.append(" deleteflg = true");
		sb.append(" where user_id = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getUser_id());

			// SQLを実行する
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}

	}
}
