package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class OperatorChangePasswordServlet
 */
@WebServlet("/user-change-password")
public class UserChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(UserChangePasswordServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		Common.logout(Common.USER, response);

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		int maxLength = 16;

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (!Common.isUserLoggedIn(session, response, logger))
			return;

		UserDto dto = (UserDto) session.getAttribute("user");
		if (dto == null) {
			response.sendRedirect("user_index.jsp");
			return;
		}
		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		String oldpass = request.getParameter("old_password");
		String pass1 = request.getParameter("password1");
		String pass2 = request.getParameter("password2");
		String name = request.getParameter("name");

		if (oldpass == null || pass1 == null || pass2 == null) {
			// パスワード変更画面へ遷移
			request.getRequestDispatcher("WEB-INF/user-change-password.jsp").forward(request, response);
		}

		// 入力チェック
		if (!pass1.equals(pass2)) {
			session.setAttribute("errorMessage", "新しいパスワードが確認用パスワードと一致しません");
		}

		if (name != null &&"".equals(replaceSpace(name))) {
			session.setAttribute("errorMessage", "氏名を入力してください");
		}

		if ("".equals(replaceSpace(pass2))) {
			session.setAttribute("errorMessage", "確認用パスワードを入力してください");
		}

		if ("".equals(replaceSpace(pass1))) {
			session.setAttribute("errorMessage", "新しいパスワードを入力してください");
		}

		if ("".equals(replaceSpace(oldpass))) {
			session.setAttribute("errorMessage", "現在のパスワードを入力してください");
		}

		if (oldpass.length() > maxLength || pass1.length() > maxLength || pass2.length() > maxLength
                ||  (name != null && name.length() > maxLength)) {
			session.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (session.getAttribute("errorMessage") != null) {
			logger.info("パスワード変更失敗 {}", request.getRemoteAddr());
			// パスワード変更画面に戻る
			request.getRequestDispatcher("WEB-INF/user-change-password.jsp").forward(request, response);
			return;
		}
		//新しいパスワードを格納＋パスワード設定済みフラグ更新
		dto.setPassword(pass1);
		dto.setUpdate_user(dto.getId());

		if (name != null) {
			dto.setName(name);
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			UserDao dao = new UserDao(conn);

			if (!dto.isPasswordsetFlg()) {
				dto.setPasswordsetFlg(true);

				if (dao.updateUser(dto, oldpass) == 0) {
					// パスワード変更に失敗
					session.setAttribute("errorMessage", "現在のパスワードが間違っています");
					dto.setPasswordsetFlg(false);

					// パスワード変更画面に戻る
					request.getRequestDispatcher("WEB-INF/user-change-password.jsp").forward(request, response);
					return;
				}
			} else if (dao.updatePassword(dto, oldpass) == 0) {
				// パスワード変更に失敗
				session.setAttribute("errorMessage", "現在のパスワードが間違っています");

				// パスワード変更画面に戻る
				request.getRequestDispatcher("WEB-INF/user-change-password.jsp").forward(request, response);
				return;
			}

			session.removeAttribute("errorMessage");

			// コース一覧画面に遷移する
			response.sendRedirect("user_index.jsp");
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("system-error.jsp");
			return;
		}
	}

	private String replaceSpace(String str) {
		String string = str;
		string = string.replaceAll(" ", "");
		string = string.replaceAll("　", "");
		return string;
	}
}
