package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.BillingDao;
import jp.kelonos.codetrain.dao.PriceDao;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.BillingDto;
import jp.kelonos.codetrain.dto.CorpPersonDto;
import jp.kelonos.codetrain.dto.PriceDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class ListUserServlet
 */
@WebServlet("/show-billing")
public class ShowBillingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CustomerLoginServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShowBillingServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (!Common.isCustomerLoggedIn(session, response, logger)) return;

		CorpPersonDto customer = (CorpPersonDto)session.getAttribute("customer");

		try (Connection conn = DataSourceManager.getConnection()) {

			try {

				// 請求一覧を取得する
				BillingDao billingDao = new BillingDao(conn);
				UserDao userDao = new UserDao(conn);
				PriceDao priceDao = new PriceDao(conn);

				// トランザクションを開始する
				conn.setAutoCommit(false);

				int count = 0;

				List<BillingDto> billing = billingDao.selectByCorpId(customer.getCorp_id());
				boolean isThisMonthSet = false;
				for (BillingDto dto : billing) {
					if (DateTimeFormatter.ofPattern("YYYYM").format(dto.getBilling_month().toLocalDate()).equals(DateTimeFormatter.ofPattern("YYYYM").format(LocalDateTime.now().toLocalDate()))) {
						isThisMonthSet = true;
						break;
					}
				}

				PriceDto priceDto = priceDao.get();

				BillingDto dto = new BillingDto();
				dto.setCnt_user_active(userDao.selectCountByStateAndCorpId(UserDto.STATE_ACTIVE, customer.getCorp_id()));
				dto.setCnt_user_suspended(userDao.selectCountByStateAndCorpId(UserDto.STATE_SUSPENDED, customer.getCorp_id()));
				dto.setCnt_user_newly_suspended(userDao.selectNewlySuspendedCountByCorpId(customer.getCorp_id(), Date.valueOf(LocalDateTime.now().toLocalDate())));
				dto.setPrice_user_active(dto.getCnt_user_active() * priceDto.getMonthlyprice_active_user());
				dto.setPrice_user_suspended(dto.getCnt_user_suspended() * priceDto.getMonthlyprice_suspended_user());
				dto.setPrice_user_newly_suspended(dto.getCnt_user_newly_suspended() * (priceDto.getMonthlyprice_active_user() - priceDto.getMonthlyprice_suspended_user()));
				dto.setPrice(dto.getPrice_user_active() + dto.getPrice_user_suspended() + dto.getPrice_user_newly_suspended());
				dto.setBilling_month(Date.valueOf(LocalDateTime.now().toLocalDate()));
				dto.setCorp_id(customer.getCorp_id());
				dto.setUpdate_user(customer.getId());
				dto.setBilledFlg(false);
				if (!isThisMonthSet) {

					count = billingDao.insert(dto);
					if (count == 0) throw new SQLException("排他制御（楽観ロック）例外");

				}
				else if(true) {

					count = billingDao.updateByBillingMonthAndCorpId(dto);
					if (count == 0) throw new SQLException("排他制御（楽観ロック）例外");

				}
				billing = billingDao.selectByCorpId(customer.getCorp_id());

				// 請求一覧情報をリクエストに保持する
				request.setAttribute("billing", billing);
				// 価格情報をリクエストに保持する
				request.setAttribute("price", priceDto);
				// コミットする
				conn.commit();

			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("customer-system-error.jsp").forward(request, response);
			return;
		}

		// 請求一覧画面に遷移する
		request.getRequestDispatcher("WEB-INF/show-billing.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
