package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CorpPersonDao;

/**
 * Servlet implementation class CustomerChangePasswordServlet
 */
@WebServlet("/customer-change-password")
public class CustomerChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CustomerLoginServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerChangePasswordServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// ログイン画面へ遷移する
		Common.logout(Common.CUSTOMER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// フォームのデータを取得する
		int id = Integer.parseInt(request.getParameter("id"));
		String password = request.getParameter("password");
		String newPassword = request.getParameter("new-password");
		String passwordCheck = request.getParameter("password-check");
		String loginNow = request.getParameter("login-now");

		// ログイン中の場合
		if ("1".equals(loginNow)) {
			// パスワード変更画面へ遷移する
			request.getRequestDispatcher("WEB-INF/customer-change-password.jsp").forward(request, response);
			return;
		}
		// セッションを取得する
		HttpSession session = request.getSession(true);

		// 入力チェック
		if (!newPassword.equals(passwordCheck)) {
			session.setAttribute("errorMessage", "新しいパスワードと確認用パスワードが一致しません");
		}

		if ("".equals(passwordCheck)) {
			session.setAttribute("errorMessage", "確認用パスワードを入力してください");
		}

		if ("".equals(newPassword)) {
			session.setAttribute("errorMessage", "新しいパスワードを入力してください");
		}

		if ("".equals(password)) {
			session.setAttribute("errorMessage", "現在のパスワードを入力してください");
		}

		if (password.length() > 16 || newPassword.length() > 16 || passwordCheck.length() > 16) {
			session.setAttribute("errorMessage", "入力文字数が多いです");
		}

		if (session.getAttribute("errorMessage") != null) {
			logger.info("パスワード変更失敗 {}", request.getRemoteAddr());
			// パスワード変更画面へ遷移する
			request.getRequestDispatcher("WEB-INF/customer-change-password.jsp").forward(request, response);
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// パスワード更新処理
			CorpPersonDao corpPersonDao = new CorpPersonDao(conn);
			int count = corpPersonDao.updatePasswordByIdAndPassword(id, password, newPassword);

			// パスワードの更新が失敗した場合
			if (count == 0) {
				session.setAttribute("errorMessage", "現在のパスワードに誤りがあります");

				// パスワード変更画面へ遷移する
				request.getRequestDispatcher("WEB-INF/customer-change-password.jsp").forward(request, response);
				return;
			}

			// ログイン画面へ遷移する
			request.getRequestDispatcher("customer-login.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("customer-system-error.jsp");
			return;
		}
	}

}
