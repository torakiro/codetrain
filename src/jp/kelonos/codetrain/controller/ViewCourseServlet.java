package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CategoryDao;
import jp.kelonos.codetrain.dao.CourseDao;
import jp.kelonos.codetrain.dao.CurriculumDao;
import jp.kelonos.codetrain.dao.LearnHistoryDao;
import jp.kelonos.codetrain.dto.CategoryDto;
import jp.kelonos.codetrain.dto.CourseDto;
import jp.kelonos.codetrain.dto.CurriculumDto;
import jp.kelonos.codetrain.dto.LearnHistoryDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/view-course")
public class ViewCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(UserChangePasswordServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session == null) {
			//セッションが消えた時
			errorHandling(request, response, "courseMessage", "セッションタイムアウト", "user_index.jsp");
			return;
		}

		int id = 0;
		try {
			//コースIDを取得
			String strId = request.getParameter("courseId");
			if (strId == null || strId.isEmpty()) throw new NumberFormatException();

			id = Integer.parseInt(strId);
		} catch (NumberFormatException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());
			//コースIDにintにできない値が入ったとき
			errorHandling(request, response, "courseMessage", "コースが存在していません", "user_index.jsp");
			return;
		}

		UserDto user = (UserDto) session.getAttribute("user");
		if (user != null) {
			if (!user.isPasswordsetFlg()) {
				response.sendRedirect("logout");
				return;
			}
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			session.removeAttribute("courseMessage");
			CourseDao courseDao = new CourseDao(conn);
			CurriculumDao curriculumDao = new CurriculumDao(conn);
			LearnHistoryDao learnHistoryDao = new LearnHistoryDao(conn);

			//コースの詳細を取得
			CourseDto course = courseDao.findById(id);
			//コースごとのカリキュラムを取得
			ArrayList<CurriculumDto> curriculumDtos = curriculumDao.findByCourseId(id);

			if (course == null) {
				//存在していないコースを見ようとしたとき
				// トップページに遷移する
				errorHandling(request, response, "courseMessage", "コースが存在していません", "user_index.jsp");
				return;
			}
			if (user == null) {
				if (!course.isFreecourseFlg()) {
					//ユーザーが未ログインでフリーコースじゃないコースを見ようとしたとき
					// トップページに遷移する
					errorHandling(request, response, "courseMessage", "フリーコースしか閲覧できません", "user_index.jsp");
					return;
				}
			} else {
				//現在のコースでユーザーの最後の学習履歴を取得し、リクエストに保持する
				LearnHistoryDto historyDto = learnHistoryDao.findByUserIdLast(user.getId(), course.getId());
				request.setAttribute("history", historyDto);

				//現在のコースでユーザーの全ての学習履歴を取得し、リクエストに保持する
				ArrayList<LearnHistoryDto> historyDtos = learnHistoryDao.findByUserIdAndCourseIdAll(user.getId(), course.getId());
				request.setAttribute("historys",historyDtos);
			}

			CategoryDao categoryDao = new CategoryDao(conn);
			CategoryDto categoryDto = categoryDao.findById(course.getCategory_id());

			//カテゴリをリクエストに保持する
			request.setAttribute("category", categoryDto);

			//コース詳細をリクエストに保持する
			request.setAttribute("course", course);

			//カリキュラム一覧データをリクエストに保持する
			request.setAttribute("curriculumList", curriculumDtos);

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI() + "?" + request.getQueryString());

			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("/WEB-INF/view-course.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// トップページに遷移する
			errorHandling(request, response, "courseMessage", "エラー", "user_index.jsp");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private void errorHandling(HttpServletRequest request, HttpServletResponse response, String errorField,
			String errorMessage, String redirectTarget)
			throws IOException {
		HttpSession session = request.getSession(true);
		session.setAttribute(errorField, errorMessage);
		response.sendRedirect(redirectTarget);
	}
}
