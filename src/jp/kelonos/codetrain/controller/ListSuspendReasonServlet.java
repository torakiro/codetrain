package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.ReasonSuspendDao;
import jp.kelonos.codetrain.dto.ReasonSuspendDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class ListUserServlet
 */
@WebServlet("/list-suspend-reason")
public class ListSuspendReasonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CustomerLoginServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListSuspendReasonServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (!Common.isOperatorLoggedIn(session, response, logger)) return;

		StaffDto staff = (StaffDto)session.getAttribute("operator");
		if (!staff.getAuthority_group().canControl_billing()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		request.setCharacterEncoding("UTF-8");

		int corp_id = 0;
		try {
			corp_id = Integer.parseInt(request.getParameter("corp_id"));
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
			Common.logout(Common.OPERATION, response);
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			ReasonSuspendDao dao = new ReasonSuspendDao(conn);
			List<ReasonSuspendDto> list = new ArrayList<>();

			list = dao.selectByCorpId(corp_id);

			// 利用者一覧情報をリクエストに保持する
			request.setAttribute("list", list);

			request.setAttribute("corp_id", request.getParameter("corp_id"));
			request.setAttribute("corp_name", request.getParameter("corp_name"));

			// 利用者一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-suspend-reason.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());
			e.printStackTrace(System.out);

			// システムエラーに遷移する
			response.sendRedirect("operation-system-error.jsp");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Common.logout(Common.OPERATION, response);
	}

}
