package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/reason-suspend")
public class ReasonSuspendUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ReasonSuspendUserServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.CUSTOMER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// フォームのデータを取得する
		int id = Integer.parseInt(request.getParameter("id"));

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (!Common.isCustomerLoggedIn(session, response, logger)) return;

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 利用者データを取得する
			UserDao userDao = new UserDao(conn);
			UserDto user = userDao.selectByPrimaryKey(id);

			// 利用者データをリクエストに保持する
			request.setAttribute("user", user);

			// 利用者休止画面に遷移する
			request.getRequestDispatcher("WEB-INF/suspend-user.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("customer-system-error.jsp");
			return;
		}

	}
}
