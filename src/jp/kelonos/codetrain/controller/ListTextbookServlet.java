package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.TextbookDao;
import jp.kelonos.codetrain.dto.StaffDto;
import jp.kelonos.codetrain.dto.TextDto;

/**
 * Servlet implementation class ListUserServlet
 */
@WebServlet("/list-text")
public class ListTextbookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListTextbookServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListTextbookServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// セッションを取得する
			HttpSession session = request.getSession(true);
			if (!Common.isOperatorLoggedIn(session, response, logger)) return;

			StaffDto user = (StaffDto)session.getAttribute("operator");
			if (!user.getAuthority_group().canShow_text()) {
				logger.warn("権限なし");
				Common.logout(Common.OPERATION, response);
				return;
			}

			// テキスト一覧を取得する
			TextbookDao textbookDao = new TextbookDao(conn);
			List<TextDto> textList = textbookDao.findAll();

			// テキスト一覧情報をリクエストに保持する
			request.setAttribute("textList", textList);

			// テキスト一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-textbook.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}