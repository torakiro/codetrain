package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/user-login")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(UserChangePasswordServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		// トップページに遷移する
		Common.logout(Common.USER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// フォームのデータを取得する
		String loginId = request.getParameter("id");
		String loginPassword = request.getParameter("password");

		// セッションを取得する
		HttpSession session = request.getSession(true);

		//既存セッション破棄
		session.invalidate();

		//新規セッションを開始
		HttpSession newSession = request.getSession(true);

		// ログインID、パスワードが未入力の場合
		if ("".equals(loginId)) {
			newSession.setAttribute("navbarMessage", "ログインIDを入力してください");

			// ログイン処理前にページ情報が存在しない場合はコース一覧に遷移する
			response.sendRedirect("list-course");
			return;
		}

		if ("".equals(loginPassword)) {
			newSession.setAttribute("navbarMessage", "パスワードを入力してください");

			// ログイン処理前にページ情報が存在しない場合はコース一覧に遷移する
			response.sendRedirect("list-course");
			return;
		}
		if (loginId.length() > 255 || loginPassword.length() > 16) {
			session.setAttribute("navbarMessage", "入力文字数が多いです");
			response.sendRedirect("list-course");
		}
		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			UserDao loginDao = new UserDao(conn);
			UserDto userDto = loginDao.findByIdAndPassword(loginId, loginPassword);

			// ログイン失敗時
			if (userDto == null) {
				newSession.setAttribute("navbarMessage", "ログインIDまたはパスワードが間違っています");
			} else if (userDto.getState() == 1) {
				//ログインしようとしたのが休止状態のとき
				newSession.setAttribute("navbarMessage", "アカウントが休止状態なのでログインできません");
				userDto = null;
			} else if (!userDto.isPasswordsetFlg()) {// 初回ログイン時
				//セッションにユーザー情報セット
				newSession.setAttribute("user", userDto);
				request.getRequestDispatcher("WEB-INF/user-change-password.jsp").forward(request, response);
				return;
			}

			//セッションにユーザー情報セット
			newSession.setAttribute("user", userDto);

			// ログイン処理前にページ情報が存在しない場合はチャンネル一覧に遷移する
			response.sendRedirect("list-course");

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("system-error.jsp");
			return;
		}
	}

}
