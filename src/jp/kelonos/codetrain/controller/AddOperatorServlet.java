package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.StaffDao;
import jp.kelonos.codetrain.dto.AuthorityGroupDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/add-operator")
public class AddOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddOperatorServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);

		if (!Common.isOperatorLoggedIn(sess, response, logger))
			return;

		StaffDto user = (StaffDto) sess.getAttribute("operator");
		if (!user.getAuthority_group().canCreate_operator()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		StaffDto newStaff = new StaffDto();
		AuthorityGroupDto authority = new AuthorityGroupDto();
		try {
			authority.setId(Integer.parseInt(request.getParameter("authority_id")));
		} catch (Exception e) {
			e.printStackTrace(System.out);
			Common.logout(Common.OPERATION, response);
			return;
		}
		if (authority.getId() < 1) {
			Common.logout(Common.OPERATION, response);
			return;
		}

		newStaff.setName(request.getParameter("name"));
		newStaff.setLogin_id(request.getParameter("login_id"));
		newStaff.setPassword(request.getParameter("password"));
		newStaff.setAuthority_group(authority);

		sess.removeAttribute("errorMessage");
		sess.removeAttribute("message");

		// 入力チェック
		if (request.getParameter("password") == null || request.getParameter("password_confirm") == null
				|| "".equals(request.getParameter("password")) || "".equals(request.getParameter("password_confirm"))) {
			sess.setAttribute("errorMessage", "パスワード、確認用パスワードを入力してください");
		} else if (!request.getParameter("password").equals(request.getParameter("password_confirm"))) {
			sess.setAttribute("errorMessage", "パスワードが確認用パスワードと一致しません");
		}
		if (newStaff.getLogin_id() == null || "".equals(newStaff.getLogin_id())) {
			sess.setAttribute("errorMessage", "ログインIDを入力してください");
		}
		if (newStaff.getName() == null || "".equals(newStaff.getName())) {
			sess.setAttribute("errorMessage", "氏名を入力してください");
		}

		if (newStaff.getLogin_id().length() > 255 || newStaff.getPassword().length() > 16
				|| newStaff.getName().length() > 16) {
			sess.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (sess.getAttribute("errorMessage") != null) {
			// 入力画面に戻る
			sess.setAttribute("name", newStaff.getName());
			sess.setAttribute("login_id", newStaff.getLogin_id());
			sess.setAttribute("authority_group_id", authority.getId());
			response.sendRedirect("register-operator");
			return;
		}

		newStaff.setUpdate_user(user.getId());
		newStaff.setPasswordsetFlg(false);

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// チャンネル一覧を取得する
			StaffDao dao = new StaffDao(conn);

			dao.insert(newStaff, user.getId());

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI());

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		sess.setAttribute("message", "「" + newStaff.getName() + "」さんを登録しました");
		// 運用管理者一覧画面に遷移する
		response.sendRedirect("list-operator");

	}

}
