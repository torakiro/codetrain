package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.StaffDao;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class OperatorLoginServlet
 */
@WebServlet(urlPatterns = { "/operator-login" }, initParams = {
		@WebInitParam(name = "password", value = "knowledge123") })
public class OperatorLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(OperatorLoginServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// フォームのデータを取得する
		String loginId = request.getParameter("id");
		String loginPassword = request.getParameter("password");

		// セッションを取得する
		HttpSession session = request.getSession(true);

		// 入力チェック
		if ("".equals(loginPassword)) {
			session.setAttribute("errorMessage", "パスワードを入力してください");
		}

		if ("".equals(loginId)) {
			logger.warn("ログイン失敗 {}", request.getRemoteAddr());

			session.setAttribute("errorMessage", "ログインIDを入力してください");
		}

		if (loginId.length() > 255 || loginPassword.length() > 16) {
			session.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (session.getAttribute("errorMessage") != null) {
			// ログイン画面に戻る
			response.sendRedirect("operator-login.jsp");
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			StaffDao loginDao = new StaffDao(conn);
			StaffDto userDto = loginDao.findByidAndPassword(loginId, loginPassword);

			session.setAttribute("operator", userDto);
			session.removeAttribute("errorMessage");

			// ログイン失敗時
			if (userDto == null) {
				logger.warn("ログイン失敗 {} mail={} pass={}", request.getRemoteAddr(), loginId, loginPassword);
				session.setAttribute("errorMessage", "ログインIDまたはパスワードが間違っています");

				// ログイン画面に戻る
				response.sendRedirect("operator-login.jsp");
				return;
			}

			// 初回ログイン時
			if (!userDto.isPasswordsetFlg()) {
				request.getRequestDispatcher("WEB-INF/operator-change-password.jsp").forward(request, response);
				return;
			}

			// 成功した場合は、権限グループに合わせてそれぞれの画面に遷移する
			Common.operatorAfterLogin(userDto, request, response);

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}
	}
}
