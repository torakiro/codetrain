package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.CorpPersonDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/remove-user")
public class RemoveUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(RemoveUserServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.CUSTOMER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);
		if (!Common.isCustomerLoggedIn(sess, response, logger)) return;

		CorpPersonDto customer = (CorpPersonDto)sess.getAttribute("customer");

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		UserDto user = new UserDto();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setUpdate_user(customer.getId());

		try (Connection conn = DataSourceManager.getConnection()) {

			UserDao userDao = new UserDao(conn);
			int count;

			// 利用者を休止状態にする
			count = userDao.remove(user);

			if (count == 0) {
				request.setAttribute("errorMessage", "削除できませんでした。もう一度やり直してください");
				// 利用者マスタメンテナンス画面へ遷移する
				request.getRequestDispatcher("manage-user").forward(request, response);
				return;
			}

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("customer-system-error.jsp");
			return;
		}

		request.setAttribute("message", "削除しました");
		// 利用者マスタメンテナンス画面へ遷移する
		request.getRequestDispatcher("manage-user").forward(request, response);

	}

}
