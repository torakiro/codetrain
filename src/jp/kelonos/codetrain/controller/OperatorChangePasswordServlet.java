package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.StaffDao;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class OperatorChangePasswordServlet
 */
@WebServlet("/operator-change-password")
public class OperatorChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(OperatorChangePasswordServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(false);

		if (!Common.isOperatorLoggedIn(session, response, logger))
			return;

		// パス変更画面に遷移する
		request.getRequestDispatcher("/WEB-INF/operator-change-password.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(false);

		if (!Common.isOperatorLoggedIn(session, response, logger))
			return;

		StaffDto dto = (StaffDto) session.getAttribute("operator");
		// フォームのデータを取得する
		String oldpass = request.getParameter("old_password");
		String pass1 = request.getParameter("password1");
		String pass2 = request.getParameter("password2");

		// 入力チェック
		if (!pass1.equals(pass2)) {
			session.setAttribute("errorMessage", "新しいパスワードが確認用パスワードと一致しません");
		}

		if ("".equals(pass2)) {
			session.setAttribute("errorMessage", "確認用パスワードを入力してください");
		}

		if ("".equals(pass1)) {
			session.setAttribute("errorMessage", "新しいパスワードを入力してください");
		}

		if ("".equals(oldpass)) {
			session.setAttribute("errorMessage", "現在のパスワードを入力してください");
		}

		if (oldpass.length() > 16 || pass1.length() > 16 || pass2.length() > 16) {
			session.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (session.getAttribute("errorMessage") != null) {
			logger.info("パスワード変更失敗 {}", request.getRemoteAddr());
			// パスワード変更画面に戻る
			request.getRequestDispatcher("WEB-INF/operator-change-password.jsp").forward(request, response);
			return;
		}

		//新しいパスワードを格納＋パスワード設定済みフラグ更新
		dto.setPassword(pass1);
		boolean passwordSetFlg = dto.isPasswordsetFlg();
		dto.setPasswordsetFlg(true);
		dto.setUpdate_user(dto.getId());

		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			StaffDao dao = new StaffDao(conn);

			if (dao.updatePassword(dto, oldpass) == 0) {
				// パスワード変更に失敗
				session.setAttribute("errorMessage", "現在のパスワードが間違っています");
				dto.setPasswordsetFlg(passwordSetFlg);

				// ログイン画面に戻る
				request.getRequestDispatcher("WEB-INF/operator-change-password.jsp").forward(request, response);
				return;
			}

			session.removeAttribute("errorMessage");

			// 成功した場合は、権限グループに合わせてそれぞれの画面に遷移する
			Common.operatorAfterLogin(dto, request, response);

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}
	}

}
