package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.BusinessLogicException;
import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.InitialPasswordDao;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.CorpPersonDto;
import jp.kelonos.codetrain.dto.InitialPasswordDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/add-user")
public class AddUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddUserServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.CUSTOMER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);

		if (!Common.isCustomerLoggedIn(sess, response, logger))
			return;
		CorpPersonDto customer = (CorpPersonDto) sess.getAttribute("customer");

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		UserDto user = new UserDto();
		user.setCorp_id(customer.getCorp_id());
		user.setLoginId(request.getParameter("login_id"));
		user.setUpdate_user(customer.getId());
		if (request.getParameter("empNo") != null) {
			user.setEmpno(Integer.valueOf(request.getParameter("empNo")));
		} else {
			user.setEmpno(null);
		}

		InitialPasswordDto password = new InitialPasswordDto();
		password.setInitial_password(request.getParameter("password"));
		password.setUpdate_user(customer.getId());

		// 入力チェック
		if ("".equals(password.getInitial_password())) {
			sess.setAttribute("errorMessage", "初期パスワードを入力してください");
		}

		if ("".equals(user.getLoginId())) {
			sess.setAttribute("errorMessage", "ログインIDを入力してください");
		}

		if (password.getInitial_password().length()>16 || user.getLoginId().length()>255) {
			sess.setAttribute("errorMessage", "入力文字数が多いです");
		}

		if (sess.getAttribute("errorMessage") != null) {
			request.setAttribute("user", user.getLoginId());
			// 入力画面に戻る
			request.getRequestDispatcher("WEB-INF/add-user.jsp").forward(request, response);
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			try {
				UserDao userDao = new UserDao(conn);
				InitialPasswordDao initialPasswordDao = new InitialPasswordDao(conn);

				// トランザクションを開始する
				conn.setAutoCommit(false);

				int nextid;
				int count;

				nextid = initialPasswordDao.selectAutoIncrement();
				password.setId(nextid);
				count = initialPasswordDao.insertWithPrimaryKey(password);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				user.setInitialpassword_id(nextid);
				count = userDao.insert(user);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				// コミットする
				conn.commit();

			} catch (BusinessLogicException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				request.setAttribute("errorMessage", "登録できませんでした - 排他制御");

				// 利用者マスタメンテナンスに遷移する
				request.getRequestDispatcher("manage-user").forward(request, response);
				return;

			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("customer-system-error.jsp");
			return;
		}

		request.setAttribute("message", "登録しました");
		// 利用者マスタメンテナンス画面へ遷移する
		request.getRequestDispatcher("manage-user").forward(request, response);

	}

}
