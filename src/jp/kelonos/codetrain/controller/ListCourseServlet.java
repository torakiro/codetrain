package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CategoryDao;
import jp.kelonos.codetrain.dao.CourseDao;
import jp.kelonos.codetrain.dao.CurriculumDao;
import jp.kelonos.codetrain.dao.LearnHistoryDao;
import jp.kelonos.codetrain.dto.CategoryDto;
import jp.kelonos.codetrain.dto.CourseDto;
import jp.kelonos.codetrain.dto.CurriculumDto;
import jp.kelonos.codetrain.dto.LearnHistoryDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/list-course")
public class ListCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CustomerLoginServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession session = request.getSession(true);
		// コネクションを取得する
				try (Connection conn = DataSourceManager.getConnection()) {
					CategoryDao categoryDao = new CategoryDao(conn);

					CourseDao courseDao = new CourseDao(conn);
					CurriculumDao curriculumDao = new CurriculumDao(conn);
					LearnHistoryDao learnHistoryDao = new LearnHistoryDao(conn);

					UserDto user = (UserDto)session.getAttribute("user");
					if (user != null) {
						if (!user.isPasswordsetFlg()) {
							response.sendRedirect("logout");
							return;
						}
					}
					// カテゴリ一覧を取得する
					List<CategoryDto> categoryList = categoryDao.selectAll();

					Map<Integer, ArrayList<CourseDto>> courseMap = new HashMap<Integer,ArrayList<CourseDto>>();

					Map<Integer, Integer> timeMap = new HashMap<>();
					Map<Integer,Integer>courseProgress = new HashMap<Integer,Integer>();
					Map<Integer,Integer>categoryProgress = new HashMap<Integer,Integer>();

					for (CategoryDto categoryDto : categoryList) {
						//カテゴリごとのコースを取得する
						ArrayList<CourseDto> coursedtos = courseDao.findByCategoryId(categoryDto.getId());
						courseMap.put(categoryDto.getId(),coursedtos);

						//ログイン中なら
						if (user != null) {
							//カテゴリごとの目安時間
							Integer totalTime = null;

							//カテゴリごとの進捗率
							int categoryProressTotal = 0;
							int categoryCount = 0;

							for (CourseDto courseDto : coursedtos) {

								//コースごとの進捗率
								int totalProgress = 0 ;

								//コースごと進捗率の計算
								ArrayList<CurriculumDto> curriculumDtos = curriculumDao.findByCourseId(courseDto.getId());
								for (int i = 0; i < curriculumDtos.size(); i++) {
									categoryCount++;
									LearnHistoryDto historyDto =  learnHistoryDao.findByCurIdAndUserId(curriculumDtos.get(i).getId(), user.getId());
									if (historyDto != null) {
										totalProgress += historyDto.getProgress();
									}
								}
								categoryProressTotal += totalProgress;
								if (curriculumDtos.size() != 0) {
									totalProgress /= curriculumDtos.size();
								}
								else {
									totalProgress = 0;
								}
								courseProgress.put(courseDto.getId(), totalProgress);

								//カテゴリごとの目安時間を計算
								if (courseDto.getTime_approx() != null) {
									if (totalTime == null) totalTime = 0;
									totalTime += courseDto.getTime_approx();
								}

							}
							timeMap.put(categoryDto.getId(), totalTime);
							int category_progress = 0;
							if (categoryCount != 0) category_progress = categoryProressTotal/categoryCount;
							categoryProgress.put(categoryDto.getId(), category_progress);//カテゴリごとの進捗率の計算し、保存
						}
					}

					// カテゴリ一覧データをリクエストに保持する
					request.setAttribute("categoryList", categoryList);

					// コース一覧データをリクエストに保持する
					request.setAttribute("courseMap", courseMap);

					if (user != null) {
						//カテゴリごとの目安時間をリクエストに保持する
						request.setAttribute("timeMap", timeMap);
						request.setAttribute("courseProgress", courseProgress);
						request.setAttribute("categoryProgress", categoryProgress);
					}

					// URIをリクエストに保持する
					request.setAttribute("uri", request.getRequestURI());

					// チャンネル一覧画面に遷移する
					request.getRequestDispatcher("/WEB-INF/list-course.jsp").forward(request, response);
				} catch (SQLException | NamingException e) {
					logger.error("{} {}", e.getClass(), e.getMessage());

					// システムエラーに遷移する
					response.sendRedirect("system-error.jsp");
					return;
				}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		doGet(request, response);
	}
}
