package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.RequestReplyDao;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dao.UserRequestDao;
import jp.kelonos.codetrain.dto.RequestReplyDto;
import jp.kelonos.codetrain.dto.StaffDto;
import jp.kelonos.codetrain.dto.UserDto;
import jp.kelonos.codetrain.dto.UserRequestDto;

/**
 * Servlet implementation class ListMessageServlet
 */
@WebServlet("/list-message")
public class ListMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListMessageServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		HttpSession sess = request.getSession(false);
		if (!Common.isOperatorLoggedIn(sess, response, logger)) return;

		StaffDto user = (StaffDto)sess.getAttribute("operator");
		if (!user.getAuthority_group().canShow_message()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		//パラメータを取得する
		Integer user_id = 0;
		try {
			user_id = Integer.parseInt(request.getParameter("id"));
		}
		catch (Exception e) {
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			UserRequestDao requestDao = new UserRequestDao(conn);
			RequestReplyDao replyDao = new RequestReplyDao(conn);
			UserDao userDao = new UserDao(conn);

			List<UserRequestDto> requestList = requestDao.findAllByUser(user_id);
			Map<Integer, List<RequestReplyDto>> replyMap = replyDao.getMapByUserId(user_id);
			UserDto userDto = userDao.selectByPrimaryKey(user_id);

			request.setAttribute("requestList", requestList);
			request.setAttribute("replyMap", replyMap);

			//利用者IDをリクエストに保持する
			request.setAttribute("userId", user_id);
			//利用者名をリクエストに保持する
			request.setAttribute("userName", userDto.getName());

			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-message.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
