package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.TextbookDao;
import jp.kelonos.codetrain.dto.StaffDto;
import jp.kelonos.codetrain.dto.TextDto;

/**
 * Servlet implementation class ViewTextbookServlet
 */
@WebServlet("/view-textbook")
public class ViewTextbookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddCustomerServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// コネクションを取得する
		HttpSession session = request.getSession(false);
		if (!Common.isOperatorLoggedIn(session, response, logger)) return;

		StaffDto user = (StaffDto)session.getAttribute("operator");
		if (!user.getAuthority_group().canShow_text()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			TextbookDao TextbookDao = new TextbookDao(conn);

			//テキストIDを取得
			String strId = request.getParameter("textId");
			int id = 0;

			try {
				id = Integer.parseInt(strId);
			}
			catch (Exception e) {
				e.printStackTrace(System.out);
				Common.logout(Common.OPERATION, response);
				return;
			}

			//テキストの詳細を取得
			TextDto textbook = TextbookDao.findByPrimaryKey(id);

			//テキスト詳細をリクエストに保持する
			request.setAttribute("textbook", textbook);

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI()+"?"+request.getQueryString());

			// テキスト一覧画面に遷移する
			request.getRequestDispatcher("/WEB-INF/view-textbook.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("operation-system-error.jsp");
			return;
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
