package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CorpPersonDao;
import jp.kelonos.codetrain.dto.CorpPersonDto;

/**
 * Servlet implementation class CustomerLoginServlet
 */
@WebServlet("/customer-login")
public class CustomerLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CustomerLoginServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerLoginServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// ログイン画面へ遷移する
		Common.logout(Common.CUSTOMER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// フォームのデータを取得する
		String loginId = request.getParameter("id");
		String loginPassword = request.getParameter("password");

		// セッションを取得する
		HttpSession session = request.getSession(true);

		// 入力チェック
		if ("".equals(loginPassword)) {
			logger.info("ログイン失敗 {}", request.getRemoteAddr());

			session.setAttribute("errorMessage", "パスワードを入力してください");
		}

		if ("".equals(loginId)) {
			logger.info("ログイン失敗 {}", request.getRemoteAddr());

			session.setAttribute("errorMessage", "ログインIDを入力してください");
		}

		if (loginId.length() > 512 || loginPassword.length() > 16) {

			session.setAttribute("errorMessage", "入力文字数が多いです");
		}

		if (session.getAttribute("errorMessage") != null) {
			// ログインページへ遷移する
			response.sendRedirect("customer-login.jsp");
			return;
		}
		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			CorpPersonDao corpPersonDao = new CorpPersonDao(conn);
			CorpPersonDto corpPersonDto = corpPersonDao.findByIdAndPassword(loginId, loginPassword);

			session.setAttribute("customer", corpPersonDto);
			session.removeAttribute("errorMessage");

			// ログイン失敗時
			if (corpPersonDto == null) {
				logger.info("ログイン失敗 {} mail={} pass={}", request.getRemoteAddr(), loginId, loginPassword);
				session.setAttribute("errorMessage", "ログインIDまたはパスワードが間違っています");
				response.sendRedirect("customer-login.jsp");
				return;
			}

			// 初回ログイン時
			if (!corpPersonDto.isPasswordsetFlg()) {
				request.getRequestDispatcher("WEB-INF/customer-change-password.jsp").forward(request, response);
				return;
			}

			// 利用者一覧へ遷移する
			request.getRequestDispatcher("list-user").forward(request, response);

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("customer-system-error.jsp");
			return;
		}
	}

}
