package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.AnswerDao;
import jp.kelonos.codetrain.dao.CourseDao;
import jp.kelonos.codetrain.dao.CurriculumDao;
import jp.kelonos.codetrain.dao.LearnHistoryDao;
import jp.kelonos.codetrain.dao.QuestionDao;
import jp.kelonos.codetrain.dao.TextDao;
import jp.kelonos.codetrain.dto.AnswerDto;
import jp.kelonos.codetrain.dto.CourseDto;
import jp.kelonos.codetrain.dto.CurriculumDto;
import jp.kelonos.codetrain.dto.LearnHistoryDto;
import jp.kelonos.codetrain.dto.QuestionDto;
import jp.kelonos.codetrain.dto.TextDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/study")
public class StudyServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(RemoveUserServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession session = request.getSession(false);
		if (session == null) {
			errorHandling(request, response, "courseMessage", "セッションタイムアウト", "user_index.jsp");
			return;
		}
		int curriculumId = 0;
		try {
			//カリキュラムIDを取得
			String strId = request.getParameter("curriculumId");
			if (strId == null || strId.isEmpty())
				throw new NumberFormatException();
			curriculumId = Integer.parseInt(strId);
		} catch (NumberFormatException e) {
			//コースIDにintにできない値が入ったとき
			errorHandling(request, response, "courseMessage", "カリキュラムが存在していません", "user_index.jsp");
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			CourseDao courseDao = new CourseDao(conn);
			CurriculumDao curriculumDao = new CurriculumDao(conn);
			QuestionDao questionDao = new QuestionDao(conn);
			TextDao textDao = new TextDao(conn);
			AnswerDao answerDao = new AnswerDao(conn);

			//テキスト情報を取得
			TextDto textDto = textDao.findByCurriculumId(curriculumId);

			//カリキュラム情報を取得
			CurriculumDto curriculumDto = curriculumDao.findById(curriculumId);

			if (curriculumDto == null) {
				//存在していないカリキュラムを見ようとしたとき
				// トップページに遷移する
				errorHandling(request, response, "courseMessage", "カリキュラムが存在していません", "user_index.jsp");
				return;
			}

			//コース情報を取得
			CourseDto course = courseDao.findById(curriculumDto.getCourse_id());

			UserDto user = (UserDto) session.getAttribute("user");
			if (user == null) {
				if (!course.isFreecourseFlg()) {
					//ユーザーが未ログインでフリーコースじゃないコースのカリキュラムを見ようとしたとき
					// トップページに遷移する
					errorHandling(request, response, "courseMessage", "フリーコースしか閲覧できません", "user_index.jsp");
					return;
				}
			}else{
				if (!user.isPasswordsetFlg()) {
					response.sendRedirect("logout");
					return;
				}
			}

			//エクササイズ情報を取得
			ArrayList<QuestionDto> questionDtos = questionDao.findByCurriculumId(curriculumId);

			//現在のカリキュラムの前後を取得
			CurriculumDto nextCurriculum = null;
			CurriculumDto previousCurriculum = null;
			ArrayList<CurriculumDto> curriculumDtos = curriculumDao.findByCourseId(course.getId());
			for (int i = 0; i < curriculumDtos.size(); i++) {
				if (curriculumDtos.get(i).getId() == curriculumDto.getId()) {
					if (i + 1 < curriculumDtos.size()) {
						nextCurriculum = curriculumDtos.get(i + 1);
					}
					if (i - 1 >= 0) {
						previousCurriculum = curriculumDtos.get(i - 1);
					}
					break;
				}
			}

			//エクササイズの問題数を取得
			Integer questionCount = questionDao.countCurId(curriculumId);

			ArrayList<AnswerDto> answerDtos = null;
			if (user != null) {
				answerDtos = answerDao.selectCurIdAndUserId(curriculumId, user.getId());
				//問題がない場合の処理
				if (questionCount == 0) {
					LearnHistoryDao learnHistoryDao = new LearnHistoryDao(conn);
					LearnHistoryDto historyDto = learnHistoryDao.findByCurIdAndUserId(curriculumId, user.getId());
					//ヒストリーテーブルにレコードが存在していない場合のみ登録
					if (historyDto == null) {
						learnHistoryDao.insert(curriculumId, 100, -1, user.getId());
					}
				}
			} else {
				answerDtos = (ArrayList<AnswerDto>) session.getAttribute("answers");
			}

			//テキストをリクエストに保持する
			request.setAttribute("text", textDto);

			//コースをリクエストに保持する
			request.setAttribute("course", course);

			//カリキュラムをセッションに保持する
			request.setAttribute("curriculum", curriculumDto);

			//次カリキュラムをリクエストに保持する
			request.setAttribute("nextCurriculum", nextCurriculum);

			//前カリキュラムをリクエストに保持する
			request.setAttribute("previousCurriculum", previousCurriculum);

			//ユーザーの解答情報リストをリクエストに保持する
			request.setAttribute("answerList", answerDtos);

			//エクササイズ情報リストをリクエストに保持する
			request.setAttribute("questionList", questionDtos);

			//エクササイズの問題数をリクエストに保持する
			request.setAttribute("questionCount", questionCount);

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI() + "?" + request.getQueryString());

			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("/WEB-INF/study.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// トップページに遷移する
			errorHandling(request, response, "courseMessage", "エラー", "user_index.jsp");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private void errorHandling(HttpServletRequest request, HttpServletResponse response, String errorField,
			String errorMessage, String redirectTarget)
			throws IOException {
		HttpSession session = request.getSession(true);
		session.setAttribute(errorField, errorMessage);
		response.sendRedirect(redirectTarget);
	}
}
