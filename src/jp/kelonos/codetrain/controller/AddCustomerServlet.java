package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.BusinessLogicException;
import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.SecureRandomPassword;
import jp.kelonos.codetrain.dao.BillingAddressDao;
import jp.kelonos.codetrain.dao.CorpDao;
import jp.kelonos.codetrain.dao.CorpPersonDao;
import jp.kelonos.codetrain.dto.BillingAddressDto;
import jp.kelonos.codetrain.dto.CorpDto;
import jp.kelonos.codetrain.dto.CorpPersonDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/add-customer")
public class AddCustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddCustomerServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);

		if (!Common.isOperatorLoggedIn(sess, response, logger))
			return;

		StaffDto user = (StaffDto) sess.getAttribute("operator");
		if (!user.getAuthority_group().canCreate_corp()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CorpDto corp = new CorpDto();
		corp.setName(request.getParameter("corpName"));
		corp.setDomain(request.getParameter("corpDomain"));
		BillingAddressDto billingAddress = new BillingAddressDto();
		billingAddress.setAddress(request.getParameter("billingAddress"));
		CorpPersonDto corpPerson = new CorpPersonDto();
		corpPerson.setName(request.getParameter("personName"));
		corpPerson.setEmail(request.getParameter("personEmail"));
		corpPerson.setPosition(request.getParameter("personPosition"));

		// 入力チェック
		if ("".equals(corpPerson.getPosition())) {

			sess.setAttribute("errorMessage", "担当者の役職を入力してください");
		}
		if ("".equals(corpPerson.getEmail())) {

			sess.setAttribute("errorMessage", "担当者メールアドレスを入力してください");
		}
		if ("".equals(corpPerson.getName())) {

			sess.setAttribute("errorMessage", "担当者名を入力してください");
		}
		if ("".equals(billingAddress.getAddress())) {

			sess.setAttribute("errorMessage", "請求先住所を入力してください");
		}
		if ("".equals(corp.getDomain())) {

			sess.setAttribute("errorMessage", "ドメインを入力してください");
		}
		if ("".equals(corp.getName())) {

			sess.setAttribute("errorMessage", "法人名を入力してください");
		}

		if (corp.getName().length() > 255 || corp.getDomain().length() > 512 || corpPerson.getEmail().length() > 512
				|| corpPerson.getName().length() > 16 || corpPerson.getPosition().length() > 255
				|| billingAddress.getAddress().length() > 512) {
			sess.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (sess.getAttribute("errorMessage") != null) {
			// 入力画面に戻る
			request.setAttribute("corpName", corp.getName());
			request.setAttribute("corpDomain", corp.getDomain());
			request.setAttribute("billingAddress", billingAddress.getAddress());
			request.setAttribute("personName", corpPerson.getName());
			request.setAttribute("personEmail", corpPerson.getEmail());
			request.setAttribute("personPosition", corpPerson.getPosition());
			request.getRequestDispatcher("WEB-INF/add-customer.jsp").forward(request, response);
			return;
		}
		String password;
		SecureRandomPassword createPassword = new SecureRandomPassword();
		corp.setUpdate_user(user.getId());
		corpPerson.setUpdate_user(user.getId());
		corpPerson.setPassword(createPassword.createPassword().toString());
		password = corpPerson.getPassword();
		corpPerson.setPasswordsetFlg(false);
		billingAddress.setUpdate_user(user.getId());

		try (Connection conn = DataSourceManager.getConnection()) {

			try {
				CorpDao corpdao = new CorpDao(conn);
				BillingAddressDao billingdao = new BillingAddressDao(conn);
				CorpPersonDao persondao = new CorpPersonDao(conn);

				// トランザクションを開始する
				conn.setAutoCommit(false);

				int nextid;
				int count;

				nextid = persondao.selectAutoIncrement();
				corpPerson.setId(nextid);
				count = persondao.insertWithPrimaryKey(corpPerson);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				corp.setCorp_person_id(nextid);
				nextid = corpdao.selectAutoIncrement();
				corp.setId(nextid);
				count = corpdao.insertWithPrimaryKey(corp);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				billingAddress.setCorp_id(nextid);
				count = billingdao.insert(billingAddress);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				// コミットする
				conn.commit();

			} catch (BusinessLogicException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				request.setAttribute("errorMessage", "「" + corp.getName() + "」を登録できませんでした - 排他制御");

				// 法人一覧に遷移する
				request.getRequestDispatcher("list-customer").forward(request, response);
				return;

			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		request.setAttribute("message", "「" + corp.getName() + "」を登録しました 初期パスワードは "+password+" です");
		// チャンネル一覧画面に遷移する
		request.getRequestDispatcher("list-customer").forward(request, response);

	}

}
