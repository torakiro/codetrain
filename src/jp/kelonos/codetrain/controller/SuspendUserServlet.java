package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.BusinessLogicException;
import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dao.UserSuspendDao;
import jp.kelonos.codetrain.dto.CorpPersonDto;
import jp.kelonos.codetrain.dto.UserDto;
import jp.kelonos.codetrain.dto.UserSuspendDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/suspend-user")
public class SuspendUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(SuspendUserServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.CUSTOMER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);
		if (!Common.isCustomerLoggedIn(sess, response, logger))
			return;

		CorpPersonDto customer = (CorpPersonDto) sess.getAttribute("customer");

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		UserSuspendDto userSuspend = new UserSuspendDto();
		UserDto user = new UserDto();
		userSuspend.setUpdate_user(customer.getId());
		userSuspend.setUser_id(Integer.parseInt(request.getParameter("id")));
		userSuspend.setReason(request.getParameter("reason"));
		user.setId(userSuspend.getUser_id());
		user.setState(1);
		user.setUpdate_user(customer.getId());
		user.setName(request.getParameter("name"));
		user.setSuspend_month(Date.valueOf(LocalDateTime.now().toLocalDate()));

		// 入力チェック
		if ("".equals(userSuspend.getReason())) {

			sess.setAttribute("errorMessage", "休止理由を入力してください");
		}

		if (userSuspend.getReason().length() > 512) {
			sess.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (sess.getAttribute("errorMessage") != null) {
			// 入力画面に戻る
			request.setAttribute("user", user);
			request.getRequestDispatcher("WEB-INF/suspend-user.jsp").forward(request, response);
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			try {
				UserSuspendDao userSuspendDao = new UserSuspendDao(conn);
				UserDao userDao = new UserDao(conn);

				// トランザクションを開始する
				conn.setAutoCommit(false);

				int count;

				// 休止理由を登録する
				count = userSuspendDao.insert(userSuspend);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				// 利用者を休止状態にする
				count = userDao.suspendStateById(user);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				// コミットする
				conn.commit();

			} catch (BusinessLogicException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				request.setAttribute("errorMessage", "更新できませんでした - 排他制御");

				// 利用者マスタメンテナンスに遷移する
				request.getRequestDispatcher("manage-user").forward(request, response);
				return;

			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("customer-system-error.jsp");
			return;
		}

		request.setAttribute("message", "休止状態に変更しました");
		// 利用者マスタメンテナンス画面へ遷移する
		request.getRequestDispatcher("manage-user").forward(request, response);

	}

}
