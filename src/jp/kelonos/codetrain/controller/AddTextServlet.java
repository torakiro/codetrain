package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.BusinessLogicException;
import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CurriculumDao;
import jp.kelonos.codetrain.dao.TextbookDao;
import jp.kelonos.codetrain.dto.CurriculumDto;
import jp.kelonos.codetrain.dto.StaffDto;
import jp.kelonos.codetrain.dto.TextDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/add-textbook")
public class AddTextServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddTextServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession session = request.getSession(false);

		if (!Common.isOperatorLoggedIn(session, response, logger))
			return;

		StaffDto user = (StaffDto) session.getAttribute("operator");
		if (!user.getAuthority_group().canCreate_text()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		TextDto text = new TextDto();
		CurriculumDto curriculum = new CurriculumDto();
		text.setTitle(request.getParameter("text_title"));
		text.setText(request.getParameter("text_text"));
		curriculum.setCourse_id(Integer.parseInt(request.getParameter("course")));
		curriculum.setName(request.getParameter("text_title"));
		//		BillingAddressDto billingAddress = new BillingAddressDto();
		//		billingAddress.setAddress(request.getParameter("billingAddress"));
		//		CorpPersonDto corpPerson = new CorpPersonDto();
		//		corpPerson.setName(request.getParameter("personName"));
		//		corpPerson.setEmail(request.getParameter("personEmail"));
		//		corpPerson.setPosition(request.getParameter("personPosition"));

		// 入力チェック
		if (text.getText() == null || "".equals(text.getText())) {

			session.setAttribute("errorMessage", "本文を入力してください");
		}
		if (text.getTitle() == null || "".equals(text.getTitle())) {

			session.setAttribute("errorMessage", "タイトルを入力してください");
		}
		if (text.getTitle().length() > 30 || text.getText().length() > 65535) {

			session.setAttribute("errorMessage", "入力文字数が多いです");
		}

		if (session.getAttribute("errorMessage") != null) {
			// 入力画面に戻る
			session.setAttribute("text_title", text.getTitle());
			session.setAttribute("text", text.getText());

			request.getRequestDispatcher("/register-textbook").forward(request, response);
			return;
		}

		text.setUpdate_user(user.getId());
		curriculum.setExamFlg(false);
		curriculum.setUpdate_user(user.getId());

		try (Connection conn = DataSourceManager.getConnection()) {

			try {

				CurriculumDao curriculumdao = new CurriculumDao(conn);
				TextbookDao textdao = new TextbookDao(conn);

				// トランザクションを開始する
				conn.setAutoCommit(false);

				int count;

				int curriculum_id;
				curriculum_id = curriculumdao.selectAutoIncrement();
				curriculum.setId(curriculum_id);
				count = curriculumdao.insertWithPrimaryKey(curriculum);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				text.setCurriculum_id(curriculum_id);
				count = textdao.insert(text);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				// コミットする
				conn.commit();

			} catch (BusinessLogicException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				session.setAttribute("errorMessage", "「" + text.getTitle() + "」を登録できませんでした - 排他制御");

				// 法人一覧に遷移する
				request.getRequestDispatcher("list-text").forward(request, response);
				return;

			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		request.setAttribute("message", "「" + text.getTitle() + "」を登録しました");
		// 一覧画面に遷移する
		request.getRequestDispatcher("list-text").forward(request, response);

	}

}
