package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CategoryDao;
import jp.kelonos.codetrain.dto.CategoryDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/add-category")
public class AddCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddCategoryServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);

		if (!Common.isOperatorLoggedIn(sess, response, logger))
			return;

		StaffDto user = (StaffDto) sess.getAttribute("operator");
		if (!user.getAuthority_group().canCreate_text()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CategoryDto category = new CategoryDto();
		category.setName(request.getParameter("categoryName"));
		int updatemode = Integer.parseInt(request.getParameter("updatemode"));
		request.setAttribute("id", request.getParameter("id"));

		// 入力チェック
		if ("".equals(category.getName())) {

			sess.setAttribute("errorMessage", "カテゴリ名を入力してください");
		}
		//		if ("".equals(text.getText())) {

		//			request.setAttribute("message", "本文を入力してください");
		//		}
		if (sess.getAttribute("errorMessage") != null) {
			// 入力画面に戻る
			request.setAttribute("categoryName", category.getName());
			//			request.setAttribute("text",text.getText());

			if (updatemode == 1)
				request.getRequestDispatcher("register-textbook").forward(request, response);
			if (updatemode == 2)
				request.getRequestDispatcher("update-textbook").forward(request, response);
			return;
		}

		category.setUpdate_user(user.getId());
		//		curriculum.setExamFlg(false);
		//		curriculum.setUpdate_user(user.getId());

		try (Connection conn = DataSourceManager.getConnection()) {

			CategoryDao categorydao = new CategoryDao(conn);
			categorydao.insert(category);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("opeation-system-error.jsp").forward(request, response);
			return;
		}

		sess.setAttribute("message", "「" + category.getName() + "」カテゴリを登録しました");
		// 一覧画面に遷移する
		if (updatemode == 1)
			request.getRequestDispatcher("register-textbook").forward(request, response);
		if (updatemode == 2)
			request.getRequestDispatcher("update-textbook").forward(request, response);

	}

}
