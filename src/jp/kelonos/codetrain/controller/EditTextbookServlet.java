package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.BusinessLogicException;
import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CurriculumDao;
import jp.kelonos.codetrain.dao.TextbookDao;
import jp.kelonos.codetrain.dto.CurriculumDto;
import jp.kelonos.codetrain.dto.StaffDto;
import jp.kelonos.codetrain.dto.TextDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/edit-textbook")
public class EditTextbookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(EditTextbookServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession session = request.getSession(false);
		if (!Common.isOperatorLoggedIn(session, response, logger)) return;

		StaffDto user = (StaffDto)session.getAttribute("operator");
		if (!user.getAuthority_group().canCreate_text()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		TextDto text = new TextDto();
		text.setId(Integer.parseInt(request.getParameter("id")));
		text.setTitle(request.getParameter("text_title"));
		text.setText(request.getParameter("text_text"));
		text.setCurriculum_id(Integer.parseInt(request.getParameter("curriculum_id")));

		// 入力チェック
		if (text.getTitle() == null || "".equals(text.getTitle())) {

			session.setAttribute("errorMessage", "タイトルを入力してください");
		}
		if (text.getText() == null || "".equals(text.getText())) {

			session.setAttribute("errorMessage", "本文を入力してください");
		}
		if(text.getTitle().length()>30||text.getText().length()>65535) {

			session.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (session.getAttribute("errorMessage") != null) {
			// 入力画面に戻る
			session.setAttribute("text_title",text.getTitle());
			session.setAttribute("text",text.getText());

			request.getRequestDispatcher("WEB-INF/edit-textbook.jsp").forward(request, response);
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

				try {

					CurriculumDao curriculumdao = new CurriculumDao(conn);
					TextbookDao textdao = new TextbookDao(conn);

					// トランザクションを開始する
					conn.setAutoCommit(false);

					int count;
					CurriculumDto curriculum = curriculumdao.findByPrimaryKey(text.getCurriculum_id());
					curriculum.setCourse_id(Integer.parseInt(request.getParameter("course")));

					curriculum.setUpdate_user(user.getId());
					count = curriculumdao.update(curriculum);
					if (count == 0) throw new BusinessLogicException("排他制御（楽観ロック）例外");

					text.setUpdate_user(user.getId());
					count = textdao.update(text);
					if (count == 0) throw new BusinessLogicException("排他制御（楽観ロック）例外");

					// コミットする
					conn.commit();

				} catch (BusinessLogicException e) {

					logger.error("{} {}", e.getClass(), e.getMessage());

					// ロールバックする
					conn.rollback();
					session.setAttribute("errorMessage", "「" + text.getTitle() + "」を登録できませんでした - 排他制御");

					// テキスト一覧に遷移する
					request.getRequestDispatcher("list-text").forward(request, response);

				} catch (SQLException e) {

					logger.error("{} {}", e.getClass(), e.getMessage());

					// ロールバックする
					conn.rollback();
					throw e;
				} finally {
					conn.setAutoCommit(true);
				}

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		request.setAttribute("message", "「" + text.getTitle() + "」を登録しました");
		// 一覧画面に遷移する
		request.getRequestDispatcher("list-text").forward(request, response);


	}

}
