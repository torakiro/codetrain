package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserRequestDao;
import jp.kelonos.codetrain.dto.StaffDto;
import jp.kelonos.codetrain.dto.UserRequestDto;

/**
 * Servlet implementation class ListMessageSenderServlet
 */
@WebServlet("/list-message-sender")
public class ListMessageSenderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListMessageSenderServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		HttpSession sess = request.getSession(false);
		if (!Common.isOperatorLoggedIn(sess, response, logger)) return;

		StaffDto user = (StaffDto)sess.getAttribute("operator");
		if (!user.getAuthority_group().canShow_message_userlist()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// チャンネル一覧を取得する
			UserRequestDao dao = new UserRequestDao(conn);
			List<UserRequestDto> list = dao.findUsers();

			// チャンネル一覧データをリクエストに保持する
			request.setAttribute("senderList", list);

			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-message-sender.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
