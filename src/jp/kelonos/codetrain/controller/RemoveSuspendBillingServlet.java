package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.StaffDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/remove-suspend-billing")
public class RemoveSuspendBillingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(RemoveSuspendBillingServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession session = request.getSession(false);
		if (!Common.isOperatorLoggedIn(session, response, logger)) return;

		StaffDto user = (StaffDto)session.getAttribute("operator");
		if (!user.getAuthority_group().canControl_billing()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		int user_id = Integer.parseInt(request.getParameter("id"));

		try (Connection conn = DataSourceManager.getConnection()) {

			UserDao dao = new UserDao(conn);
			UserDto dto = new UserDto();

			dto.setId(user_id);
			dto.setSuspend_month(null);
			dto.setUpdate_user(user.getId());

			int count = dao.updateSuspendMonthByPrimaryKey(dto);
			System.out.println(count);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		request.setAttribute("corp_id", request.getParameter("corp_id"));
		request.setAttribute("corp_name", request.getParameter("corp_name"));
		request.setAttribute("message", "「" + request.getParameter("user_name") + "」さんの休止移行に関する代金請求を取り消しました。");
		// 一覧画面に遷移する
		request.getRequestDispatcher("list-suspend-reason").forward(request, response);


	}

}
