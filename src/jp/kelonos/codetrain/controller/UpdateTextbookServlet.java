package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CategoryDao;
import jp.kelonos.codetrain.dao.CourseDao;
import jp.kelonos.codetrain.dto.CategoryDto;
import jp.kelonos.codetrain.dto.CourseDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class RegisterTextbookServlet
 */
@WebServlet("/update-textbook")
public class UpdateTextbookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(UpdateTextbookServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		//response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);

		if (sess == null || sess.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト");
			goHomeQuickly(request, response);
			return;
		}

		StaffDto user = (StaffDto)sess.getAttribute("operator");
		if (!user.getAuthority_group().canEdit_text()) {
			logger.warn("権限なし");
			goHomeQuickly(request, response);
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			CourseDao courseDao = new CourseDao(conn);
			List<CourseDto> courseList = courseDao.findAll();

			CategoryDao categoryDao = new CategoryDao(conn);
			List<CategoryDto> categoryList = categoryDao.selectAll();

			request.setAttribute("category", categoryList);
			request.setAttribute("course", courseList);

			request.setAttribute("id", request.getParameter("id"));
			request.setAttribute("curriculum_id", request.getParameter("curriculum_id"));

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());
		}

		request.getRequestDispatcher("WEB-INF/edit-textbook.jsp").forward(request, response);

	}

	private void goHomeQuickly(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//TODO: ログアウトする
		return;
	}
}
