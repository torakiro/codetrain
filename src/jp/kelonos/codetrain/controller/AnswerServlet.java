package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.AnswerDao;
import jp.kelonos.codetrain.dao.CurriculumDao;
import jp.kelonos.codetrain.dao.LearnHistoryDao;
import jp.kelonos.codetrain.dao.QuestionDao;
import jp.kelonos.codetrain.dto.AnswerDto;
import jp.kelonos.codetrain.dto.CurriculumDto;
import jp.kelonos.codetrain.dto.LearnHistoryDto;
import jp.kelonos.codetrain.dto.QuestionDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/answer")
public class AnswerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddRequestServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		Common.logout(Common.USER, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession session = request.getSession(true);
		String uri = request.getParameter("uri");

		if (uri == null || uri.isEmpty()) {
			uri = "user_index.jsp";
		}

		int curId = 0;
		try {
			//カリキュラムIDを取得
			String strId = request.getParameter("curriculum");
			if (strId == null ||strId.isEmpty()) throw new NumberFormatException();
			curId = Integer.parseInt(strId);
		} catch (NumberFormatException e) {
			// 前画面に遷移する
			response.sendRedirect(uri);
			return;
		}
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			//ユーザー情報を取得
			UserDto user = (UserDto) session.getAttribute("user");
			if (user != null) {
				if (!user.isPasswordsetFlg()) {
					response.sendRedirect("logout");
					return;
				}
			}

			LearnHistoryDao learnHistoryDao = new LearnHistoryDao(conn);
			QuestionDao questionDao = new QuestionDao(conn);
			AnswerDao answerDao = new AnswerDao(conn);

			//エクササイズ情報を取得
			ArrayList<QuestionDto> questionDtos = questionDao.findByCurriculumId(curId);

			ArrayList<AnswerDto> answers = new ArrayList<>();

			CurriculumDao curriculumDao = new CurriculumDao(conn);
			CurriculumDto curriculumDto = curriculumDao.findById(curId);
			int userId = 0;
			if (user != null) {
				userId = user.getId();
			}
			for (QuestionDto questionDto : questionDtos) {
				//ラジオボタンの入力情報を取得
				String getStr = "radio" + questionDto.getId();
				String strRadio = request.getParameter(getStr);
				int radio = 0;
				//ラジオボタンが入力されているか
				if (strRadio != null) {
					radio = Integer.parseInt(strRadio);
				} else if(!curriculumDto.isExamFlg()){
					continue;
				}

				AnswerDto answer = new AnswerDto();
				answer.setQuestion_id(questionDto.getId());
				answer.setAnswer(radio);
				answer.setCorrect(answer.getAnswer() == questionDto.getAnsno() ? true : false);

				if (user != null) {
					//ユーザーの解答情報を登録or更新
					AnswerDto answerHistory = answerDao.selectQuestIdAndUserId(answer.getQuestion_id(), userId);
					if (answerHistory == null) {
						answer.setUser_id(userId);
						answerDao.insert(answer);
					} else {
						if (!curriculumDto.isExamFlg()) {
							answerDao.update(answer.getAnswer(), answer.getCorrect(), answerHistory.getId());
						}
					}
				} else {
					answers.add(answer);
				}
			}

			//ユーザーの学習履歴を登録or更新
			int count = questionDao.countCurId(curId);
			if (count > 0 && user != null) {
				int progress = answerDao.countAnswer(curId, userId) * 100 / count;
				int correctRate = answerDao.countCorrect(curId, userId) * 100 / count;
				LearnHistoryDto historyDto = learnHistoryDao.findByCurIdAndUserId(curId, userId);
				if (historyDto != null) {
					learnHistoryDao.update(progress, correctRate, historyDto.getId());
				} else {
					learnHistoryDao.insert(curId, progress, correctRate, userId);
				}
			}

			session.setAttribute("answers", answers);
			// 前画面に遷移する
			response.sendRedirect(uri);
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// トップページに遷移する
			//session.setAttribute("courseMessage", "エラー");
			//response.sendRedirect("user_index.jsp");

			// システムエラー画面に遷移する
			response.sendRedirect("customer-system-error.jsp");
			return;
		}
	}
}
