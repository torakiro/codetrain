package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CourseDao;
import jp.kelonos.codetrain.dto.CourseDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/add-course")
public class AddCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddCourseServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);

		if (!Common.isOperatorLoggedIn(sess, response, logger))
			return;

		StaffDto user = (StaffDto) sess.getAttribute("operator");
		if (!user.getAuthority_group().canCreate_text()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CourseDto course = new CourseDto();
		course.setName(request.getParameter("course_title"));
		course.setOverview(request.getParameter("overview"));
		course.setGoal(request.getParameter("goal"));
		course.setRequirement(request.getParameter("rule"));
		course.setFreecourseFlg(true);
		if (request.getParameter("freecourse") == null || Integer.parseInt(request.getParameter("freecourse")) != 1) {
			course.setFreecourseFlg(false);
		}
		course.setCategory_id(Integer.parseInt(request.getParameter("category")));
		int updatemode = Integer.parseInt(request.getParameter("updatemode"));
		request.setAttribute("id", request.getParameter("id"));

		// 入力チェック
		if ("".equals(course.getOverview())) {
			//request.setAttribute("message", "概要を入力してください");
			course.setOverview(null);
		}
		if ("".equals(course.getGoal())) {
			//request.setAttribute("message", "目標を入力してください");
			course.setGoal(null);
		}
		if ("".equals(course.getGoal())) {
			//request.setAttribute("message", "前提条件を入力してください");
			course.setRequirement(null);
		}
		if ("".equals(request.getParameter("time"))) {
			//request.setAttribute("message", "学習目安を入力してください");
			course.setTime_approx(null);
		}
		else {
			course.setTime_approx(Integer.parseInt(request.getParameter("time")));
		}
		if ("".equals(course.getName())) {
			sess.setAttribute("errorMessage", "コース名を入力してください");
		}
		if (course.getName().length() > 30 || course.getOverview().length() > 1024
				|| course.getRequirement().length() > 1024 || course.getGoal().length() > 128) {
			sess.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (sess.getAttribute("errorMessage") != null) {
			// 入力画面に戻る
			request.setAttribute("course_name", course.getName());
			request.setAttribute("overview", course.getOverview());
			request.setAttribute("goal", course.getGoal());
			request.setAttribute("time", course.getTime_approx());

			if (updatemode == 1)
				request.getRequestDispatcher("register-textbook").forward(request, response);
			if (updatemode == 2)
				request.getRequestDispatcher("update-textbook").forward(request, response);
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			CourseDao coursedao = new CourseDao(conn);

			int count;
			count = coursedao.insert(course);

				sess.setAttribute("message", "「" + course.getName() + "」コースを登録しました");
			if (count == 0)
				request.setAttribute("errorMessage", "コースを登録できませんでした。もう一度やり直してください");

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		// 一覧画面に遷移する
		if (updatemode == 1)
			request.getRequestDispatcher("register-textbook").forward(request, response);
		if (updatemode == 2)
			request.getRequestDispatcher("update-textbook").forward(request, response);

	}

}
