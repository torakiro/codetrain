package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.CorpPersonDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class ManageUserServlet
 */
@WebServlet("/manage-user")
public class ManageUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CustomerLoginServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManageUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (!Common.isCustomerLoggedIn(session, response, logger)) return;

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 利用者一覧を取得する
			UserDao userDao = new UserDao(conn);
			List<UserDto> userList = userDao.selectByCorpId(((CorpPersonDto)session.getAttribute("customer")).getCorp_id());

			// 利用者一覧情報をリクエストに保持する
			request.setAttribute("userList", userList);

			// 利用者一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/manage-user.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			request.getRequestDispatcher("customer-system-error.jsp").forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
