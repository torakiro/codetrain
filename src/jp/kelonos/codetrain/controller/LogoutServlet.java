package jp.kelonos.codetrain.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(LogoutServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		boolean operator = false;
		boolean customer = false;
		boolean user = false;
		// セッションオブジェクトを破棄する
		HttpSession session = request.getSession(false);

		if (session == null && request.getParameter("func") != null) {
			if (String.valueOf(Common.USER).equals(request.getParameter("func"))) {
				response.sendRedirect("system-error.jsp");
				return;
			}
			if (String.valueOf(Common.CUSTOMER).equals(request.getParameter("func"))) {
				response.sendRedirect("customer-system-error.jsp");
				return;
			}
			if (String.valueOf(Common.OPERATION).equals(request.getParameter("func"))) {
				response.sendRedirect("operation-system-error.jsp");
				return;
			}
		}

		if (session == null) {
			response.sendRedirect("system-error.jsp");
			return;
		}

		if(session.getAttribute("operator")!=null) {
			operator=true;
		}else if(session.getAttribute("customer")!=null){
			customer=true;
		}else if(session.getAttribute("user")!=null){
			user=true;
		}

		session.removeAttribute("oprator");
		session.removeAttribute("customer");
		session.removeAttribute("user");
		session.invalidate();

		// ログイン画面に遷移する
		if(operator==true) {
			if (String.valueOf(Common.USER).equals(request.getParameter("func"))) {
				response.sendRedirect("operator-system-error.jsp");
				return;
			}
			if (String.valueOf(Common.CUSTOMER).equals(request.getParameter("func"))) {
				response.sendRedirect("operator-system-error.jsp");
				return;
			}
			response.sendRedirect("operator-login.jsp");
			return;
		}else if(customer==true){
			if (String.valueOf(Common.USER).equals(request.getParameter("func"))) {
				response.sendRedirect("customer-system-error.jsp");
				return;
			}
			if (String.valueOf(Common.OPERATION).equals(request.getParameter("func"))) {
				response.sendRedirect("customer-system-error.jsp");
				return;
			}
			response.sendRedirect("customer-login.jsp");
			return;
		}else if(user==true){
			if (String.valueOf(Common.CUSTOMER).equals(request.getParameter("func"))) {
				response.sendRedirect("system-error.jsp");
				return;
			}
			if (String.valueOf(Common.OPERATION).equals(request.getParameter("func"))) {
				response.sendRedirect("system-error.jsp");
				return;
			}
			response.sendRedirect("user_index.jsp");
			return;
		}

		if (request.getParameter("func") != null) {
			if (String.valueOf(Common.USER).equals(request.getParameter("func"))) {
				response.sendRedirect("system-error.jsp");
				return;
			}
			if (String.valueOf(Common.CUSTOMER).equals(request.getParameter("func"))) {
				response.sendRedirect("customer-system-error.jsp");
				return;
			}
			if (String.valueOf(Common.OPERATION).equals(request.getParameter("func"))) {
				response.sendRedirect("operation-system-error.jsp");
				return;
			}
		}
		logger.error("{} {} Error: ユーザ情報・遷移元機能の情報がないログアウト操作");

		//システムエラー画面に遷移
		response.sendRedirect("system-error.jsp");
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
