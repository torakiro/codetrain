package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.BusinessLogicException;
import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CategoryDao;
import jp.kelonos.codetrain.dao.CourseDao;
import jp.kelonos.codetrain.dao.UserRequestDao;
import jp.kelonos.codetrain.dto.UserDto;
import jp.kelonos.codetrain.dto.UserRequestDto;

/**
 * Servlet implementation class AddReplyServlet
 */
@WebServlet("/add-request")
public class AddRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddRequestServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.USER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);
		if (!Common.isUserLoggedIn(sess, response, logger))
			return;

		UserDto user = (UserDto) sess.getAttribute("user");
		if (user != null) {
			if (!user.isPasswordsetFlg()) {
				response.sendRedirect("logout");
				return;
			}
		} else {
			handling(request, response, "courseMessage", "ログインしてください", "list-course");
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		int categoryId = 0;
		int courseId = 0;
		String text = "";
		int user_id = 0;
		try {

			categoryId = Integer.parseInt(request.getParameter("categoryId"));
			courseId = Integer.parseInt(request.getParameter("courseId"));
			user_id = user.getId();
			text = request.getParameter("text");

			if (categoryId == 0)
				throw new Exception("text of reply is null");
			if (courseId == 0)
				throw new Exception("text of reply is null");
			if (text ==null || text.replaceAll(" ", "").replaceAll("　", "").equals("") ) {
				handling(request, response, "settingResultMessage", "本文を入力してください", "user-contact");
				return;
			}
			if (text.length() > 65535) {
				handling(request, response, "settingResultMessage", "入力文字数が多いです", "user-contact");
				return;
			}

		} catch (NumberFormatException e) {
			//コースIDにintにできない値が入ったとき
			handling(request, response, "settingResultMessage", "カテゴリ、コースが不正です", "user-contact");
			return;

		} catch (Exception e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			handling(request, response, "settingResultMessage", "入力してください", "user-contact");
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			try {

				conn.setAutoCommit(false);

				UserRequestDao requestDao = new UserRequestDao(conn);
				UserRequestDto dto = new UserRequestDto();
				CategoryDao categoryDao = new CategoryDao(conn);
				CourseDao courseDao = new CourseDao(conn);

				if (!categoryDao.exists(categoryId)) {
					handling(request, response, "settingResultMessage", "カテゴリが存在していません", "user-contact");
					return;
				}

				if (!courseDao.exists(courseId)) {
					handling(request, response, "settingResultMessage", "コースが存在していません", "user-contact");
					return;
				}


				dto.setCategory_id(categoryId);
				dto.setCourse_id(courseId);
				dto.setUser_id(user_id);
				dto.setText(text);
				dto.setUpdate_user(user.getId());

				int count = requestDao.insert(dto);
				if (count == 0)
					throw new BusinessLogicException("排他制御（楽観ロック）例外");

				// コミットする
				conn.commit();

			} catch (BusinessLogicException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				handling(request,response,"settingResultMessage", "送信に失敗しました - 排他制御","user-contact");
				return;
			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("system-error.jsp");
			return;
		}

		handling(request,response,"request_user_message", "連絡しました","user-contact");
	}

	private void handling(HttpServletRequest request, HttpServletResponse response, String field,
			String message, String redirectTarget)
			throws IOException {
		HttpSession session = request.getSession(true);
		session.setAttribute(field, message);
		response.sendRedirect(redirectTarget);
	}

}
