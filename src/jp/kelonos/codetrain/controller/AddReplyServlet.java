package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.BusinessLogicException;
import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.RequestReplyDao;
import jp.kelonos.codetrain.dao.UserRequestDao;
import jp.kelonos.codetrain.dto.RequestReplyDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class AddReplyServlet
 */
@WebServlet("/add-reply")
public class AddReplyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddReplyServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);

		if (!Common.isOperatorLoggedIn(sess, response, logger)) return;

		StaffDto operator = (StaffDto)sess.getAttribute("operator");
		if (!operator.getAuthority_group().canReply_message()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		int request_id = 0;
		String text = "";
		int user_id = 0;
		try {

			request_id = Integer.parseInt(request.getParameter("request_id"));
			user_id = Integer.parseInt(request.getParameter("user_id"));
			text = request.getParameter("text");
			if (text == null) throw new Exception("text of reply is null");

		} catch (Exception e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}
		if (text == null || "".equals(text)) {

			sess.setAttribute("errorMessage", "本文を入力してください");
		}
		if (text.length() > 65535) {

			sess.setAttribute("errorMessage", "入力文字数が多いです");
		}

		if (sess.getAttribute("errorMessage") != null) {
			sess.setAttribute("reply_text", text);

			response.sendRedirect("list-message?id=" + user_id);
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			try {

				conn.setAutoCommit(false);

				RequestReplyDao replyDao = new RequestReplyDao(conn);
				UserRequestDao requestDao = new UserRequestDao(conn);
				RequestReplyDto dto = new RequestReplyDto();

				dto.setRequest_id(request_id);
				dto.setStaff_name(operator.getName());
				dto.setCreated_at(Timestamp.valueOf(LocalDateTime.now()));
				dto.setText(text);
				dto.setUpdate_user(operator.getId());

				int count = replyDao.insert(dto);
				if (count == 0) throw new BusinessLogicException("排他制御（楽観ロック）例外");

				count = requestDao.updateReplyFlg(request_id, true);
				if (count == 0) throw new BusinessLogicException("排他制御（楽観ロック）例外");

				// コミットする
				conn.commit();

			} catch (BusinessLogicException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				request.setAttribute("request_reply_message", "返信に失敗しました - 排他制御");

				// 法人一覧に遷移する
				//request.getRequestDispatcher("list-customer").forward(request, response);

				// チャンネル一覧画面に遷移する
				response.sendRedirect("list-message?id=" + user_id);
				return;

			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		sess.setAttribute("request_reply_message", "返信しました");

		// チャンネル一覧画面に遷移する
		response.sendRedirect("list-message?id=" + user_id);

	}

}
