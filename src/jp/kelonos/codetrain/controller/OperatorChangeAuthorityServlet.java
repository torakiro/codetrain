package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.AuthorityGroupDao;
import jp.kelonos.codetrain.dao.StaffDao;
import jp.kelonos.codetrain.dto.AuthorityGroupDto;
import jp.kelonos.codetrain.dto.StaffDto;

/**
 * Servlet implementation class OperatorChangeAuthorityServlet
 */
@WebServlet("/operator-change-authority")
public class OperatorChangeAuthorityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(OperatorChangeAuthorityServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// トップページに遷移する
		Common.logout(Common.OPERATION, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession sess = request.getSession(false);

		if (!Common.isOperatorLoggedIn(sess, response, logger))
			return;

		StaffDto user = (StaffDto) sess.getAttribute("operator");
		if (!user.getAuthority_group().canCreate_operator()) {
			logger.warn("権限なし");
			Common.logout(Common.OPERATION, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		StaffDto dto = new StaffDto();
		try {
			AuthorityGroupDto authDto = new AuthorityGroupDto();
			authDto.setId(Integer.parseInt(request.getParameter("authorityGroup_id")));
			dto.setAuthority_group(authDto);
			dto.setId(Integer.parseInt(request.getParameter("staffId")));
		} catch (Exception e) {
			e.printStackTrace(System.out);
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		dto.setName(request.getParameter("name"));

		sess.removeAttribute("errorMessage");
		sess.removeAttribute("message");

		if (request.getParameter("name") == null || "".equals(request.getParameter("name"))) {
			sess.setAttribute("errorMessage", "氏名を入力してください");
		}

		if (dto.getName().length() > 16) {
			sess.setAttribute("errorMessage", "入力文字数が多いです");
		}
		if (sess.getAttribute("errorMessage") != null) {
			// 入力画面に戻る
			sess.setAttribute("name", dto.getName());
			response.sendRedirect("list-operator");
			return;
		}

		dto.setUpdate_user(user.getId());

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// チャンネル一覧を取得する
			StaffDao dao = new StaffDao(conn);

			dao.updateNameAndAuthorityGroup(dto);

			if (dto.getId() == user.getId()) {
				user.setName(dto.getName());
				AuthorityGroupDao authorityDao = new AuthorityGroupDao(conn);
				user.setAuthority_group(authorityDao.selectByPrimaryKey(dto.getAuthority_group().getId()));
			}

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI());

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("operation-system-error.jsp").forward(request, response);
			return;
		}

		sess.setAttribute("message", "変更しました");
		// 運用管理者一覧画面に遷移する
		response.sendRedirect("list-operator");

	}

}
