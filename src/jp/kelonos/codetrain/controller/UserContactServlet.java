package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.CategoryDao;
import jp.kelonos.codetrain.dao.CourseDao;
import jp.kelonos.codetrain.dao.RequestReplyDao;
import jp.kelonos.codetrain.dao.UserRequestDao;
import jp.kelonos.codetrain.dto.CategoryDto;
import jp.kelonos.codetrain.dto.CourseDto;
import jp.kelonos.codetrain.dto.RequestReplyDto;
import jp.kelonos.codetrain.dto.UserDto;
import jp.kelonos.codetrain.dto.UserRequestDto;

/**
 * Servlet implementation class ListMessageServlet
 */
@WebServlet("/user-contact")
public class UserContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(UserContactServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		HttpSession sess = request.getSession(false);
		if (!Common.isUserLoggedIn(sess, response, logger)) return;

		UserDto user = (UserDto)sess.getAttribute("user");
		if (user != null) {
			if (!user.isPasswordsetFlg()) {
				response.sendRedirect("logout");
				return;
			}
		}
		int user_id  = user.getId();
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			UserRequestDao requestDao = new UserRequestDao(conn);
			RequestReplyDao replyDao = new RequestReplyDao(conn);
			CategoryDao categoryDao = new CategoryDao(conn);
			CourseDao courseDao = new CourseDao(conn);

			List<UserRequestDto> requestList = requestDao.findAllByUser(user_id);
			Map<Integer, List<RequestReplyDto>> replyMap = replyDao.getMapByUserId(user_id);
			List<CategoryDto> categoryDtos = categoryDao.selectAll();
			List<CourseDto> courseDtos = courseDao.findAll();

			request.setAttribute("requestList", requestList);
			request.setAttribute("replyMap", replyMap);
			request.setAttribute("categoryList", categoryDtos);
			request.setAttribute("courseList", courseDtos);

			//利用者IDをリクエストに保持する
			request.setAttribute("userId", user_id);
			//利用者名をリクエストに保持する
			request.setAttribute("userName", user);

			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/user-contact.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("system-error.jsp");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
