package jp.kelonos.codetrain.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.kelonos.codetrain.Common;
import jp.kelonos.codetrain.DataSourceManager;
import jp.kelonos.codetrain.dao.LearnHistoryDao;
import jp.kelonos.codetrain.dao.UserDao;
import jp.kelonos.codetrain.dto.LearnHistoryDto;
import jp.kelonos.codetrain.dto.UserDto;

/**
 * Servlet implementation class ViewUserServlet
 */
@WebServlet("/view-user")
public class ViewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(CustomerLoginServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViewUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		Common.logout(Common.CUSTOMER, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// フォームのデータを取得する
		int id = Integer.parseInt(request.getParameter("id"));

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (!Common.isCustomerLoggedIn(session, response, logger)) return;

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 学習状況を取得する
			LearnHistoryDao learnHistoryDao = new LearnHistoryDao(conn);
			List<LearnHistoryDto> historyList = learnHistoryDao.findFullHistoryByUserId(id);
			Map<Integer, LearnHistoryDto> courseProgressList = learnHistoryDao.selectCourseProgressesByUserId(id);

			// 利用者情報を取得する
			UserDao userDao = new UserDao(conn);
			UserDto user = userDao.selectById(id);

			// 学習状況をリクエストに保持する
			request.setAttribute("historyList", historyList);
			request.setAttribute("courseProgress", courseProgressList);
			request.setAttribute("user",user);

			// 利用者詳細画面に遷移する
			request.getRequestDispatcher("WEB-INF/view-user.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("customer-system-error.jsp");
			return;
		}

	}

}
