package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class RequestReplyDto {

	// 返信ID
	private int id;

	// 本文
	private String text;

	// 作成日時
	private Timestamp created_at;

	// 返信先要望ID
	private int request_id;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	// 返信者の名前
	private String staff_name;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text セットする text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return text[]
	 */
	public String[] getSplitedText() {
		String[] texts =  text.split("\r\n");
        return texts;
    }

	/**
	 * @return created_at
	 */
	public Timestamp getCreated_at() {
		return created_at;
	}

	/**
	 * @param created_at セットする created_at
	 */
	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	/**
	 * @return request_id
	 */
	public int getRequest_id() {
		return request_id;
	}

	/**
	 * @param request_id セットする request_id
	 */
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

	public String getStaff_name() {
		return staff_name;
	}

	public void setStaff_name(String staff_name) {
		this.staff_name = staff_name;
	}


}
