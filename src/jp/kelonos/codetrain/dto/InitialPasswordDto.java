package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class InitialPasswordDto {

	// 初期パスワードID(PK)
	private int id;

	// 初期パスワード
	private String initial_password;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return initial_password
	 */
	public String getInitial_password() {
		return initial_password;
	}

	/**
	 * @param initial_password セットする initial_password
	 */
	public void setInitial_password(String initial_password) {
		this.initial_password = initial_password;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

}
