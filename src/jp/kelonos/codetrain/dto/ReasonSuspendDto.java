package jp.kelonos.codetrain.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class ReasonSuspendDto {

	// 利用者ID(PK)
	private int id;

	private String reason;

	private int user_id;

	private boolean deleteFlg;

	private Integer empno;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	private int corp_id;
	private String user_name;
	private Date suspend_month;
	private int state;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public boolean isDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(boolean deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public Timestamp getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	public int getUpdate_user() {
		return update_user;
	}

	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

	public int getCorp_id() {
		return corp_id;
	}

	public void setCorp_id(int corp_id) {
		this.corp_id = corp_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String name) {
		this.user_name = name;
	}

	public Date getSuspend_month() {
		return suspend_month;
	}

	public void setSuspend_month(Date suspend_month) {
		this.suspend_month = suspend_month;
	}

	public Integer getEmpno() {
		return empno;
	}

	public void setEmpno(Integer empno) {
		this.empno = empno;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}
