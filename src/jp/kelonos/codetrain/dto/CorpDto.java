package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class CorpDto {

	//法人ID(PK)
	private int id;

	//法人名
	private String name;

	//ドメイン
	private String domain;

	// 法人窓口アカウントID
	private int corp_person_id;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * @param domain セットする domain
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * @return corp_person_id
	 */
	public int getCorp_person_id() {
		return corp_person_id;
	}

	/**
	 * @param corp_person_id セットする corp_person_id
	 */
	public void setCorp_person_id(int corp_person_id) {
		this.corp_person_id = corp_person_id;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

}
