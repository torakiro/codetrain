package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class QuestionDto {
	//問題ID
	private int id;

	//カリキュラムID
	private int curriculum_id;

	//問題名
	private String name;

	//問題
	private String question;

	//選択肢1
	private String choice1;

	//選択肢2
	private String choice2;

	//選択肢3
	private String choice3;

	//選択肢4
	private String choice4;

	//解答
	private int ansno;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return curriculum_id
	 */
	public int getCurriculum_id() {
		return curriculum_id;
	}

	/**
	 * @param curriculum_id セットする curriculum_id
	 */
	public void setCurriculum_id(int curriculum_id) {
		this.curriculum_id = curriculum_id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question セットする question
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return choice1
	 */
	public String getChoice1() {
		return choice1;
	}

	/**
	 * @param choice1 セットする choice1
	 */
	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}

	/**
	 * @return choice2
	 */
	public String getChoice2() {
		return choice2;
	}

	/**
	 * @param choice2 セットする choice2
	 */
	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}

	/**
	 * @return choice3
	 */
	public String getChoice3() {
		return choice3;
	}

	/**
	 * @param choice3 セットする choice3
	 */
	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}

	/**
	 * @return choice4
	 */
	public String getChoice4() {
		return choice4;
	}

	/**
	 * @param choice4 セットする choice4
	 */
	public void setChoice4(String choice4) {
		this.choice4 = choice4;
	}

	/**
	 * @return ansno
	 */
	public int getAnsno() {
		return ansno;
	}

	/**
	 * @param ansno セットする ansno
	 */
	public void setAnsno(int ansno) {
		this.ansno = ansno;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

}