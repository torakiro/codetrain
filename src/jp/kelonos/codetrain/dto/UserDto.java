package jp.kelonos.codetrain.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class UserDto {

	// 利用者ID(PK)
	private int id;

	// ログインID
	private String loginId;

	// ログインパスワード
	private String password;

	// 氏名
	private String name;

	// 状態
	private int state;

	// 初期パスワード設定フラグ
	private boolean passwordsetFlg;

	// 削除フラグ
	private boolean deleteFlg;

	// 法人ID
	private int corp_id;

	// 初期パスワードID
	private int initialpassword_id;

	// 社員番号
	private Integer empno;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	// コース名
	private String course_name;

	// 進捗率
	private int progress;

	// 正解率
	private int correct_percent;

	private Date suspend_month;

	public static final int STATE_ACTIVE = 0;
	public static final int STATE_SUSPENDED = 1;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId セットする loginId
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password セットする password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state セットする state
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return passwordsetFlg
	 */
	public boolean isPasswordsetFlg() {
		return passwordsetFlg;
	}

	/**
	 * @param passwordsetFlg セットする passwordsetFlg
	 */
	public void setPasswordsetFlg(boolean passwordsetFlg) {
		this.passwordsetFlg = passwordsetFlg;
	}

	/**
	 * @return deleteFlg
	 */
	public boolean isDeleteFlg() {
		return deleteFlg;
	}

	/**
	 * @param deleteFlg セットする deleteFlg
	 */
	public void setDeleteFlg(boolean deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	/**
	 * @return corp_id
	 */
	public int getCorp_id() {
		return corp_id;
	}

	/**
	 * @param corp_id セットする corp_id
	 */
	public void setCorp_id(int corp_id) {
		this.corp_id = corp_id;
	}

	/**
	 * @return initialpassword_id
	 */
	public int getInitialpassword_id() {
		return initialpassword_id;
	}

	/**
	 * @param initialpassword_id セットする initialpassword_id
	 */
	public void setInitialpassword_id(int initialpassword_id) {
		this.initialpassword_id = initialpassword_id;
	}

	/**
	 * @return empno
	 */
	public Integer getEmpno() {
		return empno;
	}

	/**
	 * @param empno セットする empno
	 */
	public void setEmpno(Integer empno) {
		this.empno = empno;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

	/**
	 * @return course_name
	 */
	public String getCourse_name() {
		return course_name;
	}

	/**
	 * @param course_name セットする course_name
	 */
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	/**
	 * @return progress
	 */
	public int getProgress() {
		return progress;
	}

	/**
	 * @param progress セットする progress
	 */
	public void setProgress(int progress) {
		this.progress = progress;
	}

	/**
	 * @return correct_percent
	 */
	public int getCorrect_percent() {
		return correct_percent;
	}

	/**
	 * @param correct_percent セットする correct_percent
	 */
	public void setCorrect_percent(int correct_percent) {
		this.correct_percent = correct_percent;
	}

	public Date getSuspend_month() {
		return suspend_month;
	}

	public void setSuspend_month(Date suspend_month) {
		this.suspend_month = suspend_month;
	}


}
