package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class AnswerDto {
	//解答ID
	private int id;

	//クエスチョンID
	private int question_id;

	//カリキュラムID
	private int curriculum_id;

	//解答
	private int answer;

	//正答
	private Boolean correct;

	//ユーザーID
	private int user_id;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return question_id
	 */
	public int getQuestion_id() {
		return question_id;
	}

	/**
	 * @param question_id セットする question_id
	 */
	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}

	/**
	 * @return curriculum_id
	 */
	public int getCurriculum_id() {
		return curriculum_id;
	}

	/**
	 * @param curriculum_id セットする curriculum_id
	 */
	public void setCurriculum_id(int curriculum_id) {
		this.curriculum_id = curriculum_id;
	}

	/**
	 * @return answer
	 */
	public int getAnswer() {
		return answer;
	}

	/**
	 * @param answer セットする answer
	 */
	public void setAnswer(int answer) {
		this.answer = answer;
	}

	/**
	 * @return correct
	 */
	public Boolean getCorrect() {
		return correct;
	}

	/**
	 * @param correct セットする correct
	 */
	public void setCorrect(Boolean correct) {
		this.correct = correct;
	}

	/**
	 * @return user_id
	 */
	public int getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id セットする user_id
	 */
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}
}