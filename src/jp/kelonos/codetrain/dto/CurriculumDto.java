package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class CurriculumDto {

	// カリキュラムID(PK)
	private int id;

	// タイトル
	private String name;

	// コースID
	private int course_id;

	//試験フラグ
	private boolean examFlg;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return course_id
	 */
	public int getCourse_id() {
		return course_id;
	}

	/**
	 * @param course_id セットする course_id
	 */
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	/**
	 * @return examFlg
	 */
	public boolean isExamFlg() {
		return examFlg;
	}

	/**
	 * @param examFlg セットする examFlg
	 */
	public void setExamFlg(boolean examFlg) {
		this.examFlg = examFlg;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

}
