package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class LearnHistoryDto {

	// 学習履歴ID(PK)
	private int id;

	// カリキュラムID
	private int curriculum_id;

	//curriculum name
	private String curriculum_name;

	//course id
	private int course_id;

	//category name
	private String category_name;

	private String course_name;

	// 進捗率
	private int progress;

	// 受講開始日時
	private Timestamp started_at;

	// 正解率
	private int correct_percent;

	// 利用者ID
	private int user_id;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return curriculum_id
	 */
	public int getCurriculum_id() {
		return curriculum_id;
	}

	/**
	 * @param curriculum_id セットする curriculum_id
	 */
	public void setCurriculum_id(int curriculum_id) {
		this.curriculum_id = curriculum_id;
	}

	/**
	 * @return progress
	 */
	public int getProgress() {
		return progress;
	}

	/**
	 * @param progress セットする progress
	 */
	public void setProgress(int progress) {
		this.progress = progress;
	}

	/**
	 * @return started_at
	 */
	public Timestamp getStarted_at() {
		return started_at;
	}

	/**
	 * @param started_at セットする started_at
	 */
	public void setStarted_at(Timestamp started_at) {
		this.started_at = started_at;
	}

	/**
	 * @return correct_percent
	 */
	public int getCorrect_percent() {
		return correct_percent;
	}

	/**
	 * @param correct_percent セットする correct_percent
	 */
	public void setCorrect_percent(int correct_percent) {
		this.correct_percent = correct_percent;
	}

	/**
	 * @return user_id
	 */
	public int getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id セットする user_id
	 */
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

	public String getCurriculum_name() {
		return curriculum_name;
	}

	public void setCurriculum_name(String curriculum_name) {
		this.curriculum_name = curriculum_name;
	}

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
}
