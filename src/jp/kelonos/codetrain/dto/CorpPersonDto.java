package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class CorpPersonDto {

	// 法人窓口アカウントID(PK)
	private int id;

	// 氏名
	private String name;

	// メールアドレス
	private String email;

	// パスワード
	private String password;

	// 役職
	private String position;

	// 初期パスワード設定フラグ
	private boolean passwordsetFlg;

	// 更新日時
	private Timestamp update_time;

	//更新ユーザ
	private int update_user;

	// 法人ID
	private int corp_id;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email セットする email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password セットする password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * @param position セットする position
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * @return passwordsetFlg
	 */
	public boolean isPasswordsetFlg() {
		return passwordsetFlg;
	}

	/**
	 * @param passwordsetFlg セットする passwordsetFlg
	 */
	public void setPasswordsetFlg(boolean passwordsetFlg) {
		this.passwordsetFlg = passwordsetFlg;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

	/**
	 * @return corp_id
	 */
	public int getCorp_id() {
		return corp_id;
	}

	/**
	 * @param corp_id セットする corp_id
	 */
	public void setCorp_id(int corp_id) {
		this.corp_id = corp_id;
	}

}
