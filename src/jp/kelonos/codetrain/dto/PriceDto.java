package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class PriceDto {
	private int monthlyprice_active_user;
	private int monthlyprice_suspended_user;

	private Timestamp update_time;
	private int update_user;

	public Timestamp getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}
	public int getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}
	public int getMonthlyprice_active_user() {
		return monthlyprice_active_user;
	}
	public void setMonthlyprice_active_user(int monthlyprice_active_user) {
		this.monthlyprice_active_user = monthlyprice_active_user;
	}
	public int getMonthlyprice_suspended_user() {
		return monthlyprice_suspended_user;
	}
	public void setMonthlyprice_suspended_user(int monthlyprice_suspended_user) {
		this.monthlyprice_suspended_user = monthlyprice_suspended_user;
	}
}