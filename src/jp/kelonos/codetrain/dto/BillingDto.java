package jp.kelonos.codetrain.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class BillingDto {

	// 請求ID(PK)
	private int id;

	// 法人ID
	private int corp_id;

	// 請求金額
	private int price;

	// 請求済みフラグ
	private boolean billedFlg;

	// 請求月
	private Date billing_month;

	private int cnt_user_suspended;

	private int cnt_user_active;

	private int cnt_user_newly_suspended;

	private int price_user_suspended;

	private int price_user_active;

	private int price_user_newly_suspended;

	public int getPrice_user_suspended() {
		return price_user_suspended;
	}

	public void setPrice_user_suspended(int price_user_suspended) {
		this.price_user_suspended = price_user_suspended;
	}

	public int getPrice_user_active() {
		return price_user_active;
	}

	public void setPrice_user_active(int price_user_active) {
		this.price_user_active = price_user_active;
	}

	public int getPrice_user_newly_suspended() {
		return price_user_newly_suspended;
	}

	public void setPrice_user_newly_suspended(int price_user_newly_suspended) {
		this.price_user_newly_suspended = price_user_newly_suspended;
	}

	// 請求日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return corp_id
	 */
	public int getCorp_id() {
		return corp_id;
	}

	/**
	 * @param corp_id セットする corp_id
	 */
	public void setCorp_id(int corp_id) {
		this.corp_id = corp_id;
	}

	/**
	 * @return price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price セットする price
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return billedFlg
	 */
	public boolean isBilledFlg() {
		return billedFlg;
	}

	/**
	 * @param billedFlg セットする billedFlg
	 */
	public void setBilledFlg(boolean billedFlg) {
		this.billedFlg = billedFlg;
	}

	/**
	 * @return billing_month
	 */
	public Date getBilling_month() {
		return billing_month;
	}

	/**
	 * @param billing_month セットする billing_month
	 */
	public void setBilling_month(Date billing_month) {
		this.billing_month = billing_month;
	}

	public int getCnt_user_suspended() {
		return cnt_user_suspended;
	}

	public void setCnt_user_suspended(int cnt_user_suspended) {
		this.cnt_user_suspended = cnt_user_suspended;
	}

	public int getCnt_user_active() {
		return cnt_user_active;
	}

	public void setCnt_user_active(int cnt_user_active) {
		this.cnt_user_active = cnt_user_active;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

	public int getCnt_user_newly_suspended() {
		return cnt_user_newly_suspended;
	}

	public void setCnt_user_newly_suspended(int cnt_user_newly_suspended) {
		this.cnt_user_newly_suspended = cnt_user_newly_suspended;
	}

}
