package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class CourseDto {

	// コースID
	private int id;

	// コース名
	private String name;

	// コース概要
	private String overview;

	// 前提条件
	private String requirement;

	// ゴール
	private String goal;

	// 学習目安
	private Integer time_approx;

	// カテゴリID
	private int category_id;

	//フリーコースフラグ
	private boolean freecourseFlg;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return overview
	 */
	public String getOverview() {
		return overview;
	}

	/**
	 * @param overview セットする overview
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}

	/**
	 * @return requirement
	 */
	public String getRequirement() {
		return requirement;
	}

	/**
	 * @param requirement セットする requirement
	 */
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	/**
	 * @return goal
	 */
	public String getGoal() {
		return goal;
	}

	/**
	 * @param goal セットする goal
	 */
	public void setGoal(String goal) {
		this.goal = goal;
	}

	/**
	 * @return time_approx
	 */
	public Integer getTime_approx() {
		return time_approx;
	}

	/**
	 * @param time_approx セットする time_approx
	 */
	public void setTime_approx(Integer time_approx) {
		this.time_approx = time_approx;
	}

	/**
	 * @return category_id
	 */
	public int getCategory_id() {
		return category_id;
	}

	/**
	 * @param category_id セットする category_id
	 */
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public boolean isFreecourseFlg() {
		return freecourseFlg;
	}

	public void setFreecourseFlg(boolean freecourseFlg) {
		this.freecourseFlg = freecourseFlg;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}
}
