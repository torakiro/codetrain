package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class TextDto {

	// テキストID(PK)
	private int id;

	// タイトル
	private String title;

	// 本文
	private String text;

	// カリキュラムID
	private int curriculum_id;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	//紐づいたカテゴリ名（inner join時に使用）
	private String category_name;

	//紐づいたコース名（inner join時に使用）
	private String course_name;

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title セットする title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text セットする text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return curriculum_id
	 */
	public int getCurriculum_id() {
		return curriculum_id;
	}

	/**
	 * @param curriculum_id セットする curriculum_id
	 */
	public void setCurriculum_id(int curriculum_id) {
		this.curriculum_id = curriculum_id;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

}
