package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class UserRequestDto {

	// 要望ID
	private int id;

	// カテゴリID
	private int category_id;

	// コースID
	private int course_id;

	// 連絡事項
	private String text;

	// 返信済みフラグ
	private boolean repliedFlg;

	// 利用者ID
	private int user_id;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	// 利用者の名前（userテーブルでinner join時に使用）
	private String user_name;

	// 利用者の名前（categoryテーブルでinner join時に使用）
	private String category_name;

	// 利用者の名前（courseテーブルでinner join時に使用）
	private String course_name;

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return category_id
	 */
	public int getCategory_id() {
		return category_id;
	}

	/**
	 * @param category_id セットする category_id
	 */
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	/**
	 * @return course_id
	 */
	public int getCourse_id() {
		return course_id;
	}

	/**
	 * @param course_id セットする course_id
	 */
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text セットする text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return text[]
	 */
	public String[] getSplitedText() {
		String[] texts =  text.split("\r\n");
        return texts;
    }

	/**
	 * @return repliedFlg
	 */
	public boolean isRepliedFlg() {
		return repliedFlg;
	}

	/**
	 * @param repliedFlg セットする repliedFlg
	 */
	public void setRepliedFlg(boolean repliedFlg) {
		this.repliedFlg = repliedFlg;
	}

	/**
	 * @return user_id
	 */
	public int getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id セットする user_id
	 */
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

}
