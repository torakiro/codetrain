package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class HandlingStaffDto {

	// 主キー(?)(PK)
	private int id;

	// 法人ID
	private int corp_id;

	// 自社担当者ID
	private int staff_id;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return corp_id
	 */
	public int getCorp_id() {
		return corp_id;
	}

	/**
	 * @param corp_id セットする corp_id
	 */
	public void setCorp_id(int corp_id) {
		this.corp_id = corp_id;
	}

	/**
	 * @return staff_id
	 */
	public int getStaff_id() {
		return staff_id;
	}

	/**
	 * @param staff_id セットする staff_id
	 */
	public void setStaff_id(int staff_id) {
		this.staff_id = staff_id;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

}
