package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class StaffDto {

	//運営者アカウントID(PK)
	private int id;

	//ログインID
	private String login_id;

	//ログインパスワード
	private String password;

	// 氏名
	private String name;

	// アカウントグループ
	private AuthorityGroupDto authority_group;

	// 更新日時
	private Timestamp update_time;

	// 更新ユーザ
	private int update_user;

	// 初期パスワード設定済みフラグ
	private boolean passwordsetFlg;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return login_id
	 */
	public String getLogin_id() {
		return login_id;
	}

	/**
	 * @param login_id セットする login_id
	 */
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password セットする password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return authority_group
	 */
	public AuthorityGroupDto getAuthority_group() {
		return authority_group;
	}

	/**
	 * @param authority_group セットする authority_group
	 */
	public void setAuthority_group(AuthorityGroupDto authority_group) {
		this.authority_group = authority_group;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

	/**
	 *
	 * @return isPasswordsetFlgを取得
	 */
	public boolean isPasswordsetFlg() {
		// TODO 自動生成されたメソッド・スタブ
		return this.passwordsetFlg;
	}

	public void setPasswordsetFlg(boolean passwordsetFlg) {
		this.passwordsetFlg = passwordsetFlg;
	}

	public void setState(int State) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
