package jp.kelonos.codetrain.dto;

import java.sql.Timestamp;

public class AuthorityGroupDto {

	// 運営者アカウントグループID(PK)
	private int id;

	// 役割名
	private String name;

	// 画面遷移権限
	private boolean show_operatorlist;
	private boolean create_operator;
	private boolean show_corplist;
	private boolean create_corp;
	private boolean create_text;
	private boolean edit_text;
	private boolean show_text;
	private boolean reply_message;
	private boolean show_message;
	private boolean show_message_userlist;
	private boolean control_billing;

	private int initial_activity = NO_INITIAL_ACTIVITY;
	public static final int NO_INITIAL_ACTIVITY = 0;
	public static final int INITIAL_SHOW_CORPLIST = 1;
	public static final int INITIAL_SHOW_TEXT = 2;
	public static final int INITIAL_SHOW_OPERATORLIST = 3;
	public static final int INITIAL_SHOW_MESSAGE_USERLIST = 4;

	// 更新日時
	Timestamp update_time;

	// 更新ユーザ
	int update_user;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	public boolean canShow_operatorlist() {
		return show_operatorlist;
	}

	public void setShow_operatorlist(boolean show_operatorlist) {
		this.show_operatorlist = show_operatorlist;
	}

	public boolean canCreate_operator() {
		return create_operator;
	}

	public void setCreate_operator(boolean create_operator) {
		this.create_operator = create_operator;
	}

	public boolean canShow_corplist() {
		return show_corplist;
	}

	public void setShow_corplist(boolean show_corplist) {
		this.show_corplist = show_corplist;
	}

	public boolean canCreate_corp() {
		return create_corp;
	}

	public void setCreate_corp(boolean create_corp) {
		this.create_corp = create_corp;
	}

	public boolean canCreate_text() {
		return create_text;
	}

	public void setCreate_text(boolean create_text) {
		this.create_text = create_text;
	}

	public boolean canEdit_text() {
		return edit_text;
	}

	public void setEdit_text(boolean edit_text) {
		this.edit_text = edit_text;
	}

	public boolean canShow_text() {
		return show_text;
	}

	public void setShow_text(boolean show_text) {
		this.show_text = show_text;
	}

	public boolean canReply_message() {
		return reply_message;
	}

	public void setReply_message(boolean reply_message) {
		this.reply_message = reply_message;
	}

	public boolean canShow_message() {
		return show_message;
	}

	public void setShow_message(boolean show_message) {
		this.show_message = show_message;
	}

	public boolean canShow_message_userlist() {
		return show_message_userlist;
	}

	public void setShow_message_userlist(boolean show_message_userlist) {
		this.show_message_userlist = show_message_userlist;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return update_time
	 */
	public Timestamp getUpdate_time() {
		return update_time;
	}

	/**
	 * @param update_time セットする update_time
	 */
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}

	/**
	 * @return update_user
	 */
	public int getUpdate_user() {
		return update_user;
	}

	/**
	 * @param update_user セットする update_user
	 */
	public void setUpdate_user(int update_user) {
		this.update_user = update_user;
	}

	public int getInitial_activity() {
		return initial_activity;
	}

	public void setInitial_activity(int initial_activity) {
		this.initial_activity = initial_activity;
	}

	public boolean canControl_billing() {
		return control_billing;
	}

	public void setControl_billing(boolean control_billing) {
		this.control_billing = control_billing;
	}
}
