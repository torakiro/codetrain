<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/header.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>法人連絡窓口ログイン</title>
</head>
<body>
	<div class="container">
		<div style="text-align: center">
			<img src="img/CodeTrain2x.png" height="162" width="232" />
		</div>
		<p class="h4 mt-3 p-3 bg-light text-info rounded text-center">法人連絡窓口ログイン</p>
		<div style="text-align: center">
			<p class="text-danger">
				&nbsp;<c:out value="${ errorMessage }" />&nbsp;
				<%
					session.removeAttribute("errorMessage");
				%>
			</p>
			<div class="row">
				<div class="col-4"></div>
				<div class="form-group col-4">
					<form action="customer-login" method="post">
						<p>
							<input type="email" name="id" placeholder="ログインID"
								class="form-control" autofocus="autofocus" maxlength="512"/>
						</p>
						<p>
							<input type="password" name="password" placeholder="パスワード"
								class="form-control" maxlength="16"/>
						</p>
						<input type="hidden" name="uri" value="${ uri }">
						<button type="submit" class="btn btn-outline-primary">ログイン</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>