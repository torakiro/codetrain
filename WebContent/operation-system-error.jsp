<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
if (session != null) {
	session.removeAttribute("oprator");
	session.removeAttribute("customer");
	session.removeAttribute("user");
	session.invalidate();
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>システムエラー</title>
<%@ include file="/WEB-INF/header.jsp"%>
<link rel="stylesheet" type="text/css" href="css/navbar.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light"
		style="background:linear-gradient(-2deg, rgba(64, 64, 64, 1), rgba(255, 0, 0, 0));">
	<a class="navbar-brand" href="operator-login.jsp"><img
		src="img/CodeTrain.png" height="50" width="72" /></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	</div>
	</nav>
	<div class="container" style="margin-top:20px">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<h1>システムエラーが発生しました。</h1>
			</div>
			<div class="col-sm-2"></div>
		</div>

		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<h3>大変お手数ですが、管理者までお問い合わせください。</h3>
			</div>
			<div class="col-sm-2"></div>
		</div>

		<div style="text-align:center" style="margin-top:100px">
			<a href="operator-login.jsp">トップページに戻る</a>
		</div>
	</div>
</body>
</html>