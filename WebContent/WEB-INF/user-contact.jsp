<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>運営への連絡</title>
<%@ include file="header.jsp"%>
<script type="text/javascript">
<!--
	function submitConfirm() {
		return window.confirm("この内容で送信してよろしいでしょうか？");
	}

	$(function() {
		var $children = $('#courses');
		$children.attr('disabled', 'disabled');
		var original = $children.html();
		var catFlg = false;
		var couFlg = false;
		var texFlg = false;
		judge(catFlg, couFlg, texFlg);

		$('#categorys').change(function() {
			catFlg = false;
			couFlg = false
			var val1 = $(this).val();

			$children.html(original).find('option').each(function() {
				var val2 = $(this).data('val');

				if (val1 != val2) {
					$(this).not(':first-child').remove();
				}
			});

			if ($(this).val() == "0") {
				$children.attr('disabled', 'disabled');
			} else {
				catFlg = true;
				$children.removeAttr('disabled');
			}
			judge(catFlg, couFlg, texFlg);
		});

		$('#courses').change(function() {
			if ($(this).val() == "0") {
				couFlg = false;
			} else {
				couFlg = true;
			}
			judge(catFlg, couFlg, texFlg);
		});

		$("#texts")
				.on(
						"keyup keydown compositionstart compositionupdate compositionend paste cut drop input",
						function() {
							var TargetString = $(this).val();
							TargetString = TargetString.replace(/\s+/g, "");
							if (TargetString.length < 1) {
								texFlg = false;
							} else {
								texFlg = true;
							}
							judge(catFlg, couFlg, texFlg);
						});

	});

	function judge(catFlg, couFlg, texFlg) {
		var $requestButton = $('#requestButton');
		$requestButton.attr('disabled', 'disabled');
		if (catFlg == false || couFlg == false || texFlg == false) {
			$requestButton.attr('disabled', 'disabled');
		} else {
			$requestButton.removeAttr('disabled');
		}
	}
// -->
</script>

<link rel="stylesheet" type="text/css" href="./css/user_request.css">
</head>
<body>
	<%@ include file="user-navbar.jsp"%>
	<div class="container" style="margin-bottom: 50px">

		<div class="row" style="margin-bottom: 10px">
			<div class="col-12">
				<p style="position: relative">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">運営への連絡</p>
				<span
					style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
					<span class="text-danger"> <c:out
							value="${ settingResultMessage }" />
				</span> <span class="text-success"> <c:out
							value="${ request_user_message }" />
				</span>
				</span>
				<%
					session.removeAttribute("settingResultMessage");
				%>
				<%
					session.removeAttribute("request_user_message");
				%>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col">
						<div class="col-lg-12 form-control bg-light">
							<form action="add-request" method="post">
								<div class="row">
									<div class="col-lg-6">
										<label>カテゴリ</label> <select name="categoryId" id="categorys"
											class="d-block w-100"
											style="height: calc(2.25rem +2px); border-radius: .25rem">
											<option value="0" selected="selected">カテゴリを選択</option>
											<c:forEach items="${categoryList}" var="category">
												<option value="${category.id}"><c:out
														value="${category.name}" /></option>
											</c:forEach>
										</select>
									</div>
									<div class="col-lg-6">
										<label>コース</label> <select name="courseId" id="courses"
											class="d-block w-100"
											style="height: calc(2.25rem +2px); border-radius: .25rem"disabled="disabled">
											<option value="0" data-val="0" selected="selected" >コースを選択</option>
											<c:forEach items="${courseList}" var="course">
												<option value="${course.id}"
													data-val="${course.category_id}"><c:out
														value="${course.name}" /></option>
											</c:forEach>
										</select>
									</div>
								</div>
								<!-- 入力フォーム -->
								<div class="row col-lg-12">
									<label class="col-form-label">連絡事項</label>
								</div>
								<div class="row col-lg-12 p-0 m-0">
									<textarea id="texts" name="text" class="form-control w-100"
										maxlength="65535" autofocus="autofocus" rows="4" required><c:out
											value="" /></textarea>
								</div>
								<div class="row col-lg-12 justify-content-end  p-0 m-0">
									<button id="requestButton" type="submit"
										class="btn btn-success" style="margin-top: 5px" disabled="disabled"
										onclick="return submitConfirm()">送信</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<c:forEach items="${requestList}" var="req">
					<div class="row" style="margin-top: 10px">
						<div class="col-12">
							<!-- 利用者からの連絡 -->
							<div class="card float-left "
								style="width: 95%; background: linear-gradient(-45deg, rgba(246, 161, 161, 0.1), rgba(255, 255, 0, 0.2));">
								<div class="card-body"
									style="padding-top: 5px; padding-bottom: 5px;">
									<div class="card-title"
										style="position: relative; margin-bottom: 5px;">
										<span class="text-info" style="">カテゴリ：<c:out
												value="${req.category_name}" /></span> <br> <span
											class="text-info" style="">コース：<c:out
												value="${req.course_name}" /></span> <span class="text"
											style="position: absolute; top: 50%; right: 0%; transform: translate(0, -50%);">日時：<c:out
												value="${req.update_time}" /></span>
									</div>
									<p class="card-text">
										<c:forEach var="reqStr" items="${req.splitedText}">
											<c:out value="${reqStr}" />
											<br>
										</c:forEach>
									</p>
								</div>
							</div>
						</div>
						<div class="col-12" id="replyform_${req.id}"></div>
						<c:forEach items="${replyMap.get(req.id)}" var="reply">
							<div class="col-12">
								<!-- 利用者への返信 -->
								<div class="card float-right"
									style="width: 95%; background: linear-gradient(-45deg, rgba(161, 246, 246, 0.1), rgba(161, 161, 246, 0.1));">
									<div class="card-body"
										style="padding-top: 5px; padding-bottom: 5px;">
										<div class="card-title"
											style="position: relative; margin-bottom: 5px;">
											<span class="text-info" style="">返信者：<c:out
													value="${reply.staff_name}" /></span> <span class="text"
												style="position: absolute; top: 50%; right: 0%; transform: translate(0, -50%);">
												日時：<c:out value="${reply.update_time}" />
											</span>
										</div>
										<p class="card-text">
											<c:forEach var="replyStr" items="${reply.splitedText}">
												<c:out value="${replyStr}" />
												<br>
											</c:forEach>
										</p>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</body>
</html>