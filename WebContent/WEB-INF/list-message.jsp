<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${userName}" />さんと連絡</title>
<script type="text/javascript">
	<!--
		var old_form_id = 0;
		function submitConfirm() {
			return window.confirm("この内容で送信してよろしいでしょうか？");
		}
		function resetConfirm() {
			return window.confirm("内容をクリアしますがよろしいですか？");
		}
		function selectUserRequest(id) {

			if (old_form_id == id) return;

			const hidden_reqeust_id = document.getElementsByName('request_id')[0];
			hidden_reqeust_id.setAttribute("value", id);

			var idkey = 'replyform_' + old_form_id;
			const oldform = document.getElementById(idkey);

			idkey = 'replyform_' + id;
			const target = document.getElementById(idkey);

			//target.appendChild(oldform);
			//oldform.removeChild(oldform.firstChild);
			target.innerHTML = oldform.innerHTML;
			oldform.innerHTML = '';
			old_form_id = id;

		}
	// -->
	</script>
<%@ include file="header.jsp"%>
<link rel="stylesheet" type="text/css" href="./css/user_request.css">
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<div class="container" style="margin-bottom: 50px">

		<div class="row">
			<div class="col-12">
				<p style="position: relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded"><c:out
							value="${userName}" />さんと連絡</p>
							 <span class="text-danger"
						style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
						<c:out value="${ errorMessage }" /> <%
 	session.removeAttribute("errorMessage");
 %>
					</span>
					 <span class="text-danger"
						style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
						<c:out value="${ message }" /> <%
 	session.removeAttribute("message");
 %>
					</span>
				</p>
				<div class="row">
					<div class="col" id="replyform_0">
						<form action="add-reply" method="post" class="form-group">
							<!-- 入力フォーム -->
							<input type="hidden" name="request_id"
								value="${requestList.get(0).id}" /> <input type="hidden"
								name="user_id" value="${userId}" /> <label
								class="col-form-label"><c:out value="${userName}" />さんへ返信</label>
							<textarea name="text" class="form-control" maxlength="65535"
								autofocus="autofocus" rows="4"><c:out
									value="${reply_text}" /></textarea>
									<% session.removeAttribute("reply_text"); %>
							<button type="submit" class="btn btn-success float-right"
								style="margin-top: 5px" onclick="return submitConfirm()">送信</button>
						</form>
					</div>
				</div>
				<c:forEach items="${requestList}" var="req">
					<div class="row" style="margin-top: 10px">
						<div class="col-12">
							<!-- 利用者からの連絡 -->
							<div class="card float-left user_request" style="width: 95%"
								onclick="selectUserRequest(${req.id})">
								<c:if test="${not req.repliedFlg}">
									<div style="border: 2px solid rgba(255, 0, 0, 0.3)">
								</c:if>
								<div class="card-body"
									style="padding-top: 5px; padding-bottom: 5px;">
									<div class="card-title"
										style="position: relative; margin-bottom: 5px;">
										<c:if test="${not req.repliedFlg}">
											<span class="badge badge-danger"
												style="position: absolute; top: 0%; left: 0%; transform: translate(-100%, -50%);">未返信</span>
										</c:if>
										<span class="text-info" style="">カテゴリ：<c:out
												value="${req.category_name}" /></span> <br> <span
											class="text-info" style="">コース：<c:out
												value="${req.course_name}" /></span> <span class="text"
											style="position: absolute; top: 50%; right: 0%; transform: translate(0, -50%);">
											日時：<c:out value="${req.update_time}" />
										</span>
									</div>
									<p class="card-text">
										<c:forEach var="reqStr" items="${req.splitedText}">
											<c:out value="${reqStr}" />
											<br>
										</c:forEach>
									</p>
								</div>
								<c:if test="${not req.repliedFlg}">
							</div>
							</c:if>
						</div>
					</div>
					<div class="col-12" id="replyform_${req.id}"></div>
					<c:forEach items="${replyMap.get(req.id)}" var="reply">
						<div class="col-12">
							<!-- 利用者への返信 -->
							<div class="card float-right"
								style="width: 95%; background: linear-gradient(-45deg, rgba(161, 246, 246, 0.1), rgba(161, 161, 246, 0.1));">
								<div class="card-body"
									style="padding-top: 5px; padding-bottom: 5px;">
									<div class="card-title"
										style="position: relative; margin-bottom: 5px;">
										<span class="text-info" style="">返信者：<c:out
												value="${reply.staff_name}" /></span> <span class="text"
											style="position: absolute; top: 50%; right: 0%; transform: translate(0, -50%);">
											日時：<c:out value="${reply.update_time}" />
										</span>
									</div>
									<p class="card-text">
										<c:forEach var="replyStr" items="${reply.splitedText}">
												<c:out value="${replyStr}" />
												<br>
										</c:forEach>
									</p>
								</div>
							</div>
						</div>
					</c:forEach>
			</div>
			</c:forEach>
		</div>
	</div>
	</div>
</body>
</html>