<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>利用者一覧</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="customer-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">利用者一覧</p>
				<table class="table table-striped" style="margin-top:35px">
					<thead>
						<tr>
							<th scope="col">社員番号</th>
							<th scope="col">氏名</th>
							<th scope="col">状態</th>
							<th scope="col">詳細</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${user}" var="dto">
							<tr>
								<td><c:out value="${ dto.getEmpno() }" /></td>
								<td><c:out value="${ dto.name }" /></td>
								<c:choose>
										<c:when test="${dto.state eq 0 }">
											<td><span class="badge badge-success">アクティブ</span></td>
										</c:when>
										<c:otherwise>
											<td><span class="badge badge-dark">休止</span></td>
										</c:otherwise>
									</c:choose>
								<td>
									<form action="view-user" method="post">
										<input type="hidden" name="id" value="${ dto.id }">
										<button type="submit" class="btn btn-primary btn-sm">詳細</button>
									</form>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>