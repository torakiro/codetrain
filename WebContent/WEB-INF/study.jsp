<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${curriculum.name}" /></title>
<%@ include file="/WEB-INF/header.jsp"%>
</head>
<body>
	<%@ include file="/WEB-INF/user-navbar.jsp"%>
	<div class="container">
		<p class="row h4 mt-3 p-3 bg-light text-info rounded"><c:out value="${course.name}" /></p>
		<nav aria-label="パンくずリスト">
			<ol class="breadcrumb bg-white pl-0 m-0">
				<li class="breadcrumb-item"><a href="user_index.jsp">CodeTrain</a></li>
				<li class="breadcrumb-item"><a href="list-course">コース一覧</a></li>
				<li class="breadcrumb-item"><a
					href="view-course?courseId=${ course.id }">${course.name}</a></li>
				<li class="breadcrumb-item active" aria-current="page"><c:out
						value="${curriculum.name}" /></li>
			</ol>
		</nav>
		<div class="row form-group">
			<c:if test="${!curriculum.examFlg}">
				<div class="col-lg-6">
					<div class="h4">
						<c:out value="${text.title }" default="NO TEXT" />
					</div>
					<div class="form-control bg-light mx-auto"
						style="height: 423px; width: 510px; overflow: auto;word-wrap:break-word;">
						<c:out value="${text.text }" />
					</div>
					<div class="row mx-auto pt-2">
						<div class="col-lg-4">
							<c:if test="${not empty previousCurriculum}">
								<form action="study" method="get" class="form-inline">
									<input type="hidden" name="curriculumId"
										value="${previousCurriculum.id}" />
									<button type="submit" class="btn btn-outline-primary btn-block">前へ</button>
								</form>
							</c:if>
						</div>
						<div class="btn col-lg-4 d-flex align-self-center"
							style="cursor: auto">
							<a href="view-course?courseId=${ course.id }" class="mx-auto">コース詳細に戻る</a>
						</div>
						<div class="col-lg-4">
							<c:if test="${not empty nextCurriculum}">
								<form action="study" method="get" class="form-inline">
									<input type="hidden" name="curriculumId"
										value="${nextCurriculum.id}" />
									<button type="submit" class="btn btn-outline-primary btn-block">次へ</button>
								</form>
							</c:if>
						</div>
					</div>
				</div>
			</c:if>
			<c:choose>
				<c:when test="${!curriculum.examFlg}">
					<div class="col-lg-6">
					<div class="h4">エクササイズ</div>
				</c:when>
				<c:otherwise>
					<div class="col-lg-12">
					<div class="h4">テスト</div>
				</c:otherwise>
			</c:choose>
				<form action="answer" method="post" class="form-inline col-lg-12">
					<div class="row form-control bg-light col-lg-12 mx-auto"
						style="height: 423px; width: 510px; overflow: auto">
						<c:forEach var="quest" items="${questionList}">
							<div class="col-lg-12">
								<div class="row">
									<div class="h1">
										<c:out value="${quest.name}" />
									</div>
									<c:forEach var="answer" items="${answerList}">
										<c:if test="${answer.question_id eq quest.id}">
											<c:if test="${answer.correct == true}">
												<div class="alert alert-success">正解</div>
											</c:if>
											<c:if test="${answer.correct == false}">
												<div class="alert alert-danger">不正解</div>
											</c:if>
										</c:if>
									</c:forEach>
									 <%session.removeAttribute("answers"); %>
								</div>
								<div class="h5">
									<c:out value="${quest.question}" />
								</div>
								<div class="row">
									<c:if test="${not empty quest.choice1}">
										<div class="form-check col-lg-3">
											<input class="form-check-input" type="radio"
												name="radio${quest.id}" id="radio${quest.id}1" value="1"
												<c:forEach var="answer" items="${answerList}">
													<c:if test="${answer.question_id eq quest.id}">
														<c:if test="${answer.answer == 1}">checked="checked"</c:if>
														<c:if test="${not empty answer.answer and curriculum.examFlg}">disabled="disabled"</c:if>
													</c:if>
												</c:forEach>>
											<label class="form-check-label" for="radio${quest.id}1"><c:out
													value="${quest.choice1}" /></label>
										</div>
									</c:if>
									<c:if test="${not empty quest.choice2}">
										<div class="form-check col-lg-3">
											<input class="form-check-input" type="radio"
												name="radio${quest.id}" id="radio${quest.id}2" value="2"
												<c:forEach var="answer" items="${answerList}">
													<c:if test="${answer.question_id eq quest.id}">
														<c:if test="${answer.answer == 2}">checked="checked"</c:if>
														<c:if test="${not empty answer.answer and curriculum.examFlg}">disabled="disabled"</c:if>
													</c:if>
												</c:forEach>>
											<label class="form-check-label" for="radio${quest.id}2"><c:out
													value="${quest.choice2}" /></label>
										</div>
									</c:if>
									<c:if test="${not empty quest.choice3}">
										<div class="form-check col-lg-3">
											<input class="form-check-input" type="radio"
												name="radio${quest.id}" id="radio${quest.id}3" value="3"
												<c:forEach var="answer" items="${answerList}">
													<c:if test="${answer.question_id eq quest.id}">
														<c:if test="${answer.answer == 3}">checked="checked"</c:if>
														<c:if test="${not empty answer.answer and curriculum.examFlg}">disabled="disabled"</c:if>
													</c:if>
												</c:forEach>>
											<label class="form-check-label" for="radio${quest.id}3"><c:out
													value="${quest.choice3}" /></label>
										</div>
									</c:if>
									<c:if test="${not empty quest.choice4}">
										<div class="form-check col-lg-3">
											<input class="form-check-input" type="radio"
												name="radio${quest.id}" id="radio${quest.id}4" value="4"
												<c:forEach var="answer" items="${answerList}">
													<c:if test="${answer.question_id eq quest.id}">
														<c:if test="${answer.answer == 4}">checked="checked"</c:if>
														<c:if test="${not empty answer.answer and curriculum.examFlg}">disabled="disabled"</c:if>
													</c:if>
												</c:forEach>>
											<label class="form-check-label" for="radio${quest.id}4"><c:out
													value="${quest.choice4}" /></label>
										</div>
									</c:if>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="row pt-2 w-100 mx-auto">
						<input type="hidden" name="curriculum" value="${curriculum.id}">
						<input type="hidden" name="uri" value="${ requestScope.uri }">
						<c:choose>
							<c:when test = "${questionCount ne 0}" >
								<button type="submit" class="btn btn-primary btn-block ">送信</button>
							</c:when>
							<c:otherwise>
								<button class="btn btn-primary btn-block " disabled>送信</button>
							</c:otherwise>
						</c:choose>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>