<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>法人アカウント作成</title>
<script type="text/javascript">
<!--
function submitConfirm() {
  return window.confirm("登録後は修正できませんが、本当によろしいですか？");
}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<form id="createCorpForm" action="add-customer" method="post"></form>

	<div class="container">
		<div class="row" style="margin-bottom:10px">
			<div class="col-12">
				<p style="position:relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">法人アカウント作成</p>
					<span style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<% session.removeAttribute("errorMessage"); %>
					<% session.removeAttribute("message"); %>
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-6">
				<div class="card bg-light" style="width:100%">
					<div class="card-body">
						<h4 class="card-title text-info">
							法人
						</h4>
						<p class="card-text">
							<div class="form-group">
								<label class="col-form-label" for="corp_name">法人名</label>
								<input type="text" id="corpName" name="corpName" placeholder="法人名" autofocus="autofocus" class="form-control" form="createCorpForm" maxlength="16"/>
							</div>
							<div class="form-group">
								<label class="col-form-label" for="corpDomain">ドメイン</label>
								<input type="text" id="corpDomain" name="corpDomain" placeholder="例)kelonos.tokyo.jp" class="form-control" form="createCorpForm" maxlength="512"/>
							</div>
							<div class="form-group">
								<label class="col-form-label" for="billingAddress">請求先住所</label>
								<input type="text" id="billingAddress" name="billingAddress" placeholder="例)東京都中央区" class="form-control" form="createCorpForm" maxlength="512"/>
							</div>
						</p>
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="card bg-light" style="width:100%">
					<div class="card-body">
						<h4 class="card-title text-info">
							担当者
						</h4>
						<p class="card-text">
							<div class="form-group">
								<label class="col-form-label" for="corp_name">氏名</label>
								<input type="text" id="personName" name="personName" placeholder="氏名" class="form-control" form="createCorpForm" maxlength="16"/>
							</div>
							<div class="form-group">
								<label class="col-form-label" for="corp_name">email</label>
								<input type="email" id="personEmail" name="personEmail" placeholder="例)xxx@kelonos.tokyo.jp" class="form-control" form="createCorpForm" maxlength="512"/>
							</div>
							<div class="form-group">
								<label class="col-form-label" for="corp_name">役職</label>
								<input type="text" id="personPosition" name="personPosition" placeholder="例)部長" class="form-control" form="createCorpForm" maxlength="255"/>
							</div>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group" style="text-align:center">
			<br>
			<button type="submit" form="createCorpForm" class="btn btn-primary" form="createCorpForm" onclick="return submitConfirm()">登録</button>
			<br><br><br>
			<button type="reset" form="createCorpForm" class="btn btn-danger">リセット</button>
		</div>
	</div>
</body>
</html>