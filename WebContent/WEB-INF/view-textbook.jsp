<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><c:out value="テキスト詳細" /></title>
	<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="/WEB-INF/operation-navbar.jsp"%>
	<div class="container">
		<div class="row" style="margin-bottom:10px">
			<div class="col-12">
				<p style="position:relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">テキスト詳細</p>
				</p>
			</div>
		</div>
		<div class="row" style="margin-bottom:20px;">
			<div class="card mx-auto" style="width:100%">
				<div class="card-body">
					<h4 class="card-title text-info">
						<c:out value="${textbook.title}"/>
					</h4>
					<p class="card-text">
						<c:out value="${textbook.text}"/>
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="mx-auto" style="width:auto">
				<a href="list-text"><button class="btn btn-info btn-sm">テキスト一覧へ戻る</button></a>
			</div>
		</div>
	</div>
</body>
</html>