<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>運営者追加</title>
<script type="text/javascript">
<!--
function submitConfirm() {
  return window.confirm("送信します。本当によろしいですか？");
}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<form id="createOperatorForm" action="add-operator" method="post"></form>

	<div class="container">
		<div class="row" style="margin-bottom:10px">
			<div class="col-12">
				<p style="position:relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">運営者追加</p>
					<span style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<% session.removeAttribute("errorMessage"); %>
					<% session.removeAttribute("message"); %>
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-4"></div>
			<div class="card bg-light col-4" style="width:100%">
				<div class="card-body">
					<h4 class="card-title text-info">
					</h4>
					<p class="card-text">
						<div class="form-group">
							<label class="col-form-label" for="name">氏名</label>
							<input type="text" id="name" name="name" placeholder="氏名" autofocus="autofocus" class="form-control" form="createOperatorForm" maxlength="16"/>
						</div>
						<div class="form-group">
							<label class="col-form-label" for="authority_id">権限グループ</label>
							<select name="authority_id" id="authority_id" class="form-control" form="createOperatorForm">
								<c:forEach items="${authorityGroupList}" var="authDto">
									<option value="${authDto.id}"><c:out value="${authDto.name}" /></option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label class="col-form-label" for="login_id">ログインID</label>
							<input type="text" id="login_id" name="login_id" placeholder="example@kelonos.jeepii" class="form-control" form="createOperatorForm" maxlength="255"/>
						</div>
						<div class="form-group">
							<label class="col-form-label" for="password">パスワード</label>
							<input type="password" id="password" name="password" placeholder="パスワード" class="form-control" form="createOperatorForm" maxlength="16"/>
						</div>
						<div class="form-group">
							<label class="col-form-label" for="password_confirm">パスワード（確認）</label>
							<input type="password" id="password_confirm" name="password_confirm" placeholder="同じパスワード" class="form-control" form="createOperatorForm" maxlength="16"/>
						</div>
					</p>
				</div>
			</div>
		</div>
		<div class="form-group" style="text-align:center">
			<br>
			<button type="submit" form="createOperatorForm" class="btn btn-primary" onclick="return submitConfirm()">登録</button>
			<br><br><br>
			<button type="reset" form="createOperatorForm" class="btn btn-danger">リセット</button>
		</div>
	</div>
</body>
</html>