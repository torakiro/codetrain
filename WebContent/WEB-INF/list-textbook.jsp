<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>テキスト一覧</title>
	<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<div class="container">
		<c:choose>
			<c:when test="${operator.authority_group.canCreate_text()}">
				<div class="row">
			</c:when>
			<c:otherwise>
				<div class="row" style="margin-bottom:10px">
			</c:otherwise>
		</c:choose>
			<div class="col-12">
				<p style="position: relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">テキスト一覧</p>
					<c:if test="${operator.authority_group.canCreate_text()}">
						<form id="form-nav" action="register-textbook" method="post" class="form-inline">
							<button type="submit" class="btn btn-info">新規作成</button>
						</form>
					</c:if>
					<span style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<%
						session.removeAttribute("errorMessage");
					%>
					<%
						session.removeAttribute("message");
					%>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">カテゴリ</th>
							<th scope="col">コース</th>
							<th scope="col">タイトル</th>
							<th scope="col">詳細</th>
							<c:if test="${operator.authority_group.canEdit_text()}">
								<th scope="col">編集</th>
							</c:if>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${textList}" var="dto">
							<tr>
								<td><c:out value="${ dto.getCategory_name() }" /></td>
								<td><c:out value="${ dto.getCourse_name()}" /></td>
								<td><c:out value="${ dto.title}" /></td>

								<td>
									<form action="view-textbook" method="get">
										<input type="hidden" name="textId" value="${ dto.id }">
										<button type="submit" class="btn btn-primary btn-sm">詳細</button>
									</form>
								</td>
								<c:if test="${operator.authority_group.canEdit_text()}">
									<td>
										<form action="update-textbook" method="post">
											<input type="hidden" name="id" value="${ dto.id }">
											<input type="hidden" name="curriculum_id" value="${ dto.curriculum_id }">
											<button type="submit" class="btn btn-info btn-sm">編集</button>
										</form>
									</td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>