<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" type="text/css" href="./css/navbar.css">
<nav class="navbar navbar-expand-lg navbar-light operator">
	<a class="navbar-brand"><img src="img/CodeTrain.png" height="50"
		width="72" /></a>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<c:if test="${operator.authority_group.canShow_text()}">
				<li class="nav-item">
					<form action="list-text" method="post">
						<button type="submit" class="btn btn-outline-primary btn-sm ml-2">テキスト一覧</button>
					</form>
				</li>
			</c:if>
			<c:if test="${operator.authority_group.canShow_corplist()}">
				<li class="nav-item">
					<form action="list-customer" method="post">
						<button type="submit" class="btn btn-outline-primary btn-sm ml-2">法人一覧</button>
					</form>
				</li>
			</c:if>
			<c:if test="${operator.authority_group.canShow_message_userlist()}">
				<li class="nav-item">
					<form action="list-message-sender" method="post">
						<button type="submit" class="btn btn-outline-primary btn-sm ml-2">利用者との連絡</button>
					</form>
				</li>
			</c:if>
			<c:if test="${operator.authority_group.canShow_operatorlist()}">
				<li class="nav-item">
					<form action="list-operator" method="post">
						<button type="submit" class="btn btn-outline-primary btn-sm">運営担当者一覧</button>
					</form>
				</li>
			</c:if>
		</ul>
		<div class="navbar-text small mr-3 text-secondary">
			<c:out
				value="${ not empty operator.name ? operator.name += 'さん、こんにちは' : ''  } " />
		</div>
		<form action="operator-change-password" method="get"
			class="form-inline">
			<input type="hidden" name="id" value="${ operator.id }">
			<button type="submit" class="btn btn-outline-secondary btn-sm">パスワード変更</button>
		</form>
		<form action="logout" method="post" class="form-inline">
			<button type="submit" class="btn btn-outline-danger btn-sm ml-2">ログアウト</button>
		</form>
	</div>
</nav>
