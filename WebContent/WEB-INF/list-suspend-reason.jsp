<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.temporal.TemporalAdjusters" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>休止理由一覧</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<div class="container">
		<div class="row" style="margin-bottom:10px">
			<div class="col-12">
				<p style="position:relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded"><c:out value="${ corp_name }"/> の休止理由一覧</p>
					<span style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<% session.removeAttribute("errorMessage"); %>
					<% session.removeAttribute("message"); %>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<table class="table ">
					<thead>
						<tr>
							<th scope="col" rowspan="2">ID</th>
							<th scope="col" rowspan="2">社員番号</th>
							<th scope="col" rowspan="2">氏名</th>
							<th scope="col" rowspan="2">活性状態</th>
							<th scope="col" colspan="2">休止移行代金</th>
						</tr>
						<tr>
							<th scope="col">請求対象</th>
							<th scope="col">請求取消</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="dto">
							<tr>
								<td><c:out value="${ dto.user_id }" /></td>
								<td><c:out value="${ dto.empno }" /></td>
								<td><c:out value="${ dto.user_name }" /></td>
								<td>
									<c:choose>
										<c:when test="${dto.state eq 0 }">
											<span class="badge badge-success">アクティブ</span>
										</c:when>
										<c:otherwise>
											<span class="badge badge-dark">休止</span>
										</c:otherwise>
									</c:choose>
								</td>
								<td>
								<c:choose>
										<c:when test="${ dto.getSuspend_month().toLocalDate().isAfter(LocalDateTime.now().with(TemporalAdjusters.firstDayOfMonth()).toLocalDate()) }">
											<span class="badge badge-success">休止移行代金<br>請求対象</span>
										</c:when>
										<c:otherwise>
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<form action="remove-suspend-billing" method="post">
										<input type="hidden" name="id" value="${ dto.user_id }">
										<input type="hidden" name="user_name" value="${dto.user_name }">
										<input type="hidden" name="corp_id" value="${corp_id}">
										<button type="submit" class="btn btn-info btn-sm">実行</button>
									</form>
								</td>
							</tr>
							<tr>
								<td></td>
								<th style="text-align:right">休止理由：</th>
								<td colspan="3"><c:out value="${ dto.reason }" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>