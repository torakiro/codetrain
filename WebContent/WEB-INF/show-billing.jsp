<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.time.format.DateTimeFormatter" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>請求一覧</title>
<%@ include file="header.jsp"%>
<link rel="stylesheet" type="text/css" href="./css/icon.css">
</head>
<body>
	<%@ include file="customer-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">請求一覧</p>
				<table class="table table-striped table-bordered" style="margin-top: 35px">
					<thead>
						<tr>
							<th scope="col" rowspan="2">利用月</th>
							<th scope="col" rowspan="2">請求額</th>
							<th scope="col" colspan="3">内訳</th>
							<th scope="col" rowspan="2">払い込み期限</th>
							<th scope="col" rowspan="2">未払い</th>
						</tr>
						<tr>
							<th scope="col">アクティブユーザ</th>
							<th scope="col">休止ユーザ</th>
							<th scope="col">新規休止ユーザ</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${billing}" var="dto" varStatus="loopstat">
							<c:set var="month" value="${ DateTimeFormatter.ofPattern(\"YYYY年M月\").format(dto.getBilling_month().toLocalDate()) }" />
							<c:choose>
								<c:when test="${ dto.billedFlg eq false }">
									<tr class="table-warning">
								</c:when>
								<c:otherwise>
									<tr>
								</c:otherwise>
							</c:choose>
								<td><c:out value="${ month }" /></td>
								<td><c:out value="${ dto.price }" />円</td>
								<td>
									<c:out value="${ dto.cnt_user_active }" />人
									<br>
									<c:out value="${ dto.price_user_active }" />円
								</td>
								<td>
									<c:out value="${ dto.cnt_user_suspended }" />人
									<br>
									<c:out value="${ dto.price_user_suspended }" />円
								</td>
								<td>
									<c:out value="${ dto.cnt_user_newly_suspended }" />人
									<br>
									<c:out value="${ dto.price_user_newly_suspended }" />円
								</td>
								<td><c:out value="${ dto.getBilling_month().toLocalDate().plusMonths(1).withDayOfMonth(20) }" /></td>
								<td>
									<c:choose>
										<c:when test="${dto.billedFlg eq false }">
												<img src="./img/alert01-002-48.png" class="warning">
										</c:when>
										<c:otherwise>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</body>
</html>