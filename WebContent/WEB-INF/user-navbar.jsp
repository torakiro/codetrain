<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" type="text/css" href="./css/navbar.css">
<nav class="navbar navbar-expand-lg navbar-light user">
	<a class="navbar-brand" href="list-course"><img
		src="img/CodeTrain.png" height="50" width="72" /></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<div class="navbar-text small text-danger ml-auto mr-2">
			<c:out value="${ navbarMessage } " />
			<%
				session.removeAttribute("navbarMessage");
			%>
		</div>
		<div class="navbar-text small text-secondary mr-2">
			<c:out
				value="${ not empty user.name ? user.name += 'さん、こんにちは' : ''} " />
		</div>
		<c:choose>
			<c:when test="${ empty user }">
				<form id="form-nav" action="user-login" method="post"
					class="form-inline my-2 my-lg-0">
					<input type="text" name="id"
						class="form-control form-control-sm ml-2" maxlength = "255" placeholder="ログインID">
					<input type="password" name="password"
						class="form-control form-control-sm ml-2" placeholder="パスワード" maxlength="16">
					<button type="submit" class="btn btn-outline-primary btn-sm ml-2">ログイン</button>
				</form>
			</c:when>
			<c:otherwise>
				<form action="user-contact" method="post" class="form-inline">
					<button type="submit" class="btn btn-outline-info btn-sm ml-2">ご意見ご要望</button>
				</form>
				<form action="user-change-password" method="post"
					class="form-inline">
					<button type="submit" class="btn btn-outline-secondary btn-sm ml-2">パスワード変更</button>
				</form>
				<form action="logout" method="post" class="form-inline">
					<button type="submit" class="btn btn-outline-danger btn-sm ml-2">ログアウト</button>
				</form>
			</c:otherwise>
		</c:choose>
	</div>
</nav>
