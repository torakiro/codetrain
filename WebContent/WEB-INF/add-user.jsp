<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>利用者登録</title>
<script type="text/javascript">
<!--
	var empNo_temp;
	function submitConfirm() {
		return window.confirm("登録後は修正できませんが、本当によろしいですか？");
	}
	function connecttext(textid, ischecked) {
		const form = document.getElementById(textid);
		if (ischecked == true) {
			// チェックが入っていたら有効化
			form.disabled = false;
			//form.setAttribute("value", empNo_temp);
		} else {
			// チェックが入っていなかったら無効化
			form.disabled = true;
			//emoNo_temp = form.getAttribute("value");
			form.setAttribute("value", "");

		}
	}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="customer-navbar.jsp"%>
	<form id="createUserForm" action="add-user" method="post"></form>

	<div class="container">
		<div class="row" style="margin-bottom:10px">
			<div class="col-12">
				<p style="position:relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">利用者登録</p>
					<span style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<% session.removeAttribute("errorMessage"); %>
					<% session.removeAttribute("message"); %>
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-4"></div>
			<div class="card bg-light col-4" style="width:auto">
				<div class="card-body">
					<p class="card-text">
					<div class="form-group">
						<label class="col-form-label">ログインＩＤ</label>
						<div class="row">
							<input type="text" id="login_id" name="login_id"
								placeholder="ログインＩＤ" class="form-control col-15"
								maxlength="255" form="createUserForm" />
						</div>
					</div>
					<div class="form-group">
						<label class="checkbox-inline"> <input type="checkbox"
							value="" onclick="connecttext('empNo',this.checked);">社員番号を設定する
						</label>
						<br>
						<label class="col-form-label">社員番号</label>
						<div class="row">
							<input type="number" id="empNo" name="empNo" value="1" min="1"
								class="form-control col-15" form="createUserForm" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-form-label">初期パスワード</label>
						<div class="row">
							<input type="password" id="password" name="password"
								placeholder="初期パスワード" class="form-control col-15"
								maxlength="16" form="createUserForm" />
						</div>
					</div>
					</p>
				</div>
			</div>
		</div>
		<div class="form-group" style="text-align: center">
			<br>
			<button type="submit" form="createUserForm" class="btn btn-primary"
				form="createUserForm" onclick="return submitConfirm()">登録</button>
			<br> <br> <br>
			<button type="reset" form="createUserForm" class="btn btn-danger">リセット</button>
		</div>
	</div>
	<script type="text/javascript">
	<!--
		connecttext('empNo', false);
	// -->
	</script>
</body>
</html>