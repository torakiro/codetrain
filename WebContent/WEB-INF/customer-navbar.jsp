<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" type="text/css" href="./css/navbar.css">
<nav class="navbar navbar-expand-lg navbar-light customer">
	<a class="navbar-brand" href="list-user"><img
		src="img/CodeTrain.png" height="50" width="72" /></a>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<form action="list-user" method="post" class="form-inline">
					<button type="submit" class="btn btn-outline-primary btn-sm ml-2">利用者一覧</button>
				</form>
			</li>
			<li>
				<form action="manage-user" method="post">
					<button class="btn btn-outline-primary btn-sm ml-2">利用者登録/休止</button>
				</form>
			</li>
			<li>
				<form action="show-billing" method="post">
					<button class="btn btn-outline-primary btn-sm ml-2">請求確認</button>
				</form>
			</li>
		</ul>
		<div class="navbar-text small mr-3 text-secondary">
			<c:out
				value="${ not empty customer.name ? customer.name += 'さん、こんにちは' : '' }" />
		</div>
		<form action="customer-change-password" method="post"
			class="form-inline">
			<input type="hidden" name="id" value="${ customer.id }"> <input
				type="hidden" name="login-now" value="1">
			<button type="submit" class="btn btn-outline-secondary btn-sm">パスワード変更</button>
		</form>
		<form action="logout" method="post" class="form-inline">
			<button type="submit" class="btn btn-outline-danger btn-sm ml-2">ログアウト</button>
		</form>

	</div>
</nav>
