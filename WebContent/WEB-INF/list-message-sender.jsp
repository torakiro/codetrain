<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="連絡一覧" /></title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<div class="container">
		<p class="h4 mt-3 p-3 bg-light text-info rounded">利用者からのメッセージ</p>
		<div class="row">
			<div class="col-12" style="margin-top: 0">
				<p style="position: relative">
					<span style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
					<span class="text-success">
					  <c:out value="${ message }" />
					</span>
					</span>
					<%
						session.removeAttribute("errorMessage");
					%>
					<%
						session.removeAttribute("message");
					%>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<table class="table table-striped" style="margin-top:10px">
					<thead>
						<tr>
							<th>利用者ID</th>
							<th>未返信</th>
							<th>お客様氏名</th>
							<th>最終更新日時</th>
							<th>返信</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="sender" items="${senderList}">
							<c:choose>
								<c:when test="${not sender.repliedFlg}">
									<tr style="font-weight: bold">
								</c:when>
								<c:otherwise>
									<tr>
								</c:otherwise>
							</c:choose>
							<td><c:out value="${sender.user_id}" /></td>
							<td><c:if test="${not sender.repliedFlg}"><span class="badge badge-danger">未返信</span></c:if></td>
							<td><c:out value="${sender.getUser_name()}" /></td>
							<td><c:out value="${sender.update_time}" /></td>
							<td><a href="list-message?id=${sender.user_id}"><button
										class="btn btn-primary btn-sm">返信</button></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>