<%@page import="java.util.List"%>
<%@page import="jp.kelonos.codetrain.dto.AuthorityGroupDto"%>
<%@page import="jp.kelonos.codetrain.dto.StaffDto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>運営担当者一覧</title>
	<%@ include file="header.jsp"%>
	<script type="text/javascript">

	var rowData;
	const user_id = <% out.print(((StaffDto)session.getAttribute("operator")).getId()); %>;

	function recordRowData(rows) {
		if (typeof rowData === 'undefined') {
			rowData = [];
			for (var i = 0; i < rows.length; i++) {
				var data = [];
				var childs = rows[i].childNodes;
				data.push(childs[3].innerHTML);
				data.push(childs[5].innerHTML);
				data.push(childs[7].innerHTML);
				data.push(childs[9].innerHTML);
				rowData.push(data);
			}
		}
	}

	function startEdit(id) {

		const target_formcol = document.getElementsByClassName('buttoncol_1');
		const target_namecol = document.getElementsByClassName('buttoncol_2');
		for (var i = 0; i< target_formcol.length; i++) {
			target_formcol[i].innerHTML = '確定';
		}
		for (var i = 0; i < target_namecol.length; i++) {
			target_namecol[i].innerHTML = '取消';
		}
		document.getElementById('hidden_staff_id').setAttribute('value', '' + id);

		const rows = document.getElementsByClassName("disprows");
		const target = document.getElementById("disprow_" + id);

		recordRowData(rows);
		for (var i = 0; i < rows.length; i++) {
			var childs = rows[i].childNodes;
			childs[7].innerHTML = '';
			childs[9].innerHTML = '';
		}

		const children = target.childNodes;
		children[3].innerHTML = '<input type="text" name="name" form="changeOperatorAuthorityForm" class="form-control" placeholder="氏名" autofocus maxlength="16">';

		children[5].innerHTML = '<%
			StringBuffer sb = new StringBuffer();
			sb.append("<select class=\"form-control\" name=\"authorityGroup_id\" form=\"changeOperatorAuthorityForm\">");
			Object list = request.getAttribute("authList");
			for (AuthorityGroupDto dto : (List<AuthorityGroupDto>)list) {
				sb.append("<option value=\"");
				sb.append(dto.getId());
				sb.append("\">");
				sb.append(dto.getName());
				sb.append("</option>");
			}
			sb.append("</select>");
			out.print(sb);
		%>';

		children[7].innerHTML = '<td><button type="submit" class="btn btn-success btn-sm" form="changeOperatorAuthorityForm">確定</button></td>';
		children[9].innerHTML = '<td><button class="btn btn-danger btn-sm" onClick="abortEdit(' + id + ')">取消</button></td>';

	}

	function abortEdit(id) {

		const target_formcol = document.getElementsByClassName('buttoncol_1');
		const target_namecol = document.getElementsByClassName('buttoncol_2');
		for (var i = 0; i< target_formcol.length; i++) {
			target_formcol[i].innerHTML = '編集';
		}
		for (var i = 0; i < target_namecol.length; i++) {
			target_namecol[i].innerHTML = '';
		}

		const rows = document.getElementsByClassName("disprows");
		const target = document.getElementById("disprow_" + id);

		recordRowData(rows);
		for (var i = 0; i < rows.length; i++) {
			var childs = rows[i].childNodes;
			childs[3].innerHTML = rowData[i][0];
			childs[5].innerHTML = rowData[i][1];
			childs[7].innerHTML = rowData[i][2];
			childs[9].innerHTML = rowData[i][3];
		}

		const children = target.childNodes;

		if (id == user_id) {
			children[7].innerHTML = '<td><button name="button_edit" class="btn btn-primary btn-sm" disabled="disabled">編集</button></td>';
		}
		else {
			children[7].innerHTML = '<td><button name="button_edit" class="btn btn-primary btn-sm" onClick="startEdit(' + id + ')">編集</button></td>';
		}
		children[9].innerHTML = '<td></td>';

	}

	</script>
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<form action="operator-change-authority" method="post" id="changeOperatorAuthorityForm" ></form>
	<input type="hidden" id="hidden_staff_id" name="staffId" value="" form="changeOperatorAuthorityForm" />

	<div class="container">
		<div class="row">
			<div class="col-12">
				<p style="position: relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">運営担当者一覧</p>
					<c:if test="${operator.authority_group.canCreate_operator()}">
						<form id="form-nav" action="register-operator" method="post" class="form-inline">
							<button type="submit" class="btn btn-info">新規作成</button>
						</form>
					</c:if>
					<span style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<%
						session.removeAttribute("errorMessage");
					%>
					<%
						session.removeAttribute("message");
					%>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>氏名</th>
							<th>権限グループ</th>
							<th class="buttoncol_1">編集</th>
							<th class="buttoncol_2"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="dto" items="${staffList}">
							<tr id="disprow_${dto.id}" class="disprows">
								<td><c:out value="${dto.id}" /></td>
								<td><c:out value="${dto.name}" /></td>
								<td><c:out value="${dto.getAuthority_group().name}" /></td>
								<td>
									<c:choose>
										<c:when test="${dto.id eq sessionScope.operator.id}">
											<button name="button_edit" class="btn btn-primary btn-sm" disabled="disabled">編集</button>
										</c:when>
										<c:otherwise>
											<button name="button_edit" class="btn btn-primary btn-sm" onClick="startEdit(${dto.id})">編集</button>
										</c:otherwise>
									</c:choose>
								</td>
								<td></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>