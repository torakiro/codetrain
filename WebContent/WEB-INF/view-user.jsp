<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>利用者詳細</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="customer-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<p style="position: relative">

					<p class="h4 mt-3 p-3 bg-light text-info rounded"><c:out value="${user.name}"/>さんの学習状況</p>
					<span style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
						<span class="text-danger"> <c:out value="${ errorMessage }" /></span>
						<span class="text-success"> <c:out value="${ message }" /></span>
					</span>
					<%
						session.removeAttribute("errorMessage");
					%>
					<%
						session.removeAttribute("message");
					%>
				</p>
			</div>
		</div>
		<c:set var="old_categoryname" value="" />
		<c:set var="old_coursename" value="" />
		<table>
		<c:forEach items="${historyList}" var="history">
			<c:if test="${history.category_name ne old_categoryname}">
				</table>
				<p class="h4 rounded" style="margin-top:35px"><c:out value="${history.category_name}"/></p>
				<table class="table table-bordered">
				<c:set var="old_categoryname" value="${history.category_name }" />
			</c:if>
			<c:if test="${history.course_name ne old_coursename}">
				<thead>
					<tr class="table-warning">
						<th scope="col">コース名：<c:out value="${history.course_name}" /></th>
						<th scope="col">達成度：<c:out value="${ courseProgress.get(history.course_id).progress }" />%</th>
					</tr>
				</thead>
				<c:set var="old_coursename" value="${history.course_name }" />
			</c:if>
			<c:choose>
				<c:when test="${ history.progress >= 60 and history.correct_percent >= 60 }">
					<tr class="table-success">
				</c:when>
				<c:otherwise>
					<tr>
				</c:otherwise>
			</c:choose>
				<td>
					<c:out value="${ history.curriculum_name }" />
					<br>
				</td>
				<td>
					<div class="progress">
							<div class="progress-bar progress-bar-success progress-bar-striped progress-bar-animated" role="progressbar" style="width:${ history.progress }%">
							</div>
							<span style="position:absolute">
							進捗率：<c:out value="${ history.progress }" />%
						</span>
					</div>
					<c:choose>
						<c:when test="${history.correct_percent >= 0}">
							<div class="progress" style="margin-top:5px">
								<div class="progress-bar progress-bar-success progress-bar-striped progress-bar-animated" role="progressbar" style="width:${ history.correct_percent }%">
								</div>
								<span style="position:absolute">
									正解率：<c:out value="${ history.correct_percent }" />%
								</span>
							</div>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<c:set var="history_oldid" value="${history.id}" />
		</c:forEach>
		</table>
	</div>
</body>
</html>