<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>利用者休止</title>
<script type="text/javascript">
<!--
	var empNo_temp;
	function submitConfirm() {
		return window.confirm("休止状態に変更します。本当によろしいですか？");
	}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="customer-navbar.jsp"%>
	<form id="suspendUserForm" action="suspend-user" method="post"></form>

	<div class="container">

		<div class="row" style="margin-bottom:10px">
			<div class="col-12">
				<p style="position:relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">
						<c:out value="${user.name}"></c:out>
						さんの利用休止
					</p>
					<span style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<% session.removeAttribute("errorMessage"); %>
					<% session.removeAttribute("message"); %>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-4"></div>
			<div class="card bg-light col-4" style="width:auto">
				<div class="card-body">
					<h4 class="card-title text-info">
					</h4>
					<p class="card-text">
					<div class="form-group">
						<label class="col-form-label">休止理由</label>
						<div class="row">
							<input type="hidden" name="id" value="${user.getId() }" form="suspendUserForm" />
							<input type="hidden" name="name" value="${user.getName() }" form="suspendUserForm"/>
							<input type="text" id="reason" name="reason"
								placeholder="例)産休、育休、忌引など"
								autofocus="autofocus" class="form-control col-20"
								form="suspendUserForm" size="100" maxlength="512"/>
						</div>
					</div>
					</p>
				</div>
			</div>
		</div>
		<div class="form-group" style="text-align: center">
			<br>
			<button type="submit" form="suspendUserForm" class="btn btn-primary"
				form="suspendUserForm" onclick="return submitConfirm()">登録</button>
			<br> <br> <br>
			<button type="reset" form="suspendUserForm" class="btn btn-danger">リセット</button>
		</div>
	</div>
</body>
</html>