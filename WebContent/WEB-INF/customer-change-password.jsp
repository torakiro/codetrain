<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>パスワード変更</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<c:choose>
		<c:when test="${customer.passwordsetFlg== true }">
			<%@ include file="customer-navbar.jsp"%>
		</c:when>
		<c:otherwise>
			<br>
			<br>
		</c:otherwise>
	</c:choose>
	<div class="container">
		<div class="row" style="margin-bottom:10px">
			<div class="col-12">
				<p style="position:relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">パスワード変更</p>
					<span style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<% session.removeAttribute("errorMessage"); %>
					<% session.removeAttribute("message"); %>
				</p>
			</div>
		</div>
		<form id="form-nav" action="customer-change-password" method="post"
			class="form-inline form-control col-lg-8 mx-auto bg-light">
			<input type="hidden" name="id" value="${customer.id }" size="16">
			<div class="row col-lg-12 pt-3">
				<div class="col-4">現在のパスワード</div>
				<input type="password" name="password" placeholder="現在のパスワード"
					class="form-control form-control-sm col-8" maxlength="16">
			</div>
			<div class="row col-lg-12 pt-3">
				<div class="col-4">新しいパスワード</div>
				<input type="password" name="new-password" placeholder="新しいパスワード"
					class="form-control form-control-sm col-8" maxlength="16">
			</div>
			<div class="row col-lg-12 pt-3">
				<div class="col-4">新しいパスワード（確認)</div>
				<input type="password" name="password-check" placeholder="新しいパスワード（確認）"
					class="form-control form-control-sm col-8" maxlength="16">
			</div>

			<div class="row col-lg-12 pt-3">
				<div class="col-lg-4"></div>
				<button type="submit" class="btn btn-outline-primary ml-auto">更新</button>
			</div>
		</form>
	</div>
</body>
</html>