<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- モーダルの設定 -->
<div class="modal fade" id="exampleModalLong" tabindex="-1"
	role="dialog" aria-labelledby="exampleModalLongTitle"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title info" id="exampleModalLongTitle">カテゴリー作成</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="閉じる">
					<span aria-hidden="true">&times;</span>
				</button>
				<c:out value="${ message }" />
			</div>
			<form action="add-category" id="addCategoryForm" method="post"></form>
			<div class="modal-body">
				<p>閉じるには、ボタンをクリックするか、ESCキーを使用します。</p>
				<!-- 入力フォーム。enter も押すことができる -->
				<label class="col-form-label" for="corp_name">カテゴリ名</label> <input
					type="text" maxlength="30" title="文字数が多すぎます" id="categoryName"
					name="categoryName" placeholder="タイトルを入力してください" required
					class="form-control" form="addCategoryForm" />
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
				<button type="submit" class="btn btn-success" form="addCategoryForm">変更を保存</button>
			</div>
		</div>
	</div>
</div>


<!-- モーダルの設定 -->
<div class="modal fade" id="createCourseModalLong" tabindex="-1"
	role="dialog" aria-labelledby="createCourseModalLongTitle"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title info" id="createCourseModalLongTitle">コース作成</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="閉じる">
					<span aria-hidden="true">&times;</span>
				</button>
				<c:out value="${ message }" />
			</div>
			<form action="add-course" id="addCourseForm" method="post"></form>
			<div class="modal-body">
				<div class="form-group">
					<p>閉じるには、ボタンをクリックするか、ESCキーを使用します。</p>
					<label class="col-form-label" for="category">カテゴリ</label> <select
						name="category" class="form-contol" form="addCourseForm"
						style="width: 100%">
						<c:forEach items="${category}" var="dto">
							<option value="${dto.id}">${dto.name}</option>
						</c:forEach>
					</select>
					<!-- 入力フォーム。enter も押すことができる -->
					<label class="col-form-label" for="cource_title">コースタイトル</label> <input
						type="text" maxlength="30" title="文字数が多すぎます" id="course_title"
						name="course_title" placeholder="コースタイトルを入力してください" required
						class="form-control" form="addCourseForm" /> <label
						class="col-form-label" for="overview">概要</label> <input
						type="text" maxlength="1024" title="文字数が多すぎます" id="overview"
						name="overview" class="form-control"
						placeholder="コースタイトルを入力してください" form="addCourseForm" />
					<input type="checkbox" class="checkbox" name="freecourse" value="1" form="addCourseForm" /><label class="col-form-label" for="freecourse">フリーコース</label><br>
					<label class="col-form-label" for="rule">前提条件</label> <input
						type="text" maxlength="1024" title="文字数が多すぎます" id="rule"
						class="form-control" name="rule" placeholder="前提条件を入力してください"
						form="addCourseForm" /> <label class="col-form-label" for="goal">ゴール</label>
					<input type="text" maxlength="128" title="文字数が多すぎます" id="goal"
						class="form-control" name="goal" placeholder="ゴールを入力してください"
						form="addCourseForm" /> <label class="col-form-label" for="time">目安学習時間</label>
					<input type="number" max="65535" min="0"
						class="form-control" value="" title="数値が許容範囲を超えています" id="time"
						name="time" placeholder="ゴールを入力してください" form="addCourseForm" />
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
				<button type="submit" class="btn btn-success" form="addCourseForm">変更を保存</button>
			</div>
		</div>
	</div>
</div>

	<input type="hidden" name="id" value="${id}" form="addCourseForm" />
	<input type="hidden" name="id" value="${id}" form="addCategoryForm" />
	<input type="hidden" name="curriculum_id" value="${curriculum_id}" form="addCourseForm" />
	<input type="hidden" name="curriculum_id" value="${curriculum_id}" form="addCategoryForm" />