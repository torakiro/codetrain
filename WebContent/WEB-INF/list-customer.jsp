<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>法人一覧</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<div class="container">
		<c:choose>
			<c:when test="${operator.authority_group.canCreate_corp()}">
				<div class="row">
			</c:when>
			<c:otherwise>
				<div class="row" style="margin-bottom:10px">
			</c:otherwise>
		</c:choose>
			<div class="col-12">
				<p style="position: relative;">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">法人一覧</p>
					<c:if test="${operator.authority_group.canCreate_corp()}">
						<form id="form-nav" action="register-customer" method="post" class="form-inline">
							<button type="submit" class="btn btn-info">新規作成</button>
						</form>
					</c:if>
					<span style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<%
						session.removeAttribute("errorMessage");
					%>
					<%
						session.removeAttribute("message");
					%>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<table class="table table-striped" style="margin-top: 10px">
					<thead>
						<tr>
							<th scope="col">法人名</th>
							<th scope="col">ドメイン</th>
							<c:if test="${operator.authority_group.canControl_billing()}">
								<th>
									休止ユーザ一覧
								</th>
							</c:if>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="corp" items="${corpList}">
							<tr>
								<td><c:out value="${corp.name}" /></td>
								<td><c:out value="${corp.domain}" /></td>
								<c:if test="${operator.authority_group.canControl_billing()}">
									<td>
										<form action="list-suspend-reason" method="post">
											<input type="hidden" name="corp_id" value="${ corp.id }">
											<input type="hidden" name="corp_name" value="${ corp.name }">
											<button type="submit" class="btn btn-info btn-sm">一覧</button>
										</form>
									</td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>