<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>請求管理</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="customer-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<p style="position: relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">請求管理</p>
					<form action="register-user" method="post">
						<button type="submit" class="btn btn-info">新規登録</button>
					</form>
					<span style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<%
						session.removeAttribute("errorMessage");
					%>
					<%
						session.removeAttribute("message");
					%>
				</p>
			</div>
		</div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">社員番号</th>
					<th scope="col">氏名</th>
					<th scope="col">状態</th>
					<th scope="col">休止・復帰</th>
					<th scope="col">削除</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${userList}" var="dto">
					<tr>
						<td><c:out value="${ dto.getEmpno() }" /></td>
						<td><c:out value="${ dto.name }" /></td>
						<c:choose>
							<c:when test="${dto.state eq 0 }">
								<td><span class="badge badge-success">アクティブ</span></td>
							</c:when>
							<c:otherwise>
								<td><span class="badge badge-dark">休止</span></td>
							</c:otherwise>
						</c:choose>
						<td><c:choose>
								<c:when test="${dto.state eq 0 }">
									<form action="reason-suspend" method="post">
										<input type="hidden" name="id" value="${ dto.id }">
										<button type="submit" class="btn btn-secondary btn-sm">
											休止</button>
									</form>
								</c:when>
								<c:otherwise>
									<form action="resume-user" method="post">
										<input type="hidden" name="id" value="${ dto.id }">
										<button type="submit" class="btn btn-success btn-sm"
											onclick="return resumeConfirm('${dto.name }')">復帰</button>
									</form>
								</c:otherwise>
							</c:choose></td>
						<td>
							<form action="remove-user" method="post">
								<input type="hidden" name="id" value="${ dto.id }">
								<button type="submit" class="btn btn-danger btn-sm"
									onclick="return deleteConfirm('${dto.name}')">削除</button>
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
	</div>
</body>
</html>