<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="コース一覧" /></title>
<%@ include file="/WEB-INF/header.jsp"%>
</head>
<body>
	<%@ include file="/WEB-INF/user-navbar.jsp"%>
	<div class="container">
		<p class="row h4 mt-3 p-3 bg-light text-info rounded">コース一覧</p>
		<div class="h4 text-center text-danger ml-auto">
			<c:out value="${ courseMessage } " /><%session.removeAttribute("courseMessage"); %>
		</div>
		<nav aria-label="パンくずリスト">
			<ol class="breadcrumb bg-white pl-0 m-0">
				<li class="breadcrumb-item"><a href="user_index.jsp">CodeTrain</a></li>
				<li class="breadcrumb-item active" aria-current="page">コース一覧</li>
			</ol>
		</nav>
		<table class="table table-bordered">
			<tbody>
				<c:if test="${not empty user}">
					<tr>
						<th></th>
						<th><c:out value="進捗率" /></th>
						<th><c:out value="所要時間" /></th>
					</tr>
				</c:if>
				<c:forEach var="category" items="${categoryList}">
					<c:choose>
						<c:when test="${not empty user}">
							<tr>
								<th class="bg-info text-white"  style="word-break: break-all;"><c:out
										value="${category.name}" /></th>
								<td><c:out value="${categoryProgress[category.id] }%" /></td>
								<td><c:out value="${timeMap[category.id]}時間" /></td>
							</tr>
							<c:forEach var="course" items="${courseMap[category.id]}">
								<tr>
									<td><div class="col-sm-auto" style="word-break: break-all;">
											<a href="view-course?courseId=${ course.id }"><c:out
													value="${course.name}" /></a>
										</div></td>
									<td><c:out value="${courseProgress[course.id] }%" /></td>
									<td>
										<c:choose>
											<c:when test="${not empty course.time_approx}">
												<c:out value="${course.time_approx}時間" />
											</c:when>
											<c:otherwise>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th class="bg-info text-white"  style="word-break: break-all;"><c:out
										value="${category.name}" /></th>
							</tr>
							<c:forEach var="course" items="${courseMap[category.id]}">
								<tr>
									<td><c:choose>
											<c:when test="${course.freecourseFlg }">
												<div class="col-sm-auto" style="word-break: break-all;">
													<a href="view-course?courseId=${ course.id }"><c:out
															value="${course.name}" /></a>
												</div>
											</c:when>
											<c:otherwise>
												<div class="col-sm-auto" style="word-break: break-all;">
													<c:out value="${course.name}" />
												</div>
											</c:otherwise>
										</c:choose></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>