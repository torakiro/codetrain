<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="カリキュラム一覧" /></title>
<%@ include file="/WEB-INF/header.jsp"%>
</head>
<body>
	<%@ include file="/WEB-INF/user-navbar.jsp"%>
	<div class="container">
		<p class="row h4 mt-3 p-3 bg-light text-info rounded">
			<c:out value="${course.name}" />
		</p>
		<nav aria-label="パンくずリスト">
			<ol class="breadcrumb bg-white pl-0 m-0">
				<li class="breadcrumb-item"><a href="user_index.jsp">CodeTrain</a></li>
				<li class="breadcrumb-item"><a href="list-course">コース一覧</a></li>
				<li class="breadcrumb-item active" aria-current="page"><c:out
						value="${course.name}" /></li>
			</ol>
		</nav>
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th class="bg-info" style="white-space: nowrap;width: 100px;"><div class="text-center text-white">カテゴリ</div></th>
					<td><div class="text-center">
							<c:out value="${category.name}" />
						</div></td>
				</tr>
				<tr>
					<th class="bg-info" style="white-space: nowrap;width: 100px;"><div class="text-center text-white">前提条件</div></th>
					<td><div class="text-center">
							<c:out value="${course.requirement}" />
						</div></td>
				</tr>
				<tr>
					<th class="bg-info" style="white-space: nowrap;width: 100px;"><div class="text-center text-white">ゴール</div></th>
					<td><div class="text-center">
							<c:out value="${course.goal}" />
						</div></td>
				</tr>
				<tr>
					<th class="bg-info" style="white-space: nowrap;width: 100px;"><div class="text-center text-white">学習目安時間</div></th>
					<td><div class="text-center">
							<c:choose>
								<c:when test="${not empty course.time_approx}">
									<c:out value="${course.time_approx}時間" />
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</div></td>
				</tr>
				<tr>
					<th class="bg-info" style="white-space: nowrap;width: 100px;"><div class="text-center text-white">コース概要</div></th>
					<td><div class="text-center">
							<c:out value="${course.overview}" />
						</div></td>
				</tr>
			</tbody>
		</table>
		<div class="row h2">
			<p class="h4 text-info d-flex align-self-center pl-3">カリキュラム</p>
			<c:choose>
				<c:when
					test="${not empty curriculumList and curriculumList.size() > 0}">
					<form action="study" method="get"
						class="form-inline col-lg-2 ml-auto">
						<input type="hidden" name="curriculumId"
							value="${curriculumList.get(0).id}" />
						<button type="submit"
							class="btn btn-lg btn-outline-primary btn-block">最初から</button>
					</form>
				</c:when>
				<c:otherwise>
					<div class="col-lg-2 ml-auto">
						<button type="submit"
							class="btn btn-lg btn-outline-secondary btn-block"
							disabled="disabled">最初から</button>
					</div>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${not empty user and not empty history}">
					<form action="study" method="get" class="form-inline col-lg-2 ">
						<input type="hidden" name="curriculumId"
							value="${history.curriculum_id}" />
						<button type="submit"
							class="btn btn-lg btn-outline-primary btn-block">続きから</button>
					</form>
				</c:when>
				<c:otherwise>
					<div class="col-lg-2 ">
						<button type="button"
							class="btn btn-lg btn-outline-secondary btn-block"
							disabled="disabled">続きから</button>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<table class="table table-bordered border-dark">
			<tbody>
				<c:forEach var="curriculum" items="${curriculumList}">
					<tr>
						<td>
							<div class="text-center t3">
								<a href="study?curriculumId=${curriculum.id}" class="mx-auto"><c:out
										value="${curriculum.name}" /></a>
								<c:if test="${not empty user}">
									<div class="progress" style="height: 25px;">
										<c:forEach var="hist" items="${historys}">
											<c:if test="${hist.curriculum_id eq curriculum.id }">
												<div
													class="progress-bar bg-success
														<c:if test="${hist.curriculum_id eq history.curriculum_id }">
															progress-bar-striped
															progress-bar-animated
														</c:if>
													"
													role="progressbar" aria-valuenow="${hist.progress}"
													aria-valuemin="0" aria-valuemax="100"
													style="width:${hist.progress}%;">
													<c:out value="${hist.progress}%" />
												</div>
											</c:if>
										</c:forEach>
									</div>
								</c:if>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>