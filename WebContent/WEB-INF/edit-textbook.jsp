<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>テキスト編集</title>
<script type="text/javascript">
<!--
	function submitConfirm() {
		return window.confirm("この内容で編集しますがよろしいですか？");
	}
	var triggers = $(".modalInput").overlay({

		// マスクは、モーダルダイアログに適した若干の調整
		mask : {
			color : '#ebecff',
			loadSpeed : 200,
			opacity : 0.9
		},

		closeOnClick : false
	});
	var buttons = $("#yesno button").click(function(e) {

		// ユーザ入力の取得
		var yes = buttons.index(this) === 0;

		// 答えに何かをする
		triggers.eq(0).html("You clicked " + (yes ? "yes" : "no"));
	});
	$("#prompt form").submit(function(e) {

		// オーバレイを閉じる
		triggers.eq(1).overlay().close();

		// ユーザ入力の取得
		var input = $("input", this).val();

		// 答えに何かをする
		triggers.eq(1).html(input);

		// フォームを送信しない
		return e.preventDefault();
	});
// -->
</script>
<%@ include file="header.jsp"%>

<link rel="categoryadd" type="text/css" href="style.css">
<link rel="textadd" type="text/css" href="style.css">
</head>
<body>
	<%@ include file="operation-navbar.jsp"%>
	<form id="editTextForm" action="edit-textbook" method="post"></form>
	<input type="hidden" name="curriculum_id" value="${curriculum_id}" form="editTextForm" />
	<input type="hidden" name="id" value="${id}" form="editTextForm" />

	<div class="container">
		<div class="row" style="margin-bottom:10px">
			<div class="col-12">
				<p style="position:relative">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">テキスト編集</p>
					<span style="position:absolute; top:50%; left:50%; transform:translate(-50%, -50%);">
						<span class="text-danger">
							<c:out value="${ errorMessage }" />
						</span>
						<span class="text-success">
							<c:out value="${ message }" />
						</span>
					</span>
					<% session.removeAttribute("errorMessage"); %>
					<% session.removeAttribute("message"); %>
				</p>
			</div>
		</div>
		<div class="center-block" style="transform: translate(10%, 0);">
			<div class="form-group">
				<label class="col-form-label" for="category">カテゴリー</label> <br>
				<select name="category" form="editTextForm" class="form-contol"
					style="width: 80%">
					<c:forEach items="${category}" var="dto">
						<option value="${dto.id}">${dto.name}</option>
					</c:forEach>
				</select> <span style="width: auto"> <!-- 切り替えボタンの設定 -->
					<button type="button" class=" btn btn-primary" data-toggle="modal"
						data-target="#exampleModalLong" style="width: auto">カテゴリー作成</button>
				</span>

				<div class="form-group">
					<label class="col-form-label" for="course_name">コース</label> <br>
					<select name="course" form="editTextForm" class="form-contol"
						style="width: 80%">
						<c:forEach items="${course}" var="dto">
							<option value="${dto.id}">${dto.name}</option>
						</c:forEach>
					</select> <span style="width: auto"> <!-- 切り替えボタンの設定 -->
						<button type="button" class="btn btn-primary" data-toggle="modal"
							data-target="#createCourseModalLong">ココを押すと表示</button>
					</span>

				</div>

				<div class="form-group">
					<label class="col-form-label" for="text_title">タイトル</label> <input
						type="text" maxlength="30" title="文字数が多すぎます" id="text_title"
						name="text_title" placeholder="タイトルを入力してください"
						class="form-control" form="editTextForm" style="width: 80%" />
				</div>
				<div class="form-group">
					<label class="col-form-label" for="text_text">本文</label>
					<textarea name="text_text" maxlength="65535" id="text_text" class="form-control" form="editTextForm" style="width:80%"></textarea>
				</div>
			</div>
		</div>
		<div class="form-group" style="text-align: center">
			<br>
			<form action="edit-textbook">
				<button type="submit" form="editTextForm" class="btn btn-primary"
					form="editTextForm" onclick="return submitConfirm()">登録</button>
				<button type="reset" form="editTextForm" class="btn btn-danger">リセット</button>
			</form>
		</div>
	</div>

	<%@ include file="update-text-modal.jsp"%>
	<input type="hidden" name="updatemode" value="2" form="addCourseForm" />
	<input type="hidden" name="updatemode" value="2" form="addCategoryForm" />

</body>
</html>